﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations
{
    public class BaseEntityTypeConfiguration<T>
        : IEntityTypeConfiguration<T> where T : class, IBaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> BaseEntityConfiguration)
        {
            BaseEntityConfiguration.Property(o => o.IsDeleted).IsRequired();
            BaseEntityConfiguration.Property(o => o.FK_CreatedBy_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.FK_UpdatedBy_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.FK_DeletedBy_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.CreatedDate).IsRequired();
            BaseEntityConfiguration.Property(o => o.UpdatedDate).IsRequired();
            BaseEntityConfiguration.Property(o => o.DeletedDate).IsRequired();

        }
    }
}
