﻿using DispatchProduct.Api.HttpClient;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DispatchProduct.HttpClient
{
    public class DefaultHttpClientCrud<T,TIn, TOut>: IDefaultHttpClientCrud<T, TIn, TOut> where T: IHttpClientSettings
    {
        T _obj;
        public DefaultHttpClientCrud(T obj)
        {
            _obj = obj;
        }
        public  async Task<List<TOut>> GetList(string auth = "")
        {
            var requesturi = $"{_obj.Uri}/{_obj.GetAllVerb}";
            var response = await HttpRequestFactory.Get(requesturi,auth);

            Console.WriteLine($"Status: {response.StatusCode}");
            //Console.WriteLine(response.ContentAsString());
            var result = response.ContentAsType<List<TOut>>();
            return result;
        }

        public async Task<TOut> GetItem(string id, string auth = "")
        {
            var requesturi = $"{_obj.Uri}/{_obj.GetVerb}/{id}";
            return await GetByUri(requesturi, auth);
        }

        public async Task<TOut> Post( TIn model, string auth = "")
        {
            var requesturi = $"{_obj.Uri}/{_obj.PostVerb}";
            return await Post(requesturi, model, auth);
        }

        public async Task<TOutCustomize> PostCustomize<TInCustomize, TOutCustomize>(string requesturi, TInCustomize model, string auth = "")
        {
            return (await HttpRequestFactory.Post(requesturi, model, auth))
                .ContentAsType<TOutCustomize>();
        }

        public async Task<TOut> Post(string requesturi,TIn model, string auth = "")
        {
            var response = await HttpRequestFactory.Post(requesturi, model, auth);
            var result = response.ContentAsType<TOut>();
            return result;
        }

        public async Task<TOut> Put( TIn model,string id, string auth = "")
        {

            var requesturi = $"{_obj.Uri}/{_obj.PutVerb}/{id}";
            var response = await HttpRequestFactory.Put(requesturi, model,auth);

            Console.WriteLine($"Status: {response.StatusCode}");
            var result = response.ContentAsType<TOut>();
            return result;
        }

        public async Task Patch( string id, string auth = "")
        {
            var model = new[]
            {
                new
                {
                    op = "replace",
                    path = "/title",
                    value = "Thunderball-Patch"
                }
            };

            var requesturi = $"{_obj.Uri}/{_obj.PatchVerb}/{id}";
            var response = await HttpRequestFactory.Patch(requesturi, model,auth);
        }

        public async Task Delete(string id, string auth = "")
        {
            var requesturi = $"{_obj.Uri}/{_obj.DeleteVerb}/{id}";
            var response = await HttpRequestFactory.Delete(requesturi, auth);
        }

        public async Task<TOut> GetByUri(string requesturi,string auth="")
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<TOut>();
            return result;
        }

        public async Task<TOut> GetByUri(string requesturi)
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi);
            var result = response.ContentAsType<TOut>();
            return result;
        }
        public async Task<List<TOut>> GetListByUri(string requesturi, string auth = "")
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<List<TOut>>();
            return result;
        }

    }
}
