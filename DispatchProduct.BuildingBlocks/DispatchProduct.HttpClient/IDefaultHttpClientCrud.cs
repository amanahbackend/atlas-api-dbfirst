﻿using DispatchProduct.Api.HttpClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchProduct.HttpClient
{
    public interface IDefaultHttpClientCrud<T,TIn, TOut> where T: IHttpClientSettings
    {

        Task<List<TOut>> GetList(string auth = "");

        Task<TOut> GetItem(string id, string auth = "");

        Task<TOut> GetByUri(string requesturi, string auth = "");

        Task<TOut> Post(TIn model, string auth = "");

        Task<TOut> Put(TIn model, string id, string auth = "");


        Task Patch(string id, string auth = "");

        Task Delete(string id, string auth = "");
        
    }
}
