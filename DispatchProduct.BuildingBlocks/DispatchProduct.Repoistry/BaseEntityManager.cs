﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Repoistry
{
    public class BaseEntityManager : IBaseEntityManager
    {
        public static void AddAuditingData(IEnumerable<EntityEntry> dbEntityEntries)
        {
            try
            {
                foreach (var entry in dbEntityEntries)
                {
                    if (entry.Entity as IBaseEntity != null)
                    {
                        if (entry.State == EntityState.Added)
                        {
                            var entity = (entry.Entity as IBaseEntity);
                            if (entity != null)
                            {
                                //entity.FK_CreatedBy_Id = (entity.CurrentUser != null) ? entity.CurrentUser.Id : null;
                                entity.CreatedDate = DateTime.Now;
                            }
                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            var entity = (entry.Entity as IBaseEntity);
                            if (entity != null)
                            {
                                //entity.FK_UpdatedBy_Id = (entity.CurrentUser != null) ? entity.CurrentUser.Id : null;
                                entity.UpdatedDate = DateTime.Now;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Handle saving auditing data exception.
            }
        }
    }
}
