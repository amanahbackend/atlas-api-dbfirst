﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Repoistry
{
    public interface IRepositry<TEntity>
    {
        IQueryable<TEntity> GetAll();

        TEntity Get(params object[] id);

        TEntity Get(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);

        List<TEntity> Add(List<TEntity> entityLst);

        bool Update(TEntity entity);

        bool Delete(TEntity entity);

        bool Delete(List<TEntity> entitylst);

        bool DeleteById(params object[] id);

        PaginatedItems<TEntity> GetAllPaginated(string field, PaginatedItems<TEntity> paginatedItems = null);

        PaginatedItems<TEntity> GetAllPaginated<TKey>(Func<TEntity, TKey> orderDescExpr, PaginatedItems<TEntity> paginatedItems = null);

        PaginatedItems<TEntity> GetAllPaginated(string field, PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate);

        int SaveChanges();
    }
}