﻿using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingBlocks.ServiceDiscovery
{
    public static class ServiceDiscoveryHelper
    {
        public static async Task<string> GetServiceUrl(IDnsQuery dnsQuey, string serviceName)
        {
            var url = "";
            var dnsResult = await dnsQuey.ResolveServiceAsync("service.consul", serviceName);
            if (dnsResult.Length > 0)
            {
                var port = dnsResult.First().Port;
                var hostName = dnsResult.First().HostName;
                url = $"http://{hostName}:{port}";
            }
            return url;
        }
    }
}
