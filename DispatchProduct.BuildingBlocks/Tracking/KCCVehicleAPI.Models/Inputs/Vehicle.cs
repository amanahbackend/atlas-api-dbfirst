﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleTracking.Models
{
    public class Vehicle
    {
        public string VID { get; set; }
        public string VReg { get; set; }
        public string Vtype { get; set; }
        public string PanicButton { get; set; }
        public string DID { get; set; }
        public string DriverName { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
