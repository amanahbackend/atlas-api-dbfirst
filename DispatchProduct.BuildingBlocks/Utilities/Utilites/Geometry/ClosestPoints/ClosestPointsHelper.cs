﻿using Microsoft.Spatial;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Demo
{
    public static class ClosestPointsHelper
    {
        public static void Main()
        {
          



            //var point = DbGeography.FromText(string.Format("POINT({1} {0})", latitude, longitude), 4326);
            //var query = from person in persons
            //            let region = point.Buffer(radius)
            //            where SqlSpatialFunctions.Filter(person.Location, region) == true
            //            select person;
        }
        public static ClosestPoints FindClosestPoints(GeographyPoint p1, IEnumerable<GeographyPoint> seq2)
        {
            double closest = double.MaxValue;
            ClosestPoints result = null;
            foreach (var p2 in seq2)
            {
                var distance = p1.Distance(p2);
                if (distance!=null&&distance >= closest)
                    continue;
                result = new ClosestPoints(p1, p2);
                closest = distance.Value;
            }
            return result;
        }
        public static ClosestPoints FindClosestPoints(IEnumerable<GeographyPoint> seq1, IEnumerable<GeographyPoint> seq2)
        {
            double closest = double.MaxValue;
            ClosestPoints result = null;
            foreach (var p1 in seq1)
            {
                foreach (var p2 in seq2)
                {
                    var distance = p1.Distance(p2);
                    if (distance != null && distance >= closest)
                        continue;
                    result = new ClosestPoints(p1, p2);
                    closest = distance.Value;
                }
            }
            return result;
        }
        public static List<GeographyPoint> GetPointsInRange(GeographyPoint p1, IEnumerable<GeographyPoint> seq2, double minimumDistance)
        {
            List<GeographyPoint> result = new List<GeographyPoint>();
            foreach (var p2 in seq2)
            {
                var distance = p1.Distance(p2);
                if (distance != null && distance >= minimumDistance)
                    result.Add(p1);
            }
            return result;
        }
    }
}