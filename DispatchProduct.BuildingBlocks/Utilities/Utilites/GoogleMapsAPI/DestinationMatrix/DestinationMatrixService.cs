﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Utilites;

namespace Utilities.Utilites.GoogleMapsAPI.DestinationMatrix
{
    public class DestinationMatrixService
    {
        public static DistanceResultViewModel GetDistances(string serviceUrl, string key, List<DistancePoint> origins, List<DistancePoint> destinations)
        {
            var data= GetDistanceQuery(serviceUrl, origins, destinations, key);
            var result = JsonConvert.DeserializeObject<DistanceResultViewModel>(data);
            return result;
        }
        public static string GetDistanceQuery(string serviceUrl,List<DistancePoint> origins, List<DistancePoint> destinations, string key)
        {
            string result = "";
            string originsConcat = "";
            string destConcat = "";
            foreach (var item in origins)
            {
                if (originsConcat == "")
                {
                    originsConcat = $"{item.Latitude},{item.Longitude}";
                }
                else
                {
                    originsConcat = $"{originsConcat}|{item.Latitude},{item.Longitude}";
                }
            }
            foreach (var item in destinations)
            {
                if (destConcat == "")
                {
                    destConcat = $"{item.Latitude},{item.Longitude}";
                }
                else
                {
                    destConcat = $"{destConcat}|{item.Latitude},{item.Longitude}";
                }
            }
            string queryString = UrlUtilities.ToQueryString(new NameValueCollection
                {
                    {"origins",originsConcat },
                    {"destinations",destConcat },
                    {"key",key },
                    { "units","imperial"}
                });
            string url = serviceUrl + queryString;
            result = UrlUtilities.GetServerResponse(url);
            return result;
        }
    }
}
