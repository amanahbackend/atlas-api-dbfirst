﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Utilities.Utilites.GoogleMapsAPI.DestinationMatrix
{

    public class DistancePoint
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DistancePoint()
        {
        }
        public DistancePoint(double lat,double lng)
        {
            Latitude = lat;
            Longitude = lng;
        }
    }
    public partial class DistanceResultViewModel
    {
        [JsonProperty("destination_addresses")]
        public List<string> DestinationAddresses { get; set; }

        [JsonProperty("origin_addresses")]
        public List<string> OriginAddresses { get; set; }

        [JsonProperty("rows")]
        public List<Row> Rows { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public  class Row
    {
        [JsonProperty("elements")]
        public List<Element> Elements { get; set; }
    }

    public  class Element
    {
        [JsonProperty("distance")]
        public Distance Distance { get; set; }

        [JsonProperty("duration")]
        public Distance Duration { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public  class Distance
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }
    }

    public partial class DistanceResultViewModel
    {
        public static DistanceResultViewModel FromJson(string json) => JsonConvert.DeserializeObject<DistanceResultViewModel>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this DistanceResultViewModel self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
