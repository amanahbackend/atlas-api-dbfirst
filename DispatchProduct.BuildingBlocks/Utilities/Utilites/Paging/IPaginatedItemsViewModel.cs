﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Utilites.Paging
{
    public interface IPaginatedItemsViewModel<TEntity>
    {
        int PageNo { get; set; }

        int PageSize { get; set; }

        long Count { get; set; }

        List<TEntity> Data { get; set; }

    }
}
