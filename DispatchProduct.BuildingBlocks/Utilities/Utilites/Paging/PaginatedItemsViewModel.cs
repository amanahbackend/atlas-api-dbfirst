﻿namespace Utilities.Utilites.Paging
{
    using System.Collections.Generic;
    using Utilities.Utilites.Paging;

    public class PaginatedItemsViewModel<TEntity> : Utilities.Utilites.Paging.IPaginatedItemsViewModel<TEntity>
    {
        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public long Count { get; set; }

        public List<TEntity> Data { get; set; }
        public PaginatedItemsViewModel()
        {

        }
        public PaginatedItemsViewModel(int pageNo = 0, int pageSize = 1, long count = 0, List<TEntity> data = null)
        {
            this.PageNo = pageNo;
            this.PageSize = pageSize;
            this.Count = count;
            this.Data = data;
        }
    }
}
