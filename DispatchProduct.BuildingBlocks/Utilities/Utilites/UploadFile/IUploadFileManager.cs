﻿using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace Utilites.UploadFile
{
    public interface IUploadFileManager
    {
        ProcessResult<string> AddFile(Utilites.UploadFile.UploadFile file, string path);

        ProcessResult<List<string>> AddFiles(List<Utilites.UploadFile.UploadFile> files, string path);

        ProcessResult<string> ISFileNull(Utilites.UploadFile.UploadFile file);
    }
}
