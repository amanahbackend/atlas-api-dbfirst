﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace Utilites.UploadFile
{
    public class UploadFile : BaseEntity, IUploadFile, IBaseEntity
    {
        public string FileContent { get; set; }

        public string FileName { get; set; }

        public string FileRelativePath { get; set; }
    }
}
