﻿using BLL;
using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilites.PaginatedItems;

namespace API.Controllers
{
    [RoutePrefix("api/Contract")]
    public class ContractController : ApiController
    {
        public List<Temp_Aug_Contracts> Get()
        {
            List<Temp_Aug_Contracts> result;
            ContractManager followMngr = new ContractManager();
            result = followMngr.GetAll().Where(x => x.to_date.Value.Date > DateTime.Now.Date).ToList();
            return result;
        }
        public Temp_Aug_Contracts Get(int id)
        {
            Temp_Aug_Contracts result;
            ContractManager followMngr = new ContractManager();
            //result = followMngr.Get(id);
            result = followMngr.GetById(id);
            return result;
        }

        [Route("GetAllPaginated/{pageNo}/{pageSize}")]
        [HttpGet]
        public PaginatedItems<Temp_Aug_Contracts> GetAllPaginated(int pageNo, int pageSize)
        {
            PaginatedItems<Temp_Aug_Contracts> paginated = new PaginatedItems<Temp_Aug_Contracts>()
            {
                PageNo = pageNo,
                PageSize = pageSize
            };

            PaginatedItems<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.GetAllPaginated(nameof(Temp_Aug_Contracts.cont_no), paginated);
            //result.Data = result.Data.Where(x => x.to_date.Value.Date > DateTime.Now.Date).ToList();
            return result;
        }

        [Route("GetAllPaginated/{pageNo}/{pageSize}/{searchKey}")]
        [HttpGet]
        public PaginatedItems<Temp_Aug_Contracts> GetAllPaginated(int pageNo, int pageSize, string searchKey)
        {
            PaginatedItems<Temp_Aug_Contracts> paginated = new PaginatedItems<Temp_Aug_Contracts>()
            {
                PageNo = pageNo,
                PageSize = pageSize
            };

            PaginatedItems<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            if (String.IsNullOrEmpty(searchKey))
            {
                result = conMngr.GetAllPaginated(nameof(Temp_Aug_Contracts.cont_no), paginated);
            }
            else
            {
                result = conMngr.GetAllPaginated(nameof(Temp_Aug_Contracts.cont_no), paginated,
                    x => x.cont_no.Contains(searchKey) || x.owner_name.Contains(searchKey));
            }
            //result.Data = result.Data.Where(x => x.to_date.Value.Date > DateTime.Now.Date).ToList();
            return result;
        }


        [Route("GetByOwnerName/{id}")]
        [HttpGet]
        public List<Temp_Aug_Contracts> GetByOwnerName(string id)
        {
            List<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.GetByOwnerName(id);
            return result;
        }

        [Route("GetByOwnerId/{id}")]
        [HttpGet]
        public List<Temp_Aug_Contracts> GetByOwnerId(int id)
        {
            List<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.GetByOwnerId(id);
            return result;
        }
        //[Route("GetByStatusName/{id}")]
        //[HttpGet]
        //public List<Temp_Aug_Contracts> GetByStatusName(string id)
        //{
        //    List<Temp_Aug_Contracts> result = null;
        //    ContractManager conMngr = new ContractManager();
        //    result = conMngr.GetByStatusName(id);
        //    return result;
        //}

        [Route("Search/{id}")]
        [HttpGet]
        public List<Temp_Aug_Contracts> Search(string id)
        {
            List<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.Search(id);
            return result;
        }
        //[Route("GetByUsageName/{id}")]
        //[HttpGet]
        //public List<Temp_Aug_Contracts> GetByUsageName(string id)
        //{
        //    List<Temp_Aug_Contracts> result = null;
        //    ContractManager conMngr = new ContractManager();
        //    result = conMngr.GetByUsageName(id);
        //    return result;
        //}
        //[Route("GetByTenantName/{id}")]
        //[HttpGet]
        //public List<Temp_Aug_Contracts> GetByTenantName(string id)
        //{
        //    List<Temp_Aug_Contracts> result = null;
        //    ContractManager conMngr = new ContractManager();
        //    result = conMngr.GetByTenantName(id);
        //    return result;
        //}
        [Route("GetByRealName/{id}")]
        [HttpGet]
        public List<Temp_Aug_Contracts> GetByRealName(string id)
        {
            List<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.GetByRealName(id);
            return result;
        }
        [Route("GetByContractNo/{contractNo}")]
        [HttpGet]
        public Temp_Aug_Contracts GetByContractNo(string contractNo)
        {
            Temp_Aug_Contracts result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.GetByContractNo(contractNo);
            return result;
        }
        [Route("FilterByDates")]
        [HttpPost]
        public List<Temp_Aug_Contracts> FilterByDates(DateTime from, DateTime to)
        {
            List<Temp_Aug_Contracts> result = null;
            ContractManager conMngr = new ContractManager();
            result = conMngr.FilterByDates(from, to);
            return result;
        }

        [Route("GetCustomerByContractCode/{code}")]
        [HttpGet]
        public decimal GetCustomerByContractCode(string code)
        {
            ContractManager conMngr = new ContractManager();
            var cont = conMngr.GetByContractNo(code);
            return cont != null ? cont.customer_id : 0;
        }
    }
}
