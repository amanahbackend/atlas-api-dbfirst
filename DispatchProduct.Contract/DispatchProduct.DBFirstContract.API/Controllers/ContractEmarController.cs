﻿using BLL;
using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilites.PaginatedItems;

namespace API.Controllers
{
    [RoutePrefix("api/ContractEmar")]

    public class ContractEmarController : ApiController
    {
        public List<Contracts_E3mar_View> Get()
        {
            List<Contracts_E3mar_View> result;
            Contracts_E3mar_ViewManager followMngr = new Contracts_E3mar_ViewManager();
            result = followMngr.GetAll().ToList();
            return result;
        }
        public Contracts_E3mar_View Get(int id)
        {
            Contracts_E3mar_View result;
            Contracts_E3mar_ViewManager followMngr = new Contracts_E3mar_ViewManager();
            result = followMngr.Get(id);
            return result;
        }
        [Route("Search/{id}")]
        [HttpGet]
        public List<Contracts_E3mar_View> Search(string id)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.Search(id);
            return result;
        }
        [Route("GetAllPaginated")]
        [HttpPost]
        public PaginatedItems<Contracts_E3mar_View> GetAllPaginated([FromBody]PaginatedItems<Contracts_E3mar_View> paginatedItems)
        {
            PaginatedItems<Contracts_E3mar_View> result=null;
            Contracts_E3mar_ViewManager followMngr = new Contracts_E3mar_ViewManager();
            result = followMngr.GetAllPaginated(nameof(Contract.cont_no), paginatedItems);
            return result;
        }

        [Route("GetByOwnerName/{key}")]
        [HttpGet]
        public List<Contracts_E3mar_View> GetByOwnerName(string key)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.GetByOwnerName(key);
            return result;
        }
        [Route("GetByStatusName/{key}")]
        [HttpGet]
        public List<Contracts_E3mar_View> GetByStatusName(string key)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.GetByStatusName(key);
            return result;
        }
        [Route("GetByUsageName/{key}")]
        [HttpGet]
        public List<Contracts_E3mar_View> GetByUsageName(string key)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.GetByUsageName(key);
            return result;
        }
        [Route("GetByTenantName/{key}")]
        [HttpGet]
        public List<Contracts_E3mar_View> GetByTenantName(string key)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.GetByTenantName(key);
            return result;
        }
        [Route("GetByRealName/{key}")]
        [HttpGet]
        public List<Contracts_E3mar_View> GetByRealName(string key)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.GetByRealName(key);
            return result;
        }
        [Route("GetByContractNo/{contractNo}")]
        [HttpGet]
        public Contracts_E3mar_View GetByContractNo(string contractNo)
        {
            Contracts_E3mar_View result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.GetByContractNo(contractNo);
            return result;
        }
        [Route("FilterByDates")]
        [HttpPost]
        public List<Contracts_E3mar_View> FilterByDates(DateTime from, DateTime to)
        {
            List<Contracts_E3mar_View> result = null;
            Contracts_E3mar_ViewManager conMngr = new Contracts_E3mar_ViewManager();
            result = conMngr.FilterByDates(from, to);
            return result;
        }
    }
}
