﻿using BLL;
using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilites.PaginatedItems;

namespace API.Controllers
{
    [RoutePrefix("api/ContractProc")]

    public class ContractProcController : ApiController
    {
        public List<Contract_Proc_View> Get()
        {
            List<Contract_Proc_View> result;
            Contract_Proc_ViewManager followMngr = new Contract_Proc_ViewManager();
            result = followMngr.GetAll().ToList();
            return result;
        }
        public Contract_Proc_View Get(int id)
        {
            Contract_Proc_View result;
            Contract_Proc_ViewManager followMngr = new Contract_Proc_ViewManager();
            result = followMngr.Get(id);
            return result;
        }
        [Route("Search/{id}")]
        [HttpGet]
        public List<Contract_Proc_View> Search(string id)
        {
            List<Contract_Proc_View> result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.Search(id);
            return result;
        }
        [Route("GetAllPaginated")]
        [HttpPost]
        public PaginatedItems<Contract_Proc_View> GetAllPaginated([FromBody]PaginatedItems<Contract_Proc_View> paginatedItems)
        {
            PaginatedItems<Contract_Proc_View> result=null;
            Contract_Proc_ViewManager followMngr = new Contract_Proc_ViewManager();
            result = followMngr.GetAllPaginated(nameof(Contract_Proc_View.cont_no), paginatedItems);
            return result;
        }
        [Route("GetByOwnerName/{key}")]
        [HttpGet]
        public List<Contract_Proc_View> GetByOwnerName(string key)
        {
            List<Contract_Proc_View> result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.GetByOwnerName(key);
            return result;
        }
        [Route("GetByUsageName/{key}")]
        [HttpGet]
        public List<Contract_Proc_View> GetByUsageName(string key)
        {
            List<Contract_Proc_View> result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.GetByUsageName(key);
            return result;
        }
        [Route("GetByTenantName/{key}")]
        [HttpGet]
        public List<Contract_Proc_View> GetByTenantName(string key)
        {
            List<Contract_Proc_View> result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.GetByTenantName(key);
            return result;
        }
        [Route("GetByRealName/{key}")]
        [HttpGet]
        public List<Contract_Proc_View> GetByRealName(string key)
        {
            List<Contract_Proc_View> result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.GetByRealName(key);
            return result;
        }
        [Route("GetByContractNo/{contractNo}")]
        [HttpGet]
        public Contract_Proc_View GetByContractNo(string contractNo)
        {
            Contract_Proc_View result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.GetByContractNo(contractNo);
            return result;
        }
        [Route("FilterByDates")]
        [HttpPost]
        public List<Contract_Proc_View> FilterByDates(DateTime from, DateTime to)
        {
            List<Contract_Proc_View> result = null;
            Contract_Proc_ViewManager conMngr = new Contract_Proc_ViewManager();
            result = conMngr.FilterByDates(from, to);
            return result;
        }
    }
}
