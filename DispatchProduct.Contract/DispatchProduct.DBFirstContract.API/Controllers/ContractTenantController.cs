﻿using BLL;
using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilites.PaginatedItems;

namespace API.Controllers
{
    [RoutePrefix("api/ContractTenant")]

    public class ContractTenantController : ApiController
    {
        public List<Contract_Tenant_View> Get()
        {
            List<Contract_Tenant_View> result;
            Contract_Tenant_ViewManager followMngr = new Contract_Tenant_ViewManager();
            result = followMngr.GetAll().ToList();
            return result;
        }
        public Contract_Tenant_View Get(decimal id)
        {
            Contract_Tenant_View result;
            Contract_Tenant_ViewManager followMngr = new Contract_Tenant_ViewManager();
            result = followMngr.GetById(id);
            return result;
        }
        [Route("Search/{id}")]
        [HttpGet]
        public List<Contract_Tenant_View> Search(string id)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.Search(id);
            return result;
        }
        [Route("GetByCustomerId/{id}")]
        [HttpGet]
        public List<Contract_Tenant_View> GetByCustomerId(decimal id)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.GetByCustomerId(id);
            return result;
        }
        [Route("GetAllPaginated")]
        [HttpPost]
        public PaginatedItems<Contract_Tenant_View> GetAllPaginated([FromBody]PaginatedItems<Contract_Tenant_View> paginatedItems)
        {
            PaginatedItems<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager followMngr = new Contract_Tenant_ViewManager();
            result = followMngr.GetAllPaginated(nameof(Contract_Tenant_View.cont_no), paginatedItems);
            return result;
        }
        [Route("GetByOwnerName/{key}")]
        [HttpGet]
        public List<Contract_Tenant_View> GetByOwnerName(string key)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.GetByOwnerName(key);
            return result;
        }
        [Route("GetByTenantName/{key}")]
        [HttpGet]
        public List<Contract_Tenant_View> GetByTenantName(string key)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.GetByTenantName(key);
            return result;
        }
        [Route("GetByTenant/{key}")]
        [HttpGet]
        public List<Contract_Tenant_View> GetByTenantId(decimal key)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.GetByTenantId(key);
            return result;
        }
        [Route("GetByRealName/{key}")]
        [HttpGet]
        public List<Contract_Tenant_View> GetByRealName(string key)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.GetByRealName(key);
            return result;
        }
        [Route("GetByContractNo/{contractNo}")]
        [HttpGet]
        public Contract_Tenant_View GetByContractNo(string contractNo)
        {
            Contract_Tenant_View result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.GetByContractNo(contractNo);
            return result;
        }
        [Route("FilterByDates")]
        [HttpPost]
        public List<Contract_Tenant_View> FilterByDates(DateTime from, DateTime to)
        {
            List<Contract_Tenant_View> result = null;
            Contract_Tenant_ViewManager conMngr = new Contract_Tenant_ViewManager();
            result = conMngr.FilterByDates(from, to);
            return result;
        }
    }
}
