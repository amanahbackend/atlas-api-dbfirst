﻿using BLL;
using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilites.PaginatedItems;

namespace API.Controllers
{
    [RoutePrefix("api/Customer")]
    public class CustomerController : ApiController
    {
        public List<Customer_View> Get()
        {
            List<Customer_View> result;
            CustomerManager followMngr = new CustomerManager();
            result = followMngr.GetAll().ToList();
            return result;
        }

        public Customer_View Get(int id)
        {
            Customer_View result;
            CustomerManager followMngr = new CustomerManager();
            //result = followMngr.Get(id);
            result = followMngr.GetById(id);
            return result;
        }
        [Route("Search/{id}")]
        [HttpGet]
        public List<Customer_View> Search(string id)
        {
            List<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.Search(id);
            return result;
        }
        [Route("GetAllPaginated/{pageNo}/{pageSize}")]
        [HttpGet]
        public PaginatedItems<Customer_View> GetAllPaginated(int pageNo, int pageSize)
        {
            PaginatedItems<Customer_View> paginatedItems = new PaginatedItems<Customer_View>()
            {
                PageNo = pageNo,
                PageSize = pageSize
            };

            PaginatedItems<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetAllPaginated(nameof(real_tenants.id), paginatedItems);
            return result;
        }

        [Route("GetAllPaginated/{pageNo}/{pageSize}/{searchKey}")]
        [HttpGet]
        public PaginatedItems<Customer_View> GetAllPaginated(int pageNo, int pageSize, string searchKey)
        {
            PaginatedItems<Customer_View> paginatedItems = new PaginatedItems<Customer_View>()
            {
                PageNo = pageNo,
                PageSize = pageSize
            };

            PaginatedItems<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetAllPaginated(nameof(real_tenants.id), paginatedItems, x => x.comm_sign_person_tel.Contains(searchKey) || x.name_en.Contains(searchKey));
            return result;
        }

        [Route("GetByName/{id}")]
        [HttpGet]
        public List<Customer_View> GetByName(string id)
        {
            List<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetByName(id);
            return result;
        }
        [Route("GetByCivilId/{id}")]
        [HttpGet]
        public List<Customer_View> GetByCivilId(string id)
        {
            List<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetByCivilId(id);
            return result;
        }
        [Route("GetByPassportNo/{id}")]
        [HttpGet]
        public List<Customer_View> GetByPassportNo(string id)
        {
            List<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetByPassportNo(id);
            return result;
        }
        [Route("GetByTelephoneNo/{id}")]
        [HttpGet]
        public List<Customer_View> GetByTelephoneNo(string id)
        {
            List<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetByTelephoneNo(id);
            return result;
        }
        [Route("GetByNationality/{id}")]
        [HttpGet]
        public List<Customer_View> GetByNationality(string id)
        {
            List<Customer_View> result = null;
            CustomerManager conMngr = new CustomerManager();
            result = conMngr.GetByNationality(id);
            return result;
        }
    }
}
