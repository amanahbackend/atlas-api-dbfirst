﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DispatchProduct.DBFirstContract.API.Controllers
{
    [RoutePrefix("api/CustomerType")]
    public class CustomerTypeController : ApiController
    {
        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAllPaginated()
        {
            return Ok();
        }
    }
}