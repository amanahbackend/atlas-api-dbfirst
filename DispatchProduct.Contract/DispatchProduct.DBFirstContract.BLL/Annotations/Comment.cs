﻿//using Domain.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnotaionDAL
{
    [MetadataType(typeof(CommentAnnotation))]
    public partial class Comment
    {


    }
    public class CommentAnnotation
    {

        public int id { get; set; }
        [StringLength(200)]
        [Required]
        //[Display(Name = "label_schoolRegion", ResourceType = typeof(Messages))]
        public string regionType { get; set; }
    }
}
