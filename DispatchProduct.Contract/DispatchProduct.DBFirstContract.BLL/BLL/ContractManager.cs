﻿using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ContractManager : Repositry<Temp_Aug_Contracts>
    {

        public ContractManager()
        {

        }
        public ContractManager(DbContext context)
            : base(context)
        {

        }
        public List<Temp_Aug_Contracts> Search(string key)
        {
            return GetAll().Where(con =>
             con.owner_name.ToLower().Contains(key.ToLower())
             ||
             con.real_name.ToLower().Contains(key.ToLower())
             ||
             con.cont_no.ToLower().Contains(key.ToLower())
             /*||
             con.status.ToLower().Contains(key.ToLower())
             ||
             con.name_ar.ToLower().Contains(key.ToLower())
             ||
             con.name.ToLower().Contains(key.ToLower())*/).ToList();
        }
        public List<Temp_Aug_Contracts> GetByOwnerName(string key)
        {
            return GetAll().Where(con =>
           con.owner_name.ToLower().Contains(key.ToLower())).ToList();
        }

        public List<Temp_Aug_Contracts> GetByOwnerId(int id)
        {
            return GetAll().Where(con => con.customer_id == id).ToList();
        }
        public Temp_Aug_Contracts GetById(int id)
        {
            return GetAll().Where(x => x.contract_id == id).FirstOrDefault();
        }

        //public List<Temp_Aug_Contracts> GetByStatusName(string key)
        //{
        //    return GetAll().Where(con =>
        //   con.status.ToLower().Contains(key.ToLower())).ToList();
        //}
        //public List<Temp_Aug_Contracts> GetByUsageName(string key)
        //{
        //    return GetAll().Where(con =>
        //   con.name.ToLower().Contains(key.ToLower())).ToList();
        //}
        //public List<Temp_Aug_Contracts> GetByTenantName(string key)
        //{
        //    return GetAll().Where(con =>
        //   con.name_ar.ToLower().Contains(key.ToLower())).ToList();
        //}
        public List<Temp_Aug_Contracts> GetByRealName(string key)
        {
            return GetAll().Where(con =>
           con.real_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public Temp_Aug_Contracts GetByContractNo(string key)
        {
            return GetAll().Where(con =>
           con.cont_no.ToLower().Contains(key.ToLower())).FirstOrDefault();
        }
        public List<Temp_Aug_Contracts> FilterByDates(DateTime from, DateTime to)
        {
            return GetAll().Where(con =>
            (con.from_dt >= from)
             &&
            (con.to_date <= to)
           ).ToList();
        }

    }
}