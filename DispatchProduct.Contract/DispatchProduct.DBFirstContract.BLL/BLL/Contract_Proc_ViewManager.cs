﻿using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Contract_Proc_ViewManager : Repositry<Contract_Proc_View>
    {
        
        public Contract_Proc_ViewManager()
        {

        }
        public Contract_Proc_ViewManager(DbContext context)
            : base(context)
        {

        }
        public List<Contract_Proc_View> Search(string key)
        {
            return GetAll().Where(con =>
             con.owner.ToLower().Contains(key.ToLower())
             ||
             con.realestate.ToLower().Contains(key.ToLower())
             ||
             con.cont_no.ToLower().Contains(key.ToLower())
             ||
             con.usage.ToLower().Contains(key.ToLower())
             ||
             con.tenant.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contract_Proc_View> GetByOwnerName(string key)
        {
            return GetAll().Where(con =>
           con.owner.ToLower().Contains(key.ToLower())).ToList();
        }
        
        public List<Contract_Proc_View> GetByUsageName(string key)
        {
            return GetAll().Where(con =>
           con.usage.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contract_Proc_View> GetByTenantName(string key)
        {
            return GetAll().Where(con =>
           con.tenant.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contract_Proc_View> GetByRealName(string key)
        {
            return GetAll().Where(con =>
           con.realestate.ToLower().Contains(key.ToLower())).ToList();
        }
        public Contract_Proc_View GetByContractNo(string key)
        {
            return GetAll().Where(con =>
           con.cont_no.ToLower().Contains(key.ToLower())).FirstOrDefault();
        }
        public List<Contract_Proc_View> FilterByDates(DateTime from, DateTime to)
        {
            return GetAll().Where(con =>
            (con.from_date >= from)
             &&
            (con.to_date <= to)
           ).ToList();
        }
    }
}