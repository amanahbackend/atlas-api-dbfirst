﻿using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Contract_Tenant_ViewManager : Repositry<Contract_Tenant_View>
    {
        
        public Contract_Tenant_ViewManager()
        {

        }
        public Contract_Tenant_ViewManager(DbContext context)
            : base(context)
        {

        }
        public  Contract_Tenant_View GetById(decimal id)
        {
            return GetAll().Where(c => c.contract_id == id).FirstOrDefault();
        }
        public List<Contract_Tenant_View> Search(string key)
        {
            return GetAll().Where(con =>
             con.owner_name.ToLower().Contains(key.ToLower())
             ||
             con.real_name.ToLower().Contains(key.ToLower())
             ||
             con.cont_no.ToLower().Contains(key.ToLower())
             ||
             con.tenants_passport.ToLower().Contains(key.ToLower())
             ||
             con.tenants_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contract_Tenant_View> GetByOwnerName(string key)
        {
            return GetAll().Where(con =>
           con.owner_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contract_Tenant_View> GetByCustomerId(decimal key)
        {
            return GetAll().Where(con =>con.tenants_id==key).ToList();
        }


        public List<Contract_Tenant_View> GetByTenantName(string key)
        {
            return GetAll().Where(con =>
           con.tenants_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contract_Tenant_View> GetByTenantId(decimal key)
        {
            return GetAll().Where(con =>
           con.tenants_id==key).ToList();
        }
        public List<Contract_Tenant_View> GetByRealName(string key)
        {
            return GetAll().Where(con =>
           con.real_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public Contract_Tenant_View GetByContractNo(string key)
        {
            return GetAll().Where(con =>
           con.cont_no.ToLower().Contains(key.ToLower())).FirstOrDefault();
        }
        public List<Contract_Tenant_View> FilterByDates(DateTime from, DateTime to)
        {
            return GetAll().Where(con =>
            (con.from_dt >= from)
             &&
            (con.to_date <= to)
           ).ToList();
        }
    }
}