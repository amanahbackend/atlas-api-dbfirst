﻿using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Contracts_E3mar_ViewManager : Repositry<Contracts_E3mar_View>
    {
        
        public Contracts_E3mar_ViewManager()
        {

        }
        public Contracts_E3mar_ViewManager(DbContext context)
            : base(context)
        {

        }
        public List<Contracts_E3mar_View> Search(string key)
        {
            return GetAll().Where(con =>
             con.owner_name.ToLower().Contains(key.ToLower())
             ||
             con.real_name.ToLower().Contains(key.ToLower())
             ||
             con.cont_no.ToLower().Contains(key.ToLower())
             ||
             con.status.ToLower().Contains(key.ToLower())
             ||
             con.usage.ToLower().Contains(key.ToLower())
             ||
             con.tenant.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contracts_E3mar_View> GetByOwnerName(string key)
        {
            return GetAll().Where(con =>
           con.owner_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contracts_E3mar_View> GetByStatusName(string key)
        {
            return GetAll().Where(con =>
           con.status.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contracts_E3mar_View> GetByUsageName(string key)
        {
            return GetAll().Where(con =>
           con.usage.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contracts_E3mar_View> GetByTenantName(string key)
        {
            return GetAll().Where(con =>
           con.tenant.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Contracts_E3mar_View> GetByRealName(string key)
        {
            return GetAll().Where(con =>
           con.real_name.ToLower().Contains(key.ToLower())).ToList();
        }
        public Contracts_E3mar_View GetByContractNo(string key)
        {
            return GetAll().Where(con =>
           con.cont_no.ToLower().Contains(key.ToLower())).FirstOrDefault();
        }
        public List<Contracts_E3mar_View> FilterByDates(DateTime from, DateTime to)
        {
            return GetAll().Where(con =>
            (con.from_dt >= from)
             &&
            (con.to_date <= to)
           ).ToList();
        }
    }
}