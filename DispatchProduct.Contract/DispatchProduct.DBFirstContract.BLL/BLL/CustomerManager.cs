﻿using DispatchProduct.DBFirstContract.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CustomerManager : Repositry<Customer_View>
    {

        public CustomerManager()
        {

        }
        public CustomerManager(DbContext context)
            : base(context)
        {

        }
        public List<Customer_View> Search(string key)
        {
            return GetAll().Where(con =>
             con.name_ar.ToLower().Contains(key.ToLower())
             ||
             con.name_en.ToLower().Contains(key.ToLower())
             ||
             con.civil_id.ToLower().Contains(key.ToLower())
             ||
             /* con.comm_sign_person_civil_id.ToLower().Contains(key.ToLower())
             ||*/
             con.passport_no.ToLower().Contains(key.ToLower())
             ||
             /*con.kinder_person_tel_no.ToLower().Contains(key.ToLower())
             ||*/
             con.comm_sign_person_tel.ToLower().Contains(key.ToLower())).ToList();
        }

        public Customer_View GetById(int Id)
        {
            return GetAll().Where(x => x.id == Id).FirstOrDefault();
        }

        public List<Customer_View> GetByName(string key)
        {
            return GetAll().Where(con =>
           con.name_ar.ToLower().Contains(key.ToLower())
                ||
            con.name_en.ToLower().Contains(key.ToLower())
           ).ToList();
        }
        public List<Customer_View> GetByCivilId(string key)
        {
            return GetAll().Where(
                con => (con.civil_id.ToLower().Contains(key.ToLower())
                        ||
                con.comm_sign_person_civil_id.ToLower().Contains(key.ToLower()))).ToList();
        }
        public List<Customer_View> GetByPassportNo(string key)
        {
            return GetAll().Where(con =>
           con.passport_no.ToLower().Contains(key.ToLower())).ToList();
        }
        public List<Customer_View> GetByTelephoneNo(string key)
        {
            return GetAll().Where(con =>(
           con.kinder_person_tel_no.ToLower().Contains(key.ToLower())
           ||
           con.comm_sign_person_tel.ToLower().Contains(key.ToLower())
           )).ToList();
        }
        public List<Customer_View> GetByNationality(string key)
        {
            return GetAll().Where(con =>
           (
           con.comm_sign_person_nationality.ToLower().Contains(key.ToLower())
           ||
           con.nationality_id.ToString().Contains(key.ToLower()))
           ).ToList();
        }
    }
}