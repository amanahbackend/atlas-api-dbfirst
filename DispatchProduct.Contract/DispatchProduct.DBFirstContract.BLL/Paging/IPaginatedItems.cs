﻿using System.Collections.Generic;
namespace Utilites.PaginatedItems
{
    public interface IPaginatedItems<TEntity> 
    {
         int PageNo { get;  set; }
         int PageSize { get;  set; }
         long Count { get; set; }
         List<TEntity> Data { get;  set; }
    }
}
