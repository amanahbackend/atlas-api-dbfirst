﻿using AutoMapper;
using DispatchProduct.Custody.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites;

namespace DispatchProduct.Custody.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Category, CategoryViewModel>();
            CreateMap<CategoryViewModel, Category>()
                .IgnoreBaseEntityProperties();

            CreateMap<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TechnicanAssignedItemsViewModel, TechnicanAssignedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
                .IgnoreBaseEntityProperties();

            CreateMap<Item, ItemViewModel>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
            CreateMap<ItemViewModel, Item>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category))
                .IgnoreBaseEntityProperties();

        }
    }
}
