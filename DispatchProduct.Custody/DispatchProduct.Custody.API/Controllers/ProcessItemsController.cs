﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Utilites.ExcelToGenericList;
using Utilites.ExcelToGenericList.Test;
using Utilites.UploadFile;
using Utilites.ProcessingResult;
using Microsoft.Extensions.Options;
using DispatchProduct.Custody.ExcelSettings;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Custody.BLL.IManagers;
using DispatchProduct.Custody.Entities.Settings;
using DispatchProduct.Custody.Entities;
using IdentityServer4.AccessTokenValidation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Custody.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ProcessItemController : Controller
    {
        public IItemManager manger;
        public readonly IMapper mapper;
        InventoryAppSettings settings;
        ExcelSheetProperties excelSheetProperties;
        IProcessResultMapper processResultMapper;
        IExcelItemsManager excelSalaryManager;
        public ProcessItemController(IItemManager _manger, IMapper _mapper, IOptions<InventoryAppSettings> _settings, IOptions<ExcelSheetProperties> _excelPropsettings, IExcelItemsManager _excelSalaryManager, IProcessResultMapper _processResultMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            settings = _settings.Value;
            excelSheetProperties = _excelPropsettings.Value;
            excelSalaryManager = _excelSalaryManager;
            processResultMapper = _processResultMapper;

        }
        // GET: api/values

        [HttpPost]
        [Route("SubmitItems")]
        public ProcessResultViewModel<List<ItemViewModel>> SubmitItems([FromBody]UploadFile file)
        {
            if (settings.ExclPath == null)
            {
                settings.ExclPath = "ItemsSheet";
            }
            var entityResult=excelSalaryManager.Process(settings.ExclPath, file, excelSheetProperties);
            return processResultMapper.Map<List<Item>, List<ItemViewModel>>(entityResult);
        }
    }
}
