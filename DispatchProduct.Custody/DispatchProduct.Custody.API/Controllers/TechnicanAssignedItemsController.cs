﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers.V1;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Custody.Entities;
using DispatchingProduct.Inventory.BLL.Managers;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class TechnicanAssignedItemsController : BaseControllerV1<ITechnicanAssignedItemsManager, TechnicanAssignedItems, TechnicanAssignedItemsViewModel>
    {
        public new ITechnicanAssignedItemsManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceProvider;
        public TechnicanAssignedItemsController(IServiceProvider _serviceProvider, ITechnicanAssignedItemsManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            serviceProvider = _serviceProvider;
        }
        
        private IItemManager ItemManager
        {
            get { return serviceProvider.GetService<IItemManager>(); }
        }
        [Route("AssignItemToTechnican")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<TechnicanAssignedItemsViewModel> AssignItemToTechnican([FromBody] TechnicanAssignedItemsViewModel model)
        {
            var itemRes = ItemManager.Get(model.FK_Item_Id);
            if (itemRes.IsSucceeded && itemRes.Data != null && itemRes.Data.IsAvailable)
            {
                itemRes.Data.IsAvailable = false;
                ItemManager.Update(itemRes.Data);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<TechnicanAssignedItemsViewModel>(null, $"there is no item available with this id {model.FK_Item_Id}");
            }
            return base.Post(model);
        }
        [Route("TransferItemFromTechnicanToTechnican")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<TechnicanAssignedItemsViewModel> TransferItemFromTechnicanToTechnican([FromBody] TechnicanAssignedItemsViewModel model)
        {
            if (!string.IsNullOrEmpty(model.FK_AssignedFromTechnican_Id))
            {
                var obj = manger.GetTechnicanAssignedFromItem(model.FK_Item_Id, model.FK_AssignedFromTechnican_Id, model.IsReleased);
                if (obj.IsSucceeded)
                {
                    obj.Data.IsReleased = true;
                    model.AssignedDate = obj.Data.CompletedDate = DateTime.Now;
                    manger.Update(obj.Data);
                }
                else
                {
                    return processResultMapper.Map<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>(obj);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<TechnicanAssignedItemsViewModel>(null, "FK_AssignedFromTechnican_Id cann't be null");
            }
            return base.Post(model);
        }

        [Route("GetAssignedItemsByTechnican/{technicanId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<TechnicanAssignedItemsViewModel>> GetAssignedItemsByTechnican([FromRoute] string technicanId)
        {
            if (!string.IsNullOrEmpty(technicanId))
            {
                var obj = manger.GetTechnicanAssignedItems(technicanId);
                return processResultMapper.Map<List<TechnicanAssignedItems>, List<TechnicanAssignedItemsViewModel>>(obj);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed< List<TechnicanAssignedItemsViewModel>>(null, "FK_AssignedFromTechnican_Id cann't be null");
            }
        }
    }
}
