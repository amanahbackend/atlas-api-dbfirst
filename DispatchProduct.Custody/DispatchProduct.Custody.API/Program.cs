﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.Custody.Context;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Custody.Entities.Settings;
using Microsoft.Extensions.Options;

namespace DispatchProduct.Custody.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<InventoryDbContext>((context, services) =>
            {
                //var env = services.GetService<IHostingEnvironment>();
                //var logger = services.GetService<ILogger<InventoryDbContextSeed>>();
                //var settings = services.GetService<IOptions<InventoryAppSettings>>();
                //new InventoryDbContextSeed()
                //    .SeedAsync(context, env, logger, settings)
                //    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
       WebHost.CreateDefaultBuilder(args)
           .UseKestrel()
           .UseContentRoot(Directory.GetCurrentDirectory())
           .UseIISIntegration()
           .UseStartup<Startup>()
            .UseSetting("detailedErrors", "true")
            .CaptureStartupErrors(true)
           .ConfigureLogging((hostingContext, builder) =>
           {
               builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
               builder.AddConsole();
               builder.AddDebug();
           })
           .Build();
    }
}
