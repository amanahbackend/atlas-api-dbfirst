﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Custody.BLL.IManagers;
using DispatchProduct.Custody.BLL.Managers;
using DispatchProduct.Custody.Context;
using DispatchProduct.Custody.Entities;
using DispatchProduct.Custody.Entities.Settings;
using DispatchProduct.Custody.ExcelSettings;
using DispatchProduct.Custody.IEntities;
using DispatchProduct.RepositoryModule;
using DnsClient;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace DispatchProduct.Custody.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                 .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                 .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddDbContext<InventoryDbContext>(options =>
           options.UseSqlServer(Configuration["ConnectionString"],
           sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Custody.EFCore.MSSQL")));
            services.AddSingleton(provider => Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            services.AddSingleton(provider => Configuration);

            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddApiVersioning(o => o.ReportApiVersions = true);
            services.AddOptions();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Calling API",
                        Description = "Calling  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "calling", "calling API" }
                    }
                });
            });
            ConfigureAuthService(services);

            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));
            services.Configure<InventoryAppSettings>(Configuration);
            services.Configure<ExcelSheetProperties>(Configuration.GetSection("ExcelSheetProperties"));
            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(ICategory), typeof(Category));
            services.AddScoped(typeof(IItem), typeof(Item));
            services.AddScoped(typeof(ITechnicanAssignedItems), typeof(TechnicanAssignedItems));
            services.AddScoped(typeof(ICategoryManager), typeof(CategoryManager));
            services.AddScoped(typeof(IItemManager), typeof(ItemManager));
            services.AddScoped(typeof(IExcelItemsManager), typeof(ExcelItemsManager));
            services.AddScoped(typeof(ITechnicanAssignedItemsManager), typeof(TechnicanAssignedItemsManager));
            services.AddMvc();
            services.Configure<InventoryAppSettings>(Configuration);
            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            Mapper.AssertConfigurationIsValid();
            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reviewing API");
            });

            app.UseMvc();
            //app.UseConsulRegisterService();
        }
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

            //services.AddAuthorization(options =>
            //{
            //    // Policy for dashboard: only administrator role.
            //    options.AddPolicy("AdminRole", policy => policy.RequireClaim("role", "Admin"));
            //    // Policy for resources: user or administrator role. 
            //    options.AddPolicy("GlobalRole", policyBuilder => policyBuilder.RequireAssertion(
            //            context => context.User.HasClaim(claim => (claim.Type == "role" && claim.Value == "user")
            //               || (claim.Type == "role" && claim.Value == "Admin"))
            //        )
            //    );
            //});
        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
