﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Custody.Entities;
using DispatchProduct.RepositoryModule;

namespace DispatchingProduct.Inventory.BLL.IManagers
{
    public interface ICategoryManager : IRepository<Category>
    {
    }
}
