﻿using DispatchProduct.Custody.ExcelSettings;
using DispatchProduct.Custody.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Custody.BLL.IManagers
{
    public interface IExcelItemsManager
    {
        ProcessResult<List<Item>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties);
    }
}
