﻿using DispatchProduct.Custody.Entities;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.IManagers
{
    public interface IItemManager : IRepository<Item>
    {
        ProcessResult<Item> GetByCode(string Code);
    }
}
