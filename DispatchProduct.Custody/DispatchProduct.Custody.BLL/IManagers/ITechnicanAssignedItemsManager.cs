﻿using DispatchProduct.Custody.Entities;
using DispatchProduct.RepositoryModule;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITechnicanAssignedItemsManager : IRepository<TechnicanAssignedItems>
    {
        ProcessResult<TechnicanAssignedItems> GetTechnicanAssignedFromItem(int itemId, string fk_tech_id, bool isReleased = false);
        ProcessResult<List<TechnicanAssignedItems>> GetTechnicanAssignedItems(string fk_tech_id, bool isReleased = false);
    }
}
