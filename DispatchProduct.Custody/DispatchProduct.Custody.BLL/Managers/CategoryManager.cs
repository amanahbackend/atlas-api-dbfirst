﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Custody.Context;
using DispatchProduct.Custody.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class CategoryManager:Repository<Category>, ICategoryManager
    {
        public CategoryManager(InventoryDbContext context)
            : base(context)
        {

        }
    }
}
