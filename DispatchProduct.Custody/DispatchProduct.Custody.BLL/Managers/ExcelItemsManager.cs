﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Custody.BLL.IManagers;
using DispatchProduct.Custody.Entities;
using DispatchProduct.Custody.ExcelSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Custody.BLL.Managers
{
    public class ExcelItemsManager: IExcelItemsManager
    {
        ICategoryManager categoryManager;
        IItemManager itemManager;
        public ExcelItemsManager(ICategoryManager _categoryManager, IItemManager _itemManager)
        {
            categoryManager = _categoryManager;
            itemManager = _itemManager;
        }
        public ExcelSheetProperties ExcelSheetProperties { get; set; }
        public ExcelItem GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var item = new ExcelItem()
            {
                CategoryAR = rowData[columnNames.IndexFor(ExcelSheetProperties.CategoryAR)].ToString(),
                CategoryEN = rowData[columnNames.IndexFor(ExcelSheetProperties.CategoryEN)].ToString(),
                SerialNo = rowData[columnNames.IndexFor(ExcelSheetProperties.SerialNo)].ToString(),
                NameAR = rowData[columnNames.IndexFor(ExcelSheetProperties.NameAR)].ToString(),
                NameEN = rowData[columnNames.IndexFor(ExcelSheetProperties.NameEN)].ToString(),
                DescriptionAR = rowData[columnNames.IndexFor(ExcelSheetProperties.DescriptionAR)].ToString(),
                DescriptionEN = rowData[columnNames.IndexFor(ExcelSheetProperties.DescriptionEN)].ToString(),
            };
            return item;
        }
        public ProcessResult<List<Item>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties)
        {
            ProcessResult<List<Item>> result = new ProcessResult<List<Item>>();
            try
            {
                if (excelSheetProperties != null && path != null)
                    ExcelSheetProperties = excelSheetProperties;
                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                //Uploadfile = mapper.Map<UploadFileViewModel, UploadFile>(file);
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(Uploadfile.FileName, path);
                    IList<ExcelItem> dataList = ExcelReader.GetDataToList(excelPath, GetItems);
                    result.Data = SaveItems(dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }
        private List<Item> SaveItems(IList<ExcelItem> dataList)
        {
            List<Item> result = new List<Item>();
            foreach (var item in dataList)
            {
                if (item.CategoryEN != null && item.SerialNo != null && item.NameEN != null)
                {

                    var itemDB = GetItemIfExist(item);
                    bool isItemExist = true;
                    if (itemDB == null)
                    {
                        isItemExist = false;
                        itemDB = new Item();
                    }
                    var Category = GetOrAddCategory(item.CategoryEN, item.CategoryAR);
                    itemDB.SerialNo = item.SerialNo;
                    itemDB.FK_Category_Id = Category.Id;
                    itemDB.NameAR = item.NameAR;
                    itemDB.NameEN = item.NameEN;
                    itemDB.DescriptionAR = item.DescriptionAR;
                    itemDB.DescriptionEN = item.DescriptionEN;
                    itemDB.IsAvailable = true;
                    if (isItemExist)
                    {
                        itemManager.Update(itemDB);
                    }
                    else
                    {
                        var itemDBRes = itemManager.Add(itemDB);
                        if (itemDBRes.IsSucceeded && itemDBRes.Data != null)
                        {
                            itemDB = itemDBRes.Data;
                        }
                    }
                    result.Add(itemDB);
                }
            }
            return result;
        }
        private Category GetOrAddCategory(string categoryNameEN, string categoryNameAR)
        {
            var Category = categoryManager.GetAllQuerable().Data.Where(p => p.NameEN == categoryNameEN || p.NameAR == categoryNameAR).FirstOrDefault();
            if (Category == null)
            {
                Category = new Category();
                Category.NameEN = categoryNameEN;
                Category.NameAR = categoryNameAR;
                var CategoryRes = categoryManager.Add(Category);
                if (CategoryRes.IsSucceeded && CategoryRes.Data != null)
                {
                    Category = CategoryRes.Data;
                }
                else
                {
                    Category = null;
                }
            }
            return Category;
        }
        private Item GetItemIfExist(ExcelItem item)
        {
            return itemManager.GetAllQuerable().Data.Where(e => e.SerialNo == item.SerialNo && e.NameEN == e.NameEN).FirstOrDefault();
        }

    }
}
