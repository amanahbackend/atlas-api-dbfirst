﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Custody.Entities;
using DispatchProduct.Custody.Context;
using DispatchingProduct.Inventory.BLL.IManagers;
using System.Linq;
using Utilites.ProcessingResult;
using System.Reflection;
using DispatchProduct.RepositoryModule;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class ItemManager : Repository<Item>, IItemManager
    {
        public ItemManager(InventoryDbContext context)
            : base(context)
        {

        }
        public ProcessResult<Item> GetByCode(string serialNo)
        {
            ProcessResult<Item> result = null;
            if (serialNo != null)
            {
                var data= GetAllQuerable().Data.Where(itm => itm.SerialNo == serialNo).FirstOrDefault();
                if (data != null)
                {
                    result = ProcessResultHelper.Succedded(data);
                }
                else
                {
                    result = ProcessResultHelper.Failed<Item>(null,new Exception("There is no item with this serial"));
                }
            }
            return result;
        }
        
    }
}
