﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Custody.Context;
using DispatchProduct.Custody.Entities;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class TechnicanAssignedItemsManager : Repository<TechnicanAssignedItems>, ITechnicanAssignedItemsManager
    {
        private IItemManager itemManger;

        public TechnicanAssignedItemsManager(InventoryDbContext context, IItemManager _itemManger)
          : base((DbContext)context)
        {
            itemManger = _itemManger;
        }
        public ProcessResult<TechnicanAssignedItems> GetTechnicanAssignedFromItem(int itemId, string fk_tech_id, bool isReleased = false)
        {
            ProcessResult<TechnicanAssignedItems> result = null;
            var data = GetAllQuerable().Data.Where(r => r.FK_Item_Id == itemId && r.FK_AssignedToTechnican_Id == fk_tech_id && r.IsReleased == isReleased).FirstOrDefault();
            if (data != null)
            {
                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultHelper.Failed<TechnicanAssignedItems>(null, new Exception($"item with Id {itemId} not assigned to TechnicanId {fk_tech_id}"));
            }
            return result;
        }

        public ProcessResult<List<TechnicanAssignedItems>> GetTechnicanAssignedItems(string fk_tech_id, bool isReleased = false)
        {
            ProcessResult<List<TechnicanAssignedItems>> result = null;
            var data = GetAllQuerable().Data.Where(r => r.FK_AssignedToTechnican_Id == fk_tech_id && r.IsReleased == isReleased).ToList();
            if (data != null)
            {
                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultHelper.Failed< List<TechnicanAssignedItems>>(null, new Exception($"technican with Id {fk_tech_id} not assigned to TechnicanId {fk_tech_id}"));
            }
            return result;
        }
    }
}
