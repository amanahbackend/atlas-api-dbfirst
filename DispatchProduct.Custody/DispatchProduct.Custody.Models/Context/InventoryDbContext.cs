﻿using DispatchProduct.Custody.Entities;
using DispatchProduct.Custody.EntityConfigurations;
using DispatchProduct.Ordering.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Custody.Context
{
    public class InventoryDbContext : DbContext
    {

        public DbSet<Item> Item { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<TechnicanAssignedItems> TechnicanAssignedItems { get; set; }
        public InventoryDbContext(DbContextOptions<InventoryDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ItemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TechnicanAssignedItemsEntityTypeConfiguration());
        }
    }
}
