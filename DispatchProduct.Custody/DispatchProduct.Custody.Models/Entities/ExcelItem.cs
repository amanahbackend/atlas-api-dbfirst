﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace DispatchProduct.Custody.Entities
{
    public class ExcelItem : BaseEntity
    {
        public string CategoryAR { get; set; }
        public string CategoryEN { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string SerialNo { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public int FK_Category_Id { get; set; }
    }
}

