﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Custody.IEntities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchProduct.Custody.Entities
{
    public class Item : BaseLKPEntity, IItem, IBaseEntity
    {
        public string SerialNo { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public int FK_Category_Id { get; set; }
        public bool IsAvailable { get; set; }
        [NotMapped]
        public Category Category { get; set; }
    }
}