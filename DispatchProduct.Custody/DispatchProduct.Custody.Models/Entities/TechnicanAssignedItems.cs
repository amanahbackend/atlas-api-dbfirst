﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Custody.IEntities;
using System;

namespace DispatchProduct.Custody.Entities
{
    public class TechnicanAssignedItems : BaseEntity, ITechnicanAssignedItems, IBaseEntity
    {
        public int FK_Item_Id { get; set; }
        public string SerialNo { get; set; }
        public string FK_AssignedFromTechnican_Id { get; set; }
        public string FK_AssignedToTechnican_Id { get; set; }
        public DateTime AssignedDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public bool IsReleased { get; set; }
        public Item Item { get; set; }
    }
}
