﻿using DispatchProduct.Custody.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class CategoryEntityTypeConfiguration
        : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> CategoryConfiguration)
        {
            CategoryConfiguration.ToTable("Category");

            CategoryConfiguration.HasKey(o => o.Id);

            CategoryConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Categoryseq");


            CategoryConfiguration.Property(o => o.NameAR)
                .HasMaxLength(500)
                .IsRequired();
            CategoryConfiguration.Property(o => o.NameEN)
               .HasMaxLength(500)
               .IsRequired();
        }
    }
}
