﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Custody.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Custody.EntityConfigurations
{
    public class ItemEntityTypeConfiguration : BaseEntityTypeConfiguration<Item>, IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> ItemConfiguration)
        {
            base.Configure(ItemConfiguration);

            ItemConfiguration.ToTable("Item");

            ItemConfiguration.HasKey(o => o.Id);

            ItemConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Itemseq");
            ItemConfiguration.Property(o => o.NameAR).IsRequired();
            ItemConfiguration.Property(o => o.NameEN).IsRequired();
            ItemConfiguration.Property(o => o.SerialNo).IsRequired();
            ItemConfiguration.Property(o => o.FK_Category_Id).IsRequired();

        }
    }
}