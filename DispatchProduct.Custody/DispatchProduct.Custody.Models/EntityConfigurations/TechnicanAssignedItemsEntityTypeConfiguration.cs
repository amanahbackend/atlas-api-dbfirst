﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Custody.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Custody.EntityConfigurations
{
    public class TechnicanAssignedItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<TechnicanAssignedItems>, IEntityTypeConfiguration<TechnicanAssignedItems>
    {
        public void Configure(EntityTypeBuilder<TechnicanAssignedItems> TechnicanAssignedItemsConfiguration)
        {
            base.Configure(TechnicanAssignedItemsConfiguration);

            TechnicanAssignedItemsConfiguration.ToTable("TechnicanAssignedItems");

            TechnicanAssignedItemsConfiguration.HasKey(o => o.Id);

            TechnicanAssignedItemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("TechnicanAssignedItemsseq");
            TechnicanAssignedItemsConfiguration.Property(o => o.FK_Item_Id).IsRequired();
            TechnicanAssignedItemsConfiguration.Property(o => o.SerialNo).IsRequired();
            TechnicanAssignedItemsConfiguration.Property(o => o.FK_AssignedFromTechnican_Id).IsRequired(false);
            TechnicanAssignedItemsConfiguration.Property(o => o.FK_AssignedToTechnican_Id).IsRequired();
            TechnicanAssignedItemsConfiguration.Ignore(o => o.Item);

        }
    }
}