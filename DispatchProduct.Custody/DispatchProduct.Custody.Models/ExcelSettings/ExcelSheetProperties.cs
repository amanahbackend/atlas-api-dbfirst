﻿namespace DispatchProduct.Custody.ExcelSettings
{
    public class ExcelSheetProperties
    {
        public string CategoryEN { get; set; }
        public string CategoryAR { get; set; }
        public string SerialNo { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
    }
}
