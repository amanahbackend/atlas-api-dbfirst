﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Custody.Entities;
using System;

namespace DispatchProduct.Custody.IEntities
{
    public interface IItem : IBaseLKPEntity
    {
         string SerialNo { get; set; }
         string DescriptionAR { get; set; }
         string DescriptionEN { get; set; }
         int FK_Category_Id { get; set; }
         bool IsAvailable { get; set; }
        Category Category { get; set; }
    }
}
