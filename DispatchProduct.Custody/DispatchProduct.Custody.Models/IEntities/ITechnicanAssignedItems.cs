﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Custody.Entities;
using System;

namespace DispatchProduct.Custody.IEntities
{
    public interface ITechnicanAssignedItems : IBaseEntity
    {
         int FK_Item_Id { get; set; }
         string SerialNo { get; set; }
         string FK_AssignedFromTechnican_Id { get; set; }
         string FK_AssignedToTechnican_Id { get; set; }
         bool IsReleased { get; set; }
         Item Item { get; set; }
    }
}
