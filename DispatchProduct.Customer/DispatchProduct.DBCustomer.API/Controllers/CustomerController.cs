﻿using BLL;
using DispatchProduct.DBFirstCustomer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilites.PaginatedItems;

namespace API.Controllers
{
    public class CustomerController : ApiController
    {
        public List<sto_customer> Get()
        {
            List<sto_customer> result;
            CustomerManager customerMngr = new CustomerManager();
            result = customerMngr.GetAll().ToList();
            return result;
        }
        public List<sto_customer> Get(int id)
        {
            List<sto_customer> result;
            CustomerManager customerMngr = new CustomerManager();
            result = customerMngr.GetAll().ToList();
            return result;
        }
        [Route("GetAllPaginated")]
        [HttpPost]
        public PaginatedItems<sto_customer> GetAllPaginated([FromBody]PaginatedItems<sto_customer> paginatedItems = null)
        {
            PaginatedItems<sto_customer> result;
            CustomerManager customerMngr = new CustomerManager();
            result = customerMngr.GetAllPaginated();
            return result;
        }
        public List<sto_customer> Search(string key)
        {
            List<sto_customer> result;
            CustomerManager customerMngr = new CustomerManager();
            result = customerMngr.Search(key).ToList();
            return result;
        }
    }
}
