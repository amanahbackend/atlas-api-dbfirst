﻿using DispatchProduct.DBFirstCustomer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CustomerManager : Repositry<sto_customer>
    {

        public CustomerManager()
        {

        }
        public CustomerManager(DbContext context)
            : base(context)
        {

        }
        public List<sto_customer> Search(string key)
        {
          return  GetAll().Where(cus =>

            cus.cust_tele1.ToLower().Contains(key.ToLower())
            ||
            cus.cust_tele2.ToLower().Contains(key.ToLower())
            ||
            cus.cust_tele3.ToLower().Contains(key.ToLower())
            ||
            cus.name.ToLower().Contains(key.ToLower())
            ||
            cus.name_e.ToLower().Contains(key.ToLower())
            ||
            cus.db_name.ToLower().Contains(key.ToLower())
            ||
            cus.cemail.ToLower().Contains(key.ToLower())
            ).ToList();
        }

    }
}