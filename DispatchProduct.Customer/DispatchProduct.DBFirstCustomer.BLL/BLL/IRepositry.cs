﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IRepositry<TEntity>
    {
        IQueryable<TEntity> GetAll();
        TEntity Get(params object[] id);
        TEntity Add(TEntity entity);
        bool Update (TEntity entity);
        bool Delete(TEntity entity);
    }
}
