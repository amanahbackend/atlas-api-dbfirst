﻿using Annotations;
using AnnotaionDAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DispatchProduct.DBFirstCustomer.Models;
using Utilites.PaginatedItems;

namespace BLL
{
    public class Repositry<T> : IRepositry<T> where T:class
    {
        private DbContext _context;

        public DbContext Context
        {
            get { return _context; }
        }
        private DbSet<T> _set;
        public Repositry()
            : this(new CustomerContext())
        {
        }
        public Repositry(DbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }
        public virtual IQueryable<T> GetAll()
        {
            return _set; 
        }

        public virtual T Get(params object[] id)
        {
            return _set.Find(id);
        }

        public virtual T Add(T entity)
        {
            T result = null;
            if (Validator.IsValid(entity))
            {


                entity= _set.Add(entity);
                if (_context.SaveChanges() > 0)
                {
                    result = entity;
                }
            }
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }
        public virtual void Add(IEnumerable<T> entityLst) {
            foreach (var entity in entityLst)
            {
                _set.Add(entity);
            }
            _context.SaveChanges();
        }
        public virtual bool Update(T entity)
        {
            bool result = false;
            if (Validator.IsValid(entity))
            {
                    _context.Entry<T>(entity).State = EntityState.Modified;
                    if (_context.SaveChanges() > 0)
                    {
                        result = true;
                    }
            }
             else
            {
               StringBuilder exceptionMsgs = new StringBuilder();
               List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
               foreach (var errmsg in errorMsgs)
               { exceptionMsgs.Append(errmsg);
                   exceptionMsgs.Append("/n");
               }
               throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }
        public virtual bool Update(IEnumerable<T> entityLst)
        {
            bool result = false;
             foreach (var entity in entityLst)
            {
            if (Validator.IsValid(entity))
            {
                _context.Entry<T>(entity).State = EntityState.Modified;
               
            }
            
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            if (_context.SaveChanges() > 0)
            {
                result = true;
            }
           }
            return result;
        }
        public virtual bool Delete(T entity)
        {
            _context.Entry<T>(entity).State = EntityState.Deleted;
            return _context.SaveChanges() > 0;
            
        }
        
        public virtual bool Delete(List<T> entitylst)
        {
            bool result = false;
            if (entitylst.Count > 0)
            {
                foreach (var entity in entitylst)
                {
                    Delete(entity);
                }
              result=  _context.SaveChanges() > 0;
            }
            return result;
        }
        public virtual void RollBack() {
        
        }
        public virtual void Commit()
        {
           
            
        }
        public virtual PaginatedItems<T> GetAllPaginated(PaginatedItems<T> paginatedItems=null)
        {
            try
            {
                if (paginatedItems == null)
                {
                    paginatedItems = new PaginatedItems<T>();
                    paginatedItems.PageNo =1;
                    paginatedItems.PageSize = 10;
                }
                    var count = _set.AsNoTracking().Count();
                    paginatedItems.Count = count;
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    paginatedItems.Data = _set.AsNoTracking().Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                paginatedItems.Data = _set.AsNoTracking().Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                        }
                        else
                        {
                            paginatedItems.Data = _set.AsNoTracking().ToList();
                        }
                    }
                
                return paginatedItems;

                //var data= new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, paginatedItems.PageSize, count, orders);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
