﻿using System.Collections.Generic;
namespace Utilites.PaginatedItemsViewModel
{
    public interface IPaginatedItemsViewModel<TEntity> 
    {
         int PageNo { get;  set; }

         int PageSize { get;  set; }

         long Count { get; set; }

         List<TEntity> Data { get; set; }
       
    }
}
