﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Identity.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class HealthCheckController :Controller
    {
        public HealthCheckController()
        {
        }
        [HttpGet(""), MapToApiVersion("1.0")]
        [HttpHead("")]
        public virtual IActionResult Ping()
        {
            return Ok("I'm fine");
        }
    }
}
