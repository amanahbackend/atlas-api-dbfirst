﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.Identity.API.Certificates;
using DispatchProduct.Identity.API.Controllers;
using DispatchProduct.Identity.API.Services;
using DispatchProduct.Identity.BLL.BLL.IManagers;
using DispatchProduct.Identity.BLL.BLL.Managers;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.BLL.Managers;
using DispatchProduct.Identity.Context;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Models.Entities;
using DispatchProduct.Identity.Settings;
using DnsClient;
using IdentityAuthority.Configs;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;

namespace DispatchProduct.Identity.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                 .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                 .AddEnvironmentVariables();
            Configuration = builder.Build();
        }



        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Identity.EFCore.MSSQL")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(
                    options => {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 1;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddIdentityServer();

           

            services.AddSingleton(provider => Configuration);
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            services.AddApiVersioning(o => o.ReportApiVersions = true);

            services.Configure<AppSettings>(Configuration);
            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, ApplicationDbContext>();
            services.AddScoped(typeof(IApplicationRole), typeof(ApplicationRole));
            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IPrivilgeManager), typeof(PrivilgeManager));
            services.AddScoped(typeof(IPrivilge), typeof(Privilge));
            services.AddScoped(typeof(IRolePrivilge), typeof(RolePrivilge));
            services.AddScoped(typeof(IRolePrivilgeManager), typeof(RolePrivilgeManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));
            services.AddScoped(typeof(IUserDeviceManager), typeof(UserDeviceManager));
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();


            var connectionString = Configuration["ConnectionString"];
            //var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
             var migrationsAssembly = "DispatchProduct.Identity.EFCore.MSSQL";
            // Adds IdentityServer
            services.AddIdentityServer(x => x.IssuerUri = "null")
                .AddSigningCredential(Certificate.Get())
                .AddAspNetIdentity<ApplicationUser>()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString, opts =>
                        opts.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString, opts =>
                         opts.MigrationsAssembly(migrationsAssembly));
                })
                .Services.AddTransient<IProfileService, IdentityProfileService>();
                //.Services.AddTransient<IProfileService, ProfileService>();
            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            app.UseCors("AllowAll");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            app.UseStaticFiles();
            //var options = new RewriteOptions()
            // .AddRedirectToHttps();
            //ConfigureAuth(app);
            //app.UseRewriter(options);
            // app.UseAuthentication();
            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            // Adds IdentityServer
            app.UseIdentityServer();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //app.UseConsulRegisterService();
        }

    }
}
