﻿using DispatchProduct.Identity.BLL.BLL.IManagers;
using DispatchProduct.Identity.Context;
using DispatchProduct.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DispatchProduct.Identity.BLL.BLL.Managers
{
    public class UserDeviceManager : IUserDeviceManager
    {
        private readonly ApplicationDbContext _dbContext;
        public UserDeviceManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddIfNotExist(UserDevice userDevice)
        {
            var count = _dbContext.UserDevices.Count(x => x.UserId == userDevice.UserId);
            if (count > 0)
            {
                var isDeviceExist = _dbContext.UserDevices.Count(x =>
                                        x.UserId == userDevice.UserId &&
                                        x.DeveiceId == userDevice.DeveiceId) > 0;
                if (!isDeviceExist)
                {
                    _dbContext.UserDevices.Add(userDevice);
                }
            }
            else
            {
                _dbContext.UserDevices.Add(userDevice);
            }
            _dbContext.SaveChanges();
        }

        public List<UserDevice> GetByUserId(string userId)
        {
            var userDevices = _dbContext.UserDevices.Where(x => x.UserId == userId).ToList();
            return userDevices;
        }

        public bool DeleteDevice(string token)
        {
            var userDevice = _dbContext.UserDevices.FirstOrDefault(x => x.DeveiceId.Equals(token));
            if (userDevice != null)
            {
                _dbContext.Remove(userDevice);
                return _dbContext.SaveChanges() > 0;
            }
            return false;
        }
    }
}
