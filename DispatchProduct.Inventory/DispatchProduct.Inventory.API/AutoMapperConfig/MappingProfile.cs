﻿using AutoMapper;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Category, CategoryViewModel>();
            CreateMap<CategoryViewModel, Category>();

            CreateMap<FilterTechnicanViewModel, FilterTechnican>();
            CreateMap<FilterTechnican, FilterTechnicanViewModel>();

            CreateMap<TransferedItems, TransferedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TransferedItemsViewModel, TransferedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TechnicanAssignedItemsViewModel, TechnicanAssignedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<TechnicanUsedItems, TechnicanUsedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
            .ForMember(dest => dest.Order, opt => opt.Ignore());

            CreateMap<TechnicanUsedItemsViewModel, TechnicanUsedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<Item, ItemViewModel>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
            CreateMap<ItemViewModel, Item>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
        }
    }
}
