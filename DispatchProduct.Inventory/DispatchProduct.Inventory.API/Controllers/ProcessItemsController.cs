﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Utilites.ExcelToGenericList;
using Utilites.ExcelToGenericList.Test;
using Utilites.UploadFile;
using Utilites.ProcessingResult;
using Microsoft.Extensions.Options;
using DispatchProduct.Inventory.API.Settings;
using DispatchProduct.Inventory.ExcelSettings;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.BLL.IManagers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Inventory.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class ProcessItemController : Controller
    {
        public IItemManager manger;
        public readonly IMapper mapper;
        InventoryAppSettings settings;
        ExcelSheetProperties excelSheetProperties;
        IExcelItemsManager excelSalaryManager;
        public ProcessItemController(IItemManager _manger, IMapper _mapper, IOptions<InventoryAppSettings> _settings, IOptions<ExcelSheetProperties> _excelPropsettings, IExcelItemsManager _excelSalaryManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            settings = _settings.Value;
            excelSheetProperties = _excelPropsettings.Value;
            excelSalaryManager = _excelSalaryManager;
        }
        // GET: api/values
        
        [HttpPost]
        [Route("UpdateSalaries")]
        public IActionResult UpdateSalaries([FromBody]UploadFile file)
        {
            if (settings.ExclPath == null)
            {
                settings.ExclPath = "SalarySheet";
            }
            var  result=excelSalaryManager.Process(settings.ExclPath, file, excelSheetProperties);
            if (result.IsSucceeded)
            {
                return Ok(result.returnData);
            }
            else {
                return Ok(result.Exception);
            }
        }

       
    }
}
