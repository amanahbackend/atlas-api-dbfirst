﻿using DispatchProduct.HttpClient;
using DispatchProduct.Inventory.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Inventory.API.ServicesCommunication
{
    public interface IOrderService : IDefaultHttpClientCrud<OrderServiceSetting, OrderViewModel, OrderViewModel>
    {
        
    }
}
