﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
    public class OrderServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }  
        
    }
}
