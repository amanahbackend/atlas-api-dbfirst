﻿using DispatchProduct.Inventory.API.ViewModel;

namespace DispatchProduct.Inventory.API.ServicesViewModels
{
    public class ComplainViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Note { get; set; }

        public int FK_Customer_Id { get; set; }

        public CustomerViewModel Customer { get; set; }
    }
}
