﻿using DispatchProduct.Inventory.API.ViewModel;

namespace DispatchProduct.Inventory.API.ServicesViewModels
{
    public class OrderStatusViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
