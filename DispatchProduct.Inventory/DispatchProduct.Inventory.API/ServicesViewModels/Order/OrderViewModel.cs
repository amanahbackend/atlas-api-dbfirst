﻿using DispatchProduct.Inventory.API.ViewModel;
using System;

namespace DispatchProduct.Inventory.API.ServicesViewModels
{
    public class OrderViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public int FK_Customer_Id { get; set; }

        public string TechnicanName { get; set; }

        public string DispatcherName { get; set; }

        public int FK_Contract_Id { get; set; }

        public int FK_Location_Id { get; set; }

        public string QuotationRefNo { get; set; }

        public double Price { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public int FK_OrderType_Id { get; set; }

        public int FK_OrderStatus_Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }

        public OrderProblemViewModel OrderProblem { get; set; }

        public DateTime PreferedVisitTime { get; set; }

        public CustomerViewModel Customer { get; set; }

        public LocationViewModel Location { get; set; }

        public ContractViewModel Contract { get; set; }

        public OrderPriorityViewModel OrderPriority { get; set; }

        public OrderStatusViewModel OrderStatus { get; set; }

        public OrderTypeViewModel OrderType { get; set; }

        public string Note { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string FK_Technican_Id { get; set; }

        public string FK_Dispatcher_Id { get; set; }

        public string SignatureURL { get; set; }

        public string SignaturePath { get; set; }

        public string SignatureContractURL { get; set; }

        public string SignatureContractPath { get; set; }
    }
}