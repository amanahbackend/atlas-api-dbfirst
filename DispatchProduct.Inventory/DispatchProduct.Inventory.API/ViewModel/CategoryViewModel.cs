﻿namespace DispatchProduct.Inventory.API.ViewModel
{
    public class CategoryViewModel : BaseEntityViewModel
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}
