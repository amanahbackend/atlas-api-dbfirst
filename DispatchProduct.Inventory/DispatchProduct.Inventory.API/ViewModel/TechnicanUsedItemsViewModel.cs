﻿using DispatchProduct.Inventory.API.ServicesViewModels;

namespace DispatchProduct.Inventory.API.ViewModel
{
    public class TechnicanUsedItemsViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public ItemViewModel Item { get; set; }

        public int Amount { get; set; }

        public string FK_Technican_Id { get; set; }

        public int FK_Order_Id { get; set; }

        public OrderViewModel Order { get; set; }

        public bool IsReleased { get; set; }
    }
}