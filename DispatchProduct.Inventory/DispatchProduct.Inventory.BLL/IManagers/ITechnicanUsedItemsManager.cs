﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITechnicanUsedItemsManager : IRepositry<TechnicanUsedItems>
    {
        ProcessResult<List<TechnicanUsedItems>> AddUsedItems(List<TechnicanUsedItems> items);

        ProcessResult<TechnicanUsedItems> AddUsedItem(TechnicanUsedItems item);

        List<TechnicanUsedItems> GetUsedItemsByTechnican(string technicanId);

        ProcessResult<bool> ReleaseUsedItem(int usedItemId);

        List<TechnicanUsedItems> Filter(FilterTechnican filter);
    }
}
