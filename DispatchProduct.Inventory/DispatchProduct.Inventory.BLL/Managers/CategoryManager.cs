﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class CategoryManager:Repositry<Category>, ICategoryManager
    {
        public CategoryManager(InventoryDbContext context)
            : base(context)
        {

        }
    }
}
