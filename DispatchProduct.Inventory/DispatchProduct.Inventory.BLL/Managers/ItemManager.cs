﻿using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.Context;
using DispatchingProduct.Inventory.BLL.IManagers;
using System.Linq;
using Utilites.ProcessingResult;
using System.Reflection;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class ItemManager : Repositry<Item>, IItemManager
    {
        public ItemManager(InventoryDbContext context)
            : base(context)
        {

        }
        public Item GetByCode(string Code)
        {
            Item result = null;
            if (Code != null)
            {
                result= Get(itm => itm.Code == Code);
            }
            return result;
        }
        
        public ProcessResult<bool> DecreaseAssignedAmount(int amount, int itemId)
        {
            ProcessResult<bool> processResult = new ProcessResult<bool>();
            processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            Item entity = GetAll().Where(itm => itm.Id == itemId).FirstOrDefault();
            if (entity != null)
            {
                if (entity.Quantity >= amount)
                {
                    entity.Quantity -= amount;
                    processResult.IsSucceeded = processResult.returnData = Update(entity);
                    if (!processResult.IsSucceeded)
                        processResult.Message = "Failed in update operation";
                }
                else
                {
                    processResult.IsSucceeded = false;
                    processResult.Message = "decreased amount greater than inventory amount";
                }
            }
            else
            {
                processResult.IsSucceeded = false;
                processResult.Message = "item needed to decrease not found";
            }
            return processResult;
        }
    }
}
