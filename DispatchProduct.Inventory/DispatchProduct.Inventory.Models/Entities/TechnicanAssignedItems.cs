﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.IEntities;

namespace DispatchProduct.Inventory.Entities
{
    public class TechnicanAssignedItems : BaseEntity, ITechnicanAssignedItems, IBaseEntity
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public Item Item { get; set; }

        public int CurrentAmount { get; set; }

        public int TotalAmount { get; set; }

        public int TotalUsed { get; set; }

        public string FK_Technican_Id { get; set; }
    }
}
