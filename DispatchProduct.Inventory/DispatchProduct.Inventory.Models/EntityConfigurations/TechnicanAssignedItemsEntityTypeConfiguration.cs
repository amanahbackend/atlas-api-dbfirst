﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Inventory.EntityConfigurations
{
    public class TechnicanAssignedItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<TechnicanAssignedItems>, IEntityTypeConfiguration<TechnicanAssignedItems>
    {
        public void Configure(EntityTypeBuilder<TechnicanAssignedItems> TechnicanAssignedItemsConfiguration)
        {
            base.Configure(TechnicanAssignedItemsConfiguration);

            TechnicanAssignedItemsConfiguration.ToTable("TechnicanAssignedItems");

            TechnicanAssignedItemsConfiguration.HasKey(o => o.Id);

            TechnicanAssignedItemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("TechnicanAssignedItemsseq");
            TechnicanAssignedItemsConfiguration.Property(o => o.ItemId).IsRequired();
            TechnicanAssignedItemsConfiguration.Property(o => o.CurrentAmount).IsRequired();
            TechnicanAssignedItemsConfiguration.Property(o => o.TotalAmount).IsRequired();
            TechnicanAssignedItemsConfiguration.Property(o => o.TotalUsed).IsRequired();
            TechnicanAssignedItemsConfiguration.Property(o => o.FK_Technican_Id).IsRequired();
            TechnicanAssignedItemsConfiguration.Ignore(o => o.Item);

        }
    }
}