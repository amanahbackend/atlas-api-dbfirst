﻿using AutoMapper;
using DispatchProduct.LocationModule.API;
using DispatchProduct.LocationModule.API.ViewModel;
using DispatchProduct.LocationModule.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.LocationModule.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<LocationViewModel, Entities.Location>();
            CreateMap<Entities.Location, LocationViewModel>();


        }
    }
}
