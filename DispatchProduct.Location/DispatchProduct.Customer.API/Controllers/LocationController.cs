﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Utilites.PACI;
using Microsoft.Extensions.Configuration;
using DispatchProduct.LocationModule.API.ViewModel;
using DispatchProduct.LocationModule.Entities;
using DispatchProduct.LocationModule.BLL.IManagers;
using Microsoft.Extensions.Options;
using DispatchProduct.LocationModule.Entities.LocationSettings;
using Utilities.Utilites.PACI;
using Microsoft.AspNetCore.Authorization;
using IdentityServer4.AccessTokenValidation;
using Utilites.UploadFile;
using Microsoft.AspNetCore.Hosting;
using Utilites;
using Utilities;
using Utilites.ExcelToGenericList;
using DispatchProduct.Location.API.ViewModel;
using DispatchProduct.Location.API.ServicesCommunication.Contract;
using System.IO;

namespace DispatchProduct.LocationModule.API.Controllers
{
    [Route("api/Location")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class LocationController : Controller
    {
        private ILocationManager _manger;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IContractService _contractService;
        public LocationController(ILocationManager manger, IMapper mapper, IHostingEnvironment hostingEnv,
            IContractService contractService)
        {
            _contractService = contractService;
            _hostingEnv = hostingEnv;
            _manger = manger;
            _mapper = mapper;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public LocationViewModel Get([FromRoute]int id)
        {
            LocationViewModel result = new LocationViewModel();
            Entities.Location entityResult = new Entities.Location();
            entityResult = _manger.Get(id);
            result = _mapper.Map<Entities.Location, LocationViewModel>(entityResult);
            return result;
        }


        [Route("GetByCustomerId/{id}")]
        [HttpGet]
        public LocationViewModel GetByCustomerId([FromRoute] int id)
        {
            LocationViewModel result = new LocationViewModel();
            Entities.Location entityResult = new Entities.Location();
            entityResult = _manger.Get(id);
            result = _mapper.Map<Entities.Location, LocationViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<LocationViewModel> Get()
        {
            List<LocationViewModel> result = new List<LocationViewModel>();
            List<Entities.Location> entityResult = new List<Entities.Location>();
            entityResult = _manger.GetAll().ToList();
            result = _mapper.Map<List<Entities.Location>, List<LocationViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]LocationViewModel model)
        {
            LocationViewModel result = new LocationViewModel();
            Entities.Location entityResult = new Entities.Location();
            entityResult = _mapper.Map<LocationViewModel, Entities.Location>(model);
            entityResult = _manger.Add(entityResult);

            result = _mapper.Map<Entities.Location, LocationViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]LocationViewModel model)
        {
            bool result = false;
            Entities.Location entityResult = new Entities.Location();
            entityResult = _mapper.Map<LocationViewModel, Entities.Location>(model);

            result = _manger.Update(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            Entities.Location entityResult = new Entities.Location();
            LocationViewModel model = new LocationViewModel();
            model.Id = id;
            entityResult = _mapper.Map<LocationViewModel, Entities.Location>(model);
            result = _manger.Delete(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        [HttpGet]
        [Route("GetLocationByPaci")]
        public IActionResult GetLocationByPaci(string paciNumber)
        {
            var result = _manger.GetLocationByPACI(Convert.ToInt32(paciNumber.Replace("\"", "")));
            if (result == null)
            {
                return Ok(GetNotFoundLocationByPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetCoordinatesByPaci/{paciNumber}")]
        public Point GetCoordinatesByPaci([FromRoute] string paciNumber)
        {
            return _manger.GetCoordinatesByPaci(Convert.ToInt32(paciNumber.Replace("\"", "")));
        }

        [HttpPost]
        [Route("GetCoordinatesByStreet_Block")]
        public Point GetCoordinatesByStreet_Block([FromBody] PointInputViewModel model)
        {
            return _manger.GetCoordinatesByStreet_Block(model.Street, model.Block);
        }

        [HttpGet]
        [Route("GetAllGovernorates")]
        public IActionResult GetAllGovernorates()
        {
            var result = _manger.GetAllGovernorates();
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAreas")]
        public IActionResult GetAreas(int? govId)
        {
            var result = _manger.GetAreas(govId);
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBlocks")]
        public IActionResult GetBlocks(int? areaId)
        {
            var result = _manger.GetBlocks(areaId);
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStreets")]
        public IActionResult GetStreets(int? govId, int? areaId, string blockName)
        {
            var result = _manger.GetStreets(govId, areaId, blockName);
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetLocationByCustomerId")]
        public IActionResult GetLocationByCustomerId(int customerId)
        {
            var result = _manger.GetByCustomerId(customerId);
            return Ok(result);
        }

        [HttpPost, Route("UploadFile")]
        public async Task<IActionResult> UploadFile([FromBody]UploadFile file)
        {
            var fileExt = StringUtilities.GetFileExtension(file.FileContent);
            file.FileName = $"Loctions_{DateTime.Now.Ticks}.{fileExt}";
            UploadExcelFileManager fileManager = new UploadExcelFileManager();
            var path = $@"{_hostingEnv.WebRootPath}\Import\";
            var addFileResult = fileManager.AddFile(file, path);
            var result = await AddFromFile($@"{addFileResult.returnData}\{file.FileName}");
            return Ok(result);
        }

        private async Task<List<LocationViewModel>> AddFromFile(string filePath)
        {
            var locations = new List<LocationViewModel>();
            var models = ExcelReader.GetDataToList(filePath, GetItems);
            string fileName = Path.GetFileName(filePath);
            int i = 0;
            foreach (var item in models)
            {
                i++;
                var location = new LocationViewModel
                {
                    PACINumber = item.PACI,
                    Latitude = Convert.ToDouble(item.Lat),
                    Longitude = Convert.ToDouble(item.Long)
                };
                try
                {
                    location.Fk_Customer_Id = (int)await _contractService.GetCustomerByContractCode(item.Code);
                    LogResult($"{i} - {DateTime.UtcNow.ToString("")} : Contract Code : {item.Code} - Succeed", fileName);
                    LogResult("=========================================================", fileName);
                }
                catch (Exception ex)
                {
                    LogResult($"{i} - {DateTime.UtcNow.ToString("")} : Contract Code : {item.Code} - Failed : {ex.Message}", fileName);
                    LogResult("=========================================================", fileName);
                }
                locations.Add(location);
            }
            var entities = _mapper.Map<List<LocationViewModel>, List<Entities.Location>>(locations);
            var result = _manger.Add(entities);
            var locResult = _mapper.Map<List<Entities.Location>, List<LocationViewModel>>(result);
            return locResult;
        }

        private LocationFromFileViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var model = new LocationFromFileViewModel();

            var location = new LocationFromFileViewModel()
            {
                PACI = rowData[columnNames.IndexFor(nameof(model.PACI))],
                Lat = rowData[columnNames.IndexFor(nameof(model.Lat))],
                Long = rowData[columnNames.IndexFor(nameof(model.Long))],
                Code = rowData[columnNames.IndexFor(nameof(model.Code))]
            };
            return location;
        }

        public object GetNotFoundInPaci()
        {
            var result = new[] { new { Id = 1, Name = "Not Found In Paci" } };
            return result;
        }
        public object GetNotFoundLocationByPaci()
        {
            var Governorate = new { Id = 1, Name = "Not Found In Paci" };
            var Area = new { Id = 1, Name = "Not Found In Paci" };
            var Block = new { Id = 1, Name = "Not Found In Paci" };
            var Street = new { Id = 1, Name = "Not Found In Paci" };

            var result = new
            {
                Governorate = Governorate,
                Area = Area,
                Block = Block,
                Street = Street
            };
            return result;
        }

        public void LogResult(string resultMessage, string excelFileName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            //string dir = @"D:\Amanah\Dev\Migration"; 
            string dir = $@"{_hostingEnv.WebRootPath}\Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + @"\" + "Request_Log_" + excelFileName + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }
    }
}