﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.LocationModule.API.Seed;
using DispatchProduct.LocationModule.Models;
using Microsoft.Extensions.Options;
using DispatchProduct.LocationModule.API.Settings;

namespace DispatchProduct.LocationModule.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<LocationDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<LocationDbContextSeed>>();
                var settings = services.GetService<IOptions<CustomerAppSettings>>();
                new LocationDbContextSeed()
                    .SeedAsync(context, env, logger, settings)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
                   WebHost.CreateDefaultBuilder(args)
                       .UseKestrel()
                       .UseContentRoot(Directory.GetCurrentDirectory())
                       .UseIISIntegration()
                       .UseSetting("detailedErrors", "true")
                       .CaptureStartupErrors(true)
                       .UseStartup<Startup>()
                       .ConfigureLogging((hostingContext, builder) =>
                       {
                           builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                           builder.AddConsole();
                           builder.AddDebug();
                       })
                       .Build();
    }
}
