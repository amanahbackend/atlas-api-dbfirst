﻿
using DispatchProduct.LocationModule.API.Settings;
using DispatchProduct.LocationModule.Models;
using DispatchProduct.LocationModule.Entities;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.LocationModule.API.Seed
{
    public class LocationDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(LocationDbContext context, IHostingEnvironment env,
            ILogger<LocationDbContextSeed> logger, IOptions<CustomerAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
              

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                   
                }
             
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for LocationDbContextSeed");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
    }
}
