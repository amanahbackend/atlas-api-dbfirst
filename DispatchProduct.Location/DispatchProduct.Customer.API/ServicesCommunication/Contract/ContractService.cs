﻿using DispatchProduct.HttpClient;
using DispatchProduct.Location.API.ServicesSettings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Location.API.ServicesCommunication.Contract
{
    public class ContractService : DefaultHttpClientCrud<ContractServiceSetting, string, decimal>, IContractService
    {
        ContractServiceSetting settings;

        public ContractService(IOptions<ContractServiceSetting> _settings) : base(_settings.Value)
        {
            settings = _settings.Value;
        }

        public async Task<decimal> GetCustomerByContractCode(string code)
        {
            var requesturi = $"{settings.Uri}/{settings.GetCustomerByContractCode}/{code}";
            return await GetByUri(requesturi);
        }
    }
}
