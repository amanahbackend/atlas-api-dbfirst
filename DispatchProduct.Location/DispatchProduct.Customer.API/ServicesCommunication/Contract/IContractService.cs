﻿using DispatchProduct.HttpClient;
using DispatchProduct.Location.API.ServicesSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Location.API.ServicesCommunication.Contract
{
    public interface IContractService: IDefaultHttpClientCrud<ContractServiceSetting, string, decimal>
    {
        Task<decimal> GetCustomerByContractCode(string code);
    }
}
