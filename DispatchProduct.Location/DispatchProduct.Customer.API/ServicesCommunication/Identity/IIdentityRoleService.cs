﻿using DispatchProduct.LocationModule.API.ServicesViewModels;
using DispatchProduct.LocationModule.API.Settings;
using DispatchProduct.LocationModule.API.ViewModel;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.LocationModule.API.ServicesCommunication.Identity
{
    public interface IIdentityRoleService : IDefaultHttpClientCrud<IdentityServiceSetting, ApplicationUserViewModel, ApplicationUserViewModel>
    {
        Task<ApplicationUserViewModel> GetUserRoles(string userName, string authHeader = "");
    }
}
