﻿using DispatchProduct.LocationModule.API.ServicesViewModels;
using DispatchProduct.LocationModule.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.LocationModule.API.ServicesCommunication.Identity
{
    public class IdentityRoleService : DefaultHttpClientCrud<IdentityServiceSetting, ApplicationUserViewModel, ApplicationUserViewModel>, IIdentityRoleService
    {
        IdentityServiceSetting settings;
        public IdentityRoleService(IOptions<IdentityServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<ApplicationUserViewModel> GetUserRoles( string userName,string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.UserRolesVerb}/{userName}";
            return await GetByUri(requesturi, authHeader);
        }
    }
}
