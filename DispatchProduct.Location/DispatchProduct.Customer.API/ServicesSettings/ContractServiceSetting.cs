﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Location.API.ServicesSettings
{
    public class ContractServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }

        public string GetCustomerByContractCode
        {
            get; set;
        }
    }
}
