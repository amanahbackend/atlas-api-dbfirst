﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.LocationModule.API.Settings
{
    public class IdentityServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }

        public string UserRolesVerb
        {
            get; set;
        }
        
    }
}
