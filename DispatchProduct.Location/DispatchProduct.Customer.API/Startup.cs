﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Repoistry;
using DispatchProduct.LocationModule.Models;
using DispatchProduct.LocationModule.API.Settings;
using Swashbuckle.AspNetCore.Swagger;
using DispatchProduct.LocationModule.IEntities;
using DispatchProduct.LocationModule.Entities;
using DispatchProduct.LocationModule.BLL.IManagers;
using DispatchProduct.LocationModule.BLL.Managers;
using DispatchProduct.LocationModule.Entities.LocationSettings;
using DispatchProduct.LocationModule.API.ServicesCommunication.Identity;
using DispatchProduct.Location.API.ServicesSettings;
using DispatchProduct.Location.API.ServicesCommunication.Contract;

namespace DispatchProduct.LocationModule.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddDbContext<LocationDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Location.EFCore.MSSQL")));

            services.AddOptions();
            OptionsServiceCollectionExtensions.AddOptions(services);
            services.Configure<CustomerAppSettings>(Configuration);
            services.Configure<LocationSettings>(Configuration.GetSection("LocationSettings"));
            services.Configure<IdentityServiceSetting>(Configuration.GetSection("IdentityServiceSetting"));
            services.Configure<ContractServiceSetting>(Configuration.GetSection("ContractServiceSetting"));



            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Customer API",
                        Description = "Customer  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "Customer", "Customer API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();

            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, LocationDbContext>();
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(ILocation), typeof(Entities.Location));
            services.AddScoped(typeof(ILocationManager), typeof(LocationManager));
            services.AddScoped(typeof(IIdentityRoleService), typeof(IdentityRoleService));
            services.AddScoped(typeof(IContractService), typeof(ContractService));

            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
