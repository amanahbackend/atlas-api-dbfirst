﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Location.API.ViewModel
{
    public class LocationFromFileViewModel
    {
        public string Code { get; set; }
        public string PACI { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
    }
}
