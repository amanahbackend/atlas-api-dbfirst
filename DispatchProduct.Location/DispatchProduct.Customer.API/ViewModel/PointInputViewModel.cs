﻿namespace DispatchProduct.LocationModule.API.ViewModel
{
    public class PointInputViewModel : BaseEntityViewModel
    {
        public string Block { get; set; }

        public string Street { get; set; }
    }
}