﻿using DispatchProduct.LocationModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using Utilites.PACI;
using Utilities.Utilites.PACI;

namespace DispatchProduct.LocationModule.BLL.IManagers
{
    public interface ILocationManager : IRepositry<Location>
    {
        new Location Add(Location entity);

        bool DeleteByCustomerId(int customerId);

        List<Location> GetByCustomerId(int customerId);

        PACIHelper.PACIInfo GetLocationByPACI(int? paciNumber);

        List<PACIHelper.DropPACI> GetAllGovernorates();

        List<PACIHelper.DropPACI> GetAreas(int? govId);

        List<PACIHelper.DropPACI> GetStreets(int? govId, int? areaId, string blockName);

        List<PACIHelper.DropPACI> GetBlocks(int? areaId);

        new List<Location> Add(List<Location> entity);

        Point GetCoordinatesByPaci(int? paciNumber);

        Point GetCoordinatesByStreet_Block(string street, string Block);
    }
}