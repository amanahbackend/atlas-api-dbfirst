﻿using DispatchProduct.LocationModule.Entities;
using DispatchProduct.LocationModule.EntityConfigurations;
using DispatchProduct.LocationModule.IEntities;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.LocationModule.Models
{
    public class LocationDbContext : DbContext
    {
        public DbSet<Location> Location { get; set; }
       

        public LocationDbContext(DbContextOptions<LocationDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LocationEntityTypeConfiguration());
        }
    }
}
