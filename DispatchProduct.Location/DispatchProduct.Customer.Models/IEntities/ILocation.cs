﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.LocationModule.Entities;

namespace DispatchProduct.LocationModule.IEntities
{
    public interface ILocation : IBaseEntity
    {
        int Id { get; set; }

        string PACINumber { get; set; }

        string Governorate { get; set; }

        string Area { get; set; }

        string Block { get; set; }

        string Street { get; set; }

        string AddressNote { get; set; }

        double Latitude { get; set; }

        double Longitude { get; set; }

    }
}

