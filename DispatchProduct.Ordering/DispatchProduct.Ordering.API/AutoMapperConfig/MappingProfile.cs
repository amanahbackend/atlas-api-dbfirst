﻿using AutoMapper;
using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>();
            CreateMap<FilterOrderByDispatcher, FilterOrderViewModelByDispatcher>();

            CreateMap<OrderPriority, OrderPriorityViewModel>();
            CreateMap<OrderPriorityViewModel, OrderPriority>();

            CreateMap<AreaProblems, OrderDistributionCriteriaViewModel>()
              .ForMember(dest => dest.Id, opt => opt.Ignore())
              .ForMember(dest => dest.OrderProblem, opt => opt.Ignore())
              .ForMember(dest => dest.Dispatcher, opt => opt.Ignore())
              .ForMember(dest => dest.FK_OrderProblem_Id, opt => opt.Ignore())
              .ForMember(dest => dest.FK_Dispatcher_Id, opt => opt.Ignore());
            CreateMap<AreaProblems, IList<OrderDistributionCriteriaViewModel>>()
                .ConstructUsing(areaProb => areaProb.FK_OrderProblem_Ids.Select(probId => CreateOrderDistributionCriteriaViewModel(areaProb, probId)).ToList());


            CreateMap<OrderMultiDistributionCriteriaViewModel, OrderDistributionCriteria>()
              .ForMember(dest => dest.OrderProblem, opt => opt.Ignore())
              .ForMember(dest => dest.Area, opt => opt.Ignore())
              .ForMember(dest => dest.Governorate, opt => opt.Ignore())
              .ForMember(dest => dest.FK_OrderProblem_Id, opt => opt.Ignore())
              .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<OrderMultiDistributionCriteriaViewModel, IList<OrderDistributionCriteria>>()
                .ConstructUsing(ordMultiCrit => CreateOrderDistributionCriteriaViewModel(ordMultiCrit));


            CreateMap<AssignedMultiTechnicansViewModel, AssignedTechnicans>()
             .ForMember(dest => dest.FK_Technican_Id, opt => opt.Ignore());
            CreateMap<AssignedMultiTechnicansViewModel, IList<AssignedTechnicans>>()
                .ConstructUsing(x => x.FK_Technicans_Id.Select(y => CreateAssignedTechnicansViewModel(x, y)).ToList());

            CreateMap<CUST_VM, CustomerViewModel>()
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.name_en))
                 .ForMember(dest => dest.Id, opt => opt.Ignore())
                 .ForMember(dest => dest.CivilId, opt => opt.MapFrom(src => src.civil_id))
                 .ForMember(dest => dest.CustomerPhoneBook, opt => opt.Ignore())
                 .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.address))

                 .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                 .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                 .ForMember(dest => dest.FK_DeletedBy_Id, opt => opt.Ignore())
                 .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
                 .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                 .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
                 .ForMember(dest => dest.DeletedDate, opt => opt.Ignore())

                 .ForMember(dest => dest.Complains, opt => opt.Ignore())
                 .ForMember(dest => dest.Locations, opt => opt.Ignore())
                 .ForMember(dest => dest.FK_PhoneType_Id, opt => opt.Ignore())
                 .ForMember(dest => dest.FK_CallType_Id, opt => opt.Ignore())
                 .ForMember(dest => dest.FK_CustomerType_Id, opt => opt.Ignore())
                 .ForMember(dest => dest.Remarks, opt => opt.Ignore());


            CreateMap<Order, OrderViewModel>()
                .ForMember(dest => dest.OrderPriority, opt => opt.MapFrom(src => src.OrderPriority))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType))
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus))
                .ForMember(dest => dest.LstOrderProgress, opt => opt.MapFrom(src => src.LstOrderProgress))
                .ForMember(dest => dest.Customer, opt => opt.Ignore())
                .ForMember(dest => dest.Location, opt => opt.Ignore())
                .ForMember(dest => dest.TechnicanName, opt => opt.Ignore())
                .ForMember(dest => dest.DispatcherName, opt => opt.Ignore())
                .ForMember(dest => dest.Contract, opt => opt.Ignore());
            CreateMap<OrderViewModel, Order>()
                .ForMember(dest => dest.OrderPriority, opt => opt.MapFrom(src => src.OrderPriority))
                .ForMember(dest => dest.LstOrderProgress, opt => opt.MapFrom(src => src.LstOrderProgress))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType))
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus));

            CreateMap<OrderStatus, OrderStatusViewModel>();
            CreateMap<OrderStatusViewModel, OrderStatus>();

            CreateMap<OrderProblem, OrderProblemViewModel>();
            CreateMap<OrderProblemViewModel, OrderProblem>();

            CreateMap<OrderDistributionCriteria, OrderDistributionCriteriaViewModel>()
                 .ForMember(dest => dest.OrderProblem, opt => opt.MapFrom(src => src.OrderProblem))
                 .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Dispatcher));
            CreateMap<OrderDistributionCriteriaViewModel, OrderDistributionCriteria>()
                .ForMember(dest => dest.OrderProblem, opt => opt.MapFrom(src => src.OrderProblem))
                .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Dispatcher));

            CreateMap<OrderProgress, OrderProgressViewModel>()
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.TechnicanName, opt => opt.Ignore())
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order))
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.ProgressStatus));
            CreateMap<OrderProgressViewModel, OrderProgress>()
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order))
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.ProgressStatus));

            CreateMap<ProgressStatus, ProgressStatusViewModel>()
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus));
            CreateMap<ProgressStatusViewModel, ProgressStatus>()
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus));

            CreateMap<OrderType, OrderTypeViewModel>();
            CreateMap<OrderTypeViewModel, OrderType>();

            CreateMap<ApplicationUser, ApplicationUserViewModel>()
                .ForMember(dest => dest.RoleNames, opt => opt.MapFrom(src => src.RoleNames));
            CreateMap<ApplicationUserViewModel, ApplicationUser>()
                .ForMember(dest => dest.RoleNames, opt => opt.MapFrom(src => src.RoleNames));

            CreateMap<Technican, TechnicanViewModel>();
            CreateMap<TechnicanViewModel, Technican>();

            CreateMap<ApplicationUserViewModel, Technican>()
                .ForMember(dest => dest.Orders, opt => opt.Ignore());
            CreateMap<Technican, ApplicationUserViewModel>();

            CreateMap<ApplicationUserViewModel, Dispatcher>()
               .ForMember(dest => dest.Orders, opt => opt.Ignore());
            CreateMap<Dispatcher, ApplicationUserViewModel>();

            CreateMap<DispatcherViewModel, Dispatcher>();
            CreateMap<Dispatcher, DispatcherViewModel>();

            CreateMap<PreventiveMaintainenceScheduleViewModel, Order>()
                .ForMember(dest => dest.FK_Contract_Id, opt => opt.MapFrom(src => src.FK_Contract_Id))
                .ForMember(dest => dest.FK_Customer_Id, opt => opt.MapFrom(src => src.FK_Customer_Id))
                .ForMember(dest => dest.FK_Location_Id, opt => opt.MapFrom(src => src.FK_Location_Id))
                .ForMember(dest => dest.FK_OrderPriority_Id, opt => opt.MapFrom(src => src.FK_OrderPriority_Id))
                .ForMember(dest => dest.FK_OrderType_Id, opt => opt.MapFrom(src => src.FK_OrderType_Id))
                .ForMember(dest => dest.FK_OrderProblem_Id, opt => opt.MapFrom(src => src.FK_OrderProblem_Id))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.OrderDate))

                //.ForMember(dest => dest.QuotationRefNo, opt => opt.Ignore())
                .ForMember(dest => dest.Area, opt => opt.Ignore())
                 .ForMember(dest => dest.SignatureURL, opt => opt.Ignore())
                .ForMember(dest => dest.SignaturePath, opt => opt.Ignore())
                .ForMember(dest => dest.SignatureContractURL, opt => opt.Ignore())
                .ForMember(dest => dest.SignatureContractPath, opt => opt.Ignore())
                .ForMember(dest => dest.SignatureContractPath, opt => opt.Ignore())
                .ForMember(dest => dest.EndDate, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Price, opt => opt.Ignore())
                .ForMember(dest => dest.Code, opt => opt.Ignore())
                .ForMember(dest => dest.OrderPriority, opt => opt.Ignore())
                .ForMember(dest => dest.OrderType, opt => opt.Ignore())
                .ForMember(dest => dest.OrderProblem, opt => opt.Ignore())
                .ForMember(dest => dest.Note, opt => opt.Ignore())
                .ForMember(dest => dest.FK_OrderStatus_Id, opt => opt.Ignore())
                .ForMember(dest => dest.OrderStatus, opt => opt.Ignore())
                .ForMember(dest => dest.PreferedVisitTime, opt => opt.Ignore())
                .ForMember(dest => dest.LstOrderProgress, opt => opt.Ignore())
                .ForMember(dest => dest.FK_Technican_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_Dispatcher_Id, opt => opt.Ignore());


            this.CreateMap<FilteredOrderViewModel, OrderViewModel>()
                .AfterMap(((src, dst) => Mapper.Map(src.Order, dst)))
                .ForAllOtherMembers((dest => dest.Ignore()));
            this.CreateMap<OrderViewModel, FilteredOrderViewModel>()
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src))
                .ForAllOtherMembers((dest => dest.Ignore()));
            this.CreateMap<OrderProgressViewModel, FilteredOrderViewModel>()
                .ForMember(dest => dest.OrderProgress, opt => opt.MapFrom(src => src))
                .ForAllOtherMembers((dest => dest.Ignore()));
            this.CreateMap<FilteredOrderViewModel, OrderProgressViewModel>()
                .AfterMap((src, dst) => Mapper.Map(src.OrderProgress, dst))
                .ForAllOtherMembers((dest => dest.Ignore()));

            CreateMap<OrderViewModel, OrderResultViewModel>()
                .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.name_en))
                .ForMember(dest => dest.CustomerPhone, opt => opt.MapFrom(src => src.Customer.kinder_person_tel_no))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Note))
                .ForMember(dest => dest.PreferedVisitTime, opt => opt.MapFrom(src => src.PreferedVisitTime))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.OrderPriority.Name))
                .ForMember(dest => dest.Problem, opt => opt.MapFrom(src => src.OrderProblem.Name))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.OrderStatus.Name))
                .ForMember(dest => dest.TechnicanName, opt => opt.MapFrom(src => src.TechnicanName))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.OrderType.Name))
                .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Customer.address));
        }

        private AssignedTechnicans CreateAssignedTechnicansViewModel(AssignedMultiTechnicansViewModel multitechnicans, string technican_Id)
        {
            var technican = Mapper.Map<AssignedMultiTechnicansViewModel, AssignedTechnicans>(multitechnicans);
            technican.FK_Technican_Id = technican_Id;
            return technican;
        }
        private List<OrderDistributionCriteria> CreateOrderDistributionCriteriaViewModel(OrderMultiDistributionCriteriaViewModel multiCriteria)
        {
            List<OrderDistributionCriteriaViewModel> resultVM = new List<OrderDistributionCriteriaViewModel>();
            List<OrderDistributionCriteria> result = null;
            foreach (var problem in multiCriteria.AreaProblems)
            {
                var criteria = Mapper.Map<AreaProblems, IList<OrderDistributionCriteriaViewModel>>(problem);

                foreach (var item in criteria)
                {
                    item.FK_Dispatcher_Id = multiCriteria.FK_Dispatcher_Id;
                    item.Dispatcher = multiCriteria.Dispatcher;
                }
                resultVM.AddRange(criteria.ToList());
            }
            result = Mapper.Map<List<OrderDistributionCriteriaViewModel>, List<OrderDistributionCriteria>>(resultVM);

            return result;
        }
        private OrderDistributionCriteriaViewModel CreateOrderDistributionCriteriaViewModel(AreaProblems problems, int probId)
        {
            var criteria = Mapper.Map<AreaProblems, OrderDistributionCriteriaViewModel>(problems);
            criteria.FK_OrderProblem_Id = probId;
            return criteria;
        }
    }
}
