﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.API.ServicesCommunication;
using Utilites;
using DispatchProduct.Ordering.API.Controllers;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.Ordering.API.ServicesViewModels;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Route("api/[controller]")]

    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class AssignedTechnicansController : Controller
    {
        public IAssignedTechnicansManager manger;
        public readonly IMapper mapper;
        IUserService userService;
        IOrderManager ordermanager;
        OrderController ordController;
        public AssignedTechnicansController(IAssignedTechnicansManager _manger, IMapper _mapper, IUserService _userService, IOrderManager _ordermanager, OrderController _ordController)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            userService = _userService;
            ordermanager = _ordermanager;
            ordController = _ordController;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            AssignedTechnicans entityResult = manger.Get(id);
            var result = mapper.Map<AssignedTechnicans, AssignedTechnicansViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<AssignedTechnicans> entityResult = manger.GetAll().ToList();
            List<AssignedTechnicansViewModel> result = mapper.Map<List<AssignedTechnicans>, List<AssignedTechnicansViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AssignedTechnicansViewModel model)
        {
            AssignedTechnicans entityResult = mapper.Map<AssignedTechnicansViewModel, AssignedTechnicans>(model);
            entityResult = manger.Add(entityResult);
            AssignedTechnicansViewModel result = mapper.Map<AssignedTechnicans, AssignedTechnicansViewModel>(entityResult);
            return Ok(result);
        }

        [Route("Assign")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AssignedMultiTechnicansViewModel model)
        {
            List<AssignedTechnicansViewModel> result = new List<AssignedTechnicansViewModel>();
            IList<AssignedTechnicans> entityResult = mapper.Map<AssignedMultiTechnicansViewModel, IList<AssignedTechnicans>>(model);
            foreach (var item in entityResult)
            {
                var res = manger.Add(item);
                result.Add(mapper.Map<AssignedTechnicans, AssignedTechnicansViewModel>(res));
            }
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]AssignedTechnicansViewModel model)
        {
            AssignedTechnicans entityResult = mapper.Map<AssignedTechnicansViewModel, AssignedTechnicans>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }

        [Route("UpdateAssignedTechnican")]
        [HttpPut]
        public async Task<IActionResult> UpdateAssignedTechnican([FromBody]AssignedTechnicansViewModel model)
        {
            AssignedTechnicans entityResult = mapper.Map<AssignedTechnicansViewModel, AssignedTechnicans>(model);
            bool result = manger.UpdateAssignedTechnican(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            AssignedTechnicans entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }

        [Route("DeleteAssignedTechnican/{technicanId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteAssignedTechnican([FromRoute]string technicanId)
        {
            bool result = false;
            result = manger.DeleteAssignedTechnican(technicanId);
            return Ok(result);
        }
        #endregion
        #endregion
        [Route("GetAssignedTechnicans")]
        [HttpGet]
        public async Task<List<ApplicationUserViewModel>> GetAssignedTechnicans(string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<string> fK_Technican_Ids = manger.GetAll().Select(t => t.FK_Technican_Id).ToList();
            return await userService.GetByUserIds(fK_Technican_Ids, authHeader);
        }

        [Route("GetUnAssignedTechnicans")]
        [HttpGet]
        public async Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicans(string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<string> fK_Technican_Ids = manger.GetAll().Select(t => t.FK_Technican_Id).ToList();
            return await userService.GetUnAssignedTechnicans(fK_Technican_Ids, authHeader);
        }
        [Route("GetTechnicansByDispatcherId/{dispatcherId}")]
        [HttpGet]
        public async Task<List<TechnicanViewModel>> GetTechnicansByDispatcherId([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<TechnicanViewModel> result = null;
            var assignedTechnicans = manger.GetByDispatcherId(dispatcherId);
            var technicansIds = assignedTechnicans.Select(t => t.Id).ToList();
            var tecnicansInfo = await userService.GetByUserIds(technicansIds, authHeader);
            foreach (var techn in assignedTechnicans)
            {
                var techInfo = tecnicansInfo.Find(t => t.Id == techn.Id);
                mapper.Map(techInfo, techn);
            }
            result = mapper.Map<List<Technican>, List<TechnicanViewModel>>(assignedTechnicans);
            foreach (var techViewMod in result)
            {
                var orders = techViewMod.Orders;
                orders = await ordController.FillCustomerOrders(orders, authHeader);
                orders = await ordController.FillLocationOrders(orders, authHeader);
            }
            return result;
        }
        [Route("GetDispatcher/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetDispatcher([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetDispatcherOrders(dispatcherId);
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            result.Orders = await ordController.FillCustomerOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillLocationOrders(result.Orders, authHeader);
            return result;
        }
        [Route("GetOrderByDispatcher/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetOrderByDispatcher([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetAllDispatcherOrders(dispatcherId);
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            result.Orders = await ordController.FillCustomerOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillLocationOrders(result.Orders, authHeader);
            return result;
        }
        [Route("GetDispatcherSupervisor/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetDispatcherSupervisor([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetOrdersAssignedToDispatcher(dispatcherId);
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            return result;
        }
        [Route("GetTechnican/{technicanId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetTechnican([FromRoute]string technicanId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { technicanId }, authHeader)).FirstOrDefault();
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetOrdersAssignedToTechnican(technicanId);
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            await FillOrderInfo(result);
            return result;
        }
        public async Task FillOrderInfo(DispatcherViewModel result, string authHeader = null)
        {
            if (result != null && result.Orders != null)
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                foreach (var item in result.Orders)
                {
                    var customer = await ordController.GetCustomerById(item.FK_Customer_Id, authHeader);

                    //CustomerViewModel tempView = mapper.Map<CUST_VM, CustomerViewModel>(customer);

                    //CustomerPhoneBookViewModel customerPhone = new CustomerPhoneBookViewModel()
                    //{
                    //    Phone = customer.address,
                    //    Id = 1,
                    //    FK_PhoneType_Id = 1,
                    //    FK_Customer_Id = item.FK_Customer_Id
                    //};


                    //tempView.CustomerPhoneBook = new List<CustomerPhoneBookViewModel>();
                    //tempView.CustomerPhoneBook.Add(customerPhone);
                    item.Customer = customer;

                    item.Location = await ordController.GetLocationById(item.FK_Location_Id, authHeader);
                    item.Contract = await ordController.GetContractById(item.FK_Contract_Id, authHeader);
                }
            }
        }
    }
}