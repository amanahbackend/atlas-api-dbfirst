﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.Controllers
{
    [Route("api/[controller]")]
    public class HealthCheckController :Controller
    {
        public HealthCheckController()
        {
        }
        [HttpGet(""), MapToApiVersion("1.0")]
        [HttpHead("")]
        public virtual IActionResult Ping()
        {
            return Ok("I'm fine");
        }
    }
}
