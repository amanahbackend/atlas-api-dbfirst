﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.API.ServicesViewModels;
using Utilites;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API;
using System.Threading;
using Microsoft.Extensions.Options;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Ordering.BLL.Filters;
using Utilites.UploadFile;
using Microsoft.AspNetCore.SignalR;
using DispatchProduct.Ordering.Hubs;
using Utilites.ProcessingResult;
using Utilities.Utilites.GoogleMapsAPI.DestinationMatrix;
using IdentityServer4.AccessTokenValidation;
using Utilities.Utilites.Paging;
using Microsoft.Extensions.Configuration;
using DispatchProduct.Api.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels.Dropout;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using DispatchProduct.Ordering.API.ServicesCommunication.Contract;
using DispatchProduct.Ordering.API.ServicesViewModels.Contract;

namespace DispatchProduct.Ordering.API.Controllers
{

    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class OrderController : Controller
    {
        public IOrderManager manger;
        public readonly IMapper mapper;
        private ICustomerService customerService;
        private ICustomerXService customerXService;
        private ILocationService locService;
        private IVehicleService vehicleService;
        private IOrderDistributionCriteriaManager ordDistribtion;
        private IUserService userService;
        private OrderAppSettings appSettings;
        private IPreventiveMaintainenceService preventiveService;
        private IContractService contractService;
        private IContractXService contractXService;
        private IHubContext<OrderingHub> orderingHub;
        private IOrderingHub orderHub;
        private ITechnicanUsedItemService usedItmService;
        private IConfigurationRoot configuration;
        private IOrderStatusManager orderStatusManager;
        private readonly IHostingEnvironment hostingEnv;

        public OrderController(IContractService _contractService, IOrderManager _manger, IMapper _mapper,
            ICustomerService _customerService, ILocationService _locService,
            IVehicleService _vehicleService, IOrderDistributionCriteriaManager _ordDistribtion,
            IUserService _userService, IOptions<OrderAppSettings> _appSettings,
            IPreventiveMaintainenceService _preventiveService,
            IHubContext<OrderingHub> _orderingHub, IOrderingHub _orderHub,
            ITechnicanUsedItemService _usedItmServ, ICustomerXService _customerXService,
            IConfigurationRoot _configuration, IOrderStatusManager _orderStatusManager,
            IHostingEnvironment _hostingEnv, IContractXService _contractXService)
        {
            contractXService = _contractXService;
            hostingEnv = _hostingEnv;
            orderStatusManager = _orderStatusManager;
            configuration = _configuration;
            manger = _manger;
            mapper = _mapper;
            customerService = _customerService;
            locService = _locService;
            vehicleService = _vehicleService;
            ordDistribtion = _ordDistribtion;
            userService = _userService;
            appSettings = _appSettings.Value;
            preventiveService = _preventiveService;
            contractService = _contractService;
            orderingHub = _orderingHub;
            orderHub = _orderHub;
            usedItmService = _usedItmServ;
            customerXService = _customerXService;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            Order entityResult = manger.Get(id);
            var result = mapper.Map<Order, OrderViewModel>(entityResult);
            if (result != null)
            {
                result.Customer = await GetCustomerById(result.FK_Customer_Id);
                result.Location = await GetLocationById(result.FK_Location_Id);
                result.Contract = await GetContractById(result.FK_Contract_Id);
                BindFilesURL(result);
            }

            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<Order> entityResult = manger.GetAllOrderFilledProps().ToList<Order>();
            List<OrderViewModel> result = mapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
            //result = await FillCustomerOrders(result);
            result = await FillLocationOrders(result);
            //result = await FillContractOrders(result);
            BindFilesURL(result);
            return Ok(result);
        }

        [Route("GetMapFilteredOrderByDispatcherId")]
        [HttpPost]
        public async Task<IActionResult> GetMapFilteredOrderByDispatcherId([FromBody] FilterOrderViewModelByDispatcher model)
        {
            FilterOrderByDispatcher filter = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(model);
            List<Order> orders = manger.GetFilteredOrderByDispatcherId(filter);
            List<OrderViewModel> ordersViewModel = mapper.Map<List<Order>, List<OrderViewModel>>(orders);
            //ordersViewModel = await FillCustomerOrders(ordersViewModel);
            ordersViewModel = await FillLocationOrders(ordersViewModel);
            ordersViewModel = await Fill_Tech_Dispatcher_Orders(ordersViewModel);
            BindFilesURL(ordersViewModel);
            return Ok(ordersViewModel);
        }

        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] OrderViewModel model)
        {
            ProcessResult<OrderViewModel> processResult = await AddOrder(model);
            return !processResult.IsSucceeded ? BadRequest(processResult.Message) : (IActionResult)Ok(processResult.returnData);
        }

        public async Task<ProcessResult<OrderViewModel>> AddOrder([FromBody] OrderViewModel model, string auth = null)
        {
            ProcessResult<OrderViewModel> result = new ProcessResult<OrderViewModel>();
            result.IsSucceeded = true;
            Order entityResult = mapper.Map<OrderViewModel, Order>(model);
            LocationViewModel locationById = await GetLocationById(entityResult.FK_Location_Id, auth);
            if (locationById != null)
            {
                entityResult.Area = locationById.Area;
                string str = ordDistribtion.GetDispatcherIdForOrder(entityResult.FK_OrderProblem_Id, locationById.Area);
                if (str == null)
                    //str = (await GetDispatcherSupervisor(auth)).Id;
                    str = "0d066197-0aab-46fb-bdb8-d41d809b23a7";
                if (str != null)
                {
                    entityResult.FK_Dispatcher_Id = str;
                    entityResult = manger.Add(entityResult);
                    result.returnData = mapper.Map<Order, OrderViewModel>(entityResult);
                    orderHub.AssignOrderToDispatcher(result.returnData, orderingHub, entityResult.FK_Dispatcher_Id);
                }
                else
                {
                    result.IsSucceeded = false;
                    result.Message = "Can't create order without assigning to dispatcher. set supervisor dispatcher or assign convenient criteria to dispatcher";
                }
            }
            else
            {
                result.IsSucceeded = false;
                result.Message = "Can't create order by this location";
            }
            return result;
        }
        [Route("AddOrderProgress")]
        [HttpPost]
        public async Task<IActionResult> AddOrderProgress([FromBody]OrderProgressViewModel model)
        {
            OrderProgress entityResult = mapper.Map<OrderProgressViewModel, OrderProgress>(model);
            entityResult = manger.AddOrderProgress(entityResult);
            OrderProgressViewModel result = mapper.Map<OrderProgress, OrderProgressViewModel>(entityResult);
            Order order = manger.Get(model.FK_Order_Id);
            OrderViewModel orderViewModel = mapper.Map<Order, OrderViewModel>(order);
            result.Order = orderViewModel;
            if (result.FK_CreatedBy_Id == result.FK_Technican_Id)
                orderHub.ChangeOrderProgress(result, orderingHub, order.FK_Dispatcher_Id);
            else
                orderHub.ChangeOrderProgress(result, orderingHub, order.FK_Technican_Id);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderViewModel model, string authHeader = null)
        {
            Order entityResult = mapper.Map<OrderViewModel, Order>(model);
            bool result = manger.Update(entityResult);
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            orderHub.UpdateOrder(model, orderingHub, authHeader);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Order entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        #region Assign
        [Route("AssignOrderToTechnican")]
        [HttpPost]
        public async Task<bool> AssignOrderToTechnican([FromBody]AssignOrderToTechnicanViewModel model)
        {
            var result = manger.AssignOrderToTechnican(model.FK_Order_Id, model.FK_Technican_Id);
            var orderentity = manger.Get(model.FK_Order_Id);
            var order = mapper.Map<Order, OrderViewModel>(orderentity);
            //orderHub.AssignOrderToTechnican(order, orderingHub, model.FK_Technican_Id);
            await NotifyAssignTechnician(order);
            return result;
        }
        [Route("UnAssignOrderToTechnican/{orderId}")]
        [HttpGet]
        public async Task<bool> UnAssignOrderToTechnican([FromRoute]int orderId)
        {
            var userId = manger.UnAssignOrderToTechnican(orderId);
            await NotifyUnAssignTechnician(userId);
            return true;
        }
        [Route("AssignOrdersToTechnican")]
        [HttpPost]
        public bool AssignOrdersToTechnican([FromBody]AssignMultiOrderToTechnicanViewModel model)
        {
            return manger.AssignOrdersToTechnican(model.FK_Order_Ids, model.FK_Technican_Id);
        }

        [Route("TransferOrderToDispatcher")]
        [HttpPost]
        public bool TransferOrderToDispatcher([FromBody]AssignOrderToDispatcherViewModel model)
        {

            var result = manger.TransferOrderToDispatcher(model.FK_Order_Id, model.FK_Dispatcher_Id);
            var orderentity = manger.Get(model.FK_Order_Id);
            var order = mapper.Map<Order, OrderViewModel>(orderentity);
            orderHub.AssignOrderToDispatcher(order, orderingHub, model.FK_Dispatcher_Id);
            return result;
        }
        [Route("SetPreferedVisitTime")]
        [HttpPost]
        public bool SetPreferedVisitTime([FromBody]OrderPreferedVisitTimeViewModel model)
        {
            return manger.SetPreferedVisitTime(model.FK_Order_Id, model.PreferedVisitTime);
        }
        [Route("AssignOrdersToDispatcher")]
        [HttpPost]
        public bool AssignOrdersToDispatcher([FromBody]AssignMultiOrderToDispatcherViewModel model)
        {
            return manger.AssignOrdersToDispatcher(model.FK_Order_Ids, model.FK_Dispatcher_Id);
        }
        [Route("AssignOrderToDispatcher_Technican")]
        [HttpPost]
        public bool AssignOrderToDispatcher_Technican([FromBody]AssignOrderToDispatcher_TechnicanViewModel model)
        {
            return manger.AssignOrderToDispatcher_Technican(model.FK_Order_Id, model.FK_Technican_Id, model.FK_Dispatcher_Id);
        }
        [Route("AssignOrdersToDispatcher_Technican")]
        [HttpPost]
        public bool AssignOrdersToDispatcher_Technican([FromBody]AssignMultiOrderToDispatcher_TechnicanViewModel model)
        {
            return manger.AssignOrdersToDispatcher_Technican(model.FK_Order_Ids, model.FK_Technican_Id, model.FK_Dispatcher_Id);
        }
        #endregion

        [Route("GetAllPaginated/{pageNo}/{pageSize}")]
        [HttpGet]
        public async Task<PaginatedItemsViewModel<OrderViewModel>> GetAllPaginated(int pageNo, int pageSize)
        {
            var authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            PaginatedItems<Order> paginated = new PaginatedItems<Order>()
            {
                PageNo = pageNo,
                PageSize = pageSize
            };

            PaginatedItems<Order> orders = manger.GetAllPaginated(nameof(Order.Code), paginated);
            orders.Data = manger.FillOrdersNavProps(orders.Data);

            List<OrderViewModel> resultOrders = mapper.Map<List<OrderViewModel>>(orders.Data);
            foreach (var item in resultOrders)
            {
                item.Location = await locService.GetItem(item.FK_Location_Id.ToString(), authHeader);
                item.Customer = await GetCustomerById(item.FK_Customer_Id, authHeader);
            }
            PaginatedItemsViewModel<OrderViewModel> result = new PaginatedItemsViewModel<OrderViewModel>
            {
                PageNo = orders.PageNo,
                PageSize = orders.PageSize,
                Count = orders.Count,
                Data = resultOrders
            };
            return result;
        }

        [Route("GetAllPaginated/{pageNo}/{pageSize}/{searchKey}")]
        [HttpGet]
        public async Task<PaginatedItemsViewModel<OrderViewModel>> GetAllPaginated(int pageNo, int pageSize, string searchKey)
        {
            var authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");

            PaginatedItems<Order> paginated = new PaginatedItems<Order>()
            {
                PageNo = pageNo,
                PageSize = pageSize
            };

            PaginatedItems<Order> orders = manger.GetAllPaginated(nameof(Order.Code), paginated,
                x => x.Area.Contains(searchKey) || x.Code.Contains(searchKey));
            orders.Data = manger.FillOrdersNavProps(orders.Data);

            List<OrderViewModel> resultOrders = mapper.Map<List<OrderViewModel>>(orders.Data);
            foreach (var item in resultOrders)
            {
                item.Location = await locService.GetItem(item.FK_Location_Id.ToString(), authHeader);
                item.Customer = await GetCustomerById(item.FK_Customer_Id, authHeader);
            }
            PaginatedItemsViewModel<OrderViewModel> result = new PaginatedItemsViewModel<OrderViewModel>
            {
                PageNo = orders.PageNo,
                PageSize = orders.PageSize,
                Count = orders.Count,
                Data = resultOrders
            };
            return result;
        }
        [HttpPost]
        [Route("CustomerSignature")]
        public async Task<IActionResult> CustomerSignature([FromBody]CustomerSignatureViewModel signature)
        {

            var issucceed = await AddUsedItems(signature.UsedItems, signature.OrderProgress.FK_Technican_Id, signature.OrderProgress.FK_Order_Id);
            if (issucceed)
            {
                await AddOrderProgress(signature.OrderProgress);
                Order entity = manger.Get(signature.OrderProgress.FK_Order_Id);
                if (entity != null)
                {
                    if (signature.File != null)
                    {
                        signature.File.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignaturePath == null)
                            appSettings.SignaturePath = "OrderSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignaturePath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.File, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignaturePath = $"{path}/{signature.File.FileName}";
                    }
                    if (signature.ContractSignatureFile != null)
                    {
                        signature.ContractSignatureFile.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignatureContractPath == null)
                            appSettings.SignatureContractPath = "OrderContractSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignatureContractPath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.ContractSignatureFile, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignatureContractPath = $"{path}/{signature.ContractSignatureFile.FileName}";
                    }
                    manger.Update(entity);
                }
                return Ok(true);
            }
            else
            {
                return BadRequest("Add Used Items Process Failed");
            }

        }
        [Route("Search/{key}")]
        [HttpGet]
        public async Task<IActionResult> Search(string key)
        {
            List<OrderViewModel> orderViewModelList = null;
            if (key != null)
            {
                List<Order> entityResult = manger.Search(key);
                //if (entityResult == null || entityResult.Count == 0)
                //{
                //    CustomerViewModel customerViewModel = await SearchCustomer(key);
                //    if (customerViewModel != null)
                //    {
                //        entityResult = manger.SearchByCustomerId(customerViewModel.Id);
                //        foreach (OrderViewModel orderViewModel in mapper.Map<List<Order>, List<OrderViewModel>>(entityResult))
                //            orderViewModel.Customer = customerViewModel;
                //    }
                //}
                orderViewModelList = mapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
                orderViewModelList = await FillLocationOrders(orderViewModelList);
                //orderViewModelList = await FillCustomerOrders(orders);
            }
            return Ok(orderViewModelList);
        }
        [Route("SearchByCustomerId/{customerId}")]
        [HttpGet]
        public async Task<IActionResult> SearchByCustomerId(int customerId)
        {
            List<OrderViewModel> result = null;
            if (customerId > 0)
            {
                var entityResult = manger.SearchByCustomerId(customerId);
                result = mapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
                result = await FillLocationOrders(result);

            }
            return Ok(result);
        }
        public async Task<List<OrderViewModel>> FillCustomerOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                item.Customer = await GetCustomerById(item.FK_Customer_Id, authHeader);
            }
            return orders;
        }
        public async Task<List<OrderProgressViewModel>> Fill_Tech_Dispatcher_OrderProgress(List<OrderProgressViewModel> orderProgress, string authHeader = null)
        {
            foreach (var item in orderProgress)
            {
                ApplicationUserViewModel user = await GetUser(item.FK_Technican_Id, authHeader);
                if (user != null)
                    item.TechnicanName = user.UserName;
                item.CreatedBy = await GetUser(item.FK_CreatedBy_Id, authHeader);
            }

            return orderProgress;
        }
        private async Task<CustomerViewModel> SearchCustomer(string key, string authHeader = null)
        {
            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                result = await customerService.SearchCustomer(key, authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        //public async Task<List<OrderViewModel>> FillContractOrders(List<OrderViewModel> orders, string authHeader = null)
        //{
        //    foreach (var item in orders)
        //    {
        //        item.Contract = await GetContractById(item.FK_Contract_Id, authHeader);
        //    }
        //    return orders;
        //}
        public async Task<List<OrderViewModel>> FillLocationOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                item.Location = await GetLocationById(item.FK_Location_Id, authHeader);
            }
            return orders;
        }
        public async Task<List<OrderViewModel>> Fill_Tech_Dispatcher_Orders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                var tech = await GetUser(item.FK_Technican_Id, authHeader);
                if (tech != null)
                    item.TechnicanName = tech.UserName;
                var dispatcher = await GetUser(item.FK_Dispatcher_Id, authHeader);
                if (dispatcher != null)
                    item.DispatcherName = dispatcher.UserName;
            }

            return orders;
        }
        [Route("GetOrderByDispatcher/{dispatcherId}")]
        [HttpGet]
        private async Task<ApplicationUserViewModel> GetUser([FromRoute] string usreId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            List<string> userIds = new List<string>();
            userIds.Add(usreId);
            string authHeader1 = authHeader;
            return (await userService.GetByUserIds(userIds, authHeader1))?.FirstOrDefault();
        }
        public async Task<CUST_VM> GetCustomerById(int id, string authHeader = null)
        {

            CUST_VM result = null;
            try
            {
                //if (Request != null && authHeader == null)
                //{
                //    authHeader = Helper.GetValueFromRequestHeader(Request);
                //}
                result = await customerXService.GetCustomer(id, authHeader);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            return result;
        }
        public async Task<CONT_VM> GetContractById(int id, string authHeader = null)
        {

            CONT_VM result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await contractXService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<LocationViewModel> GetLocationById(int? locId, string authHeader = null)
        {
            LocationViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await locService.GetItem(locId.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<List<VehicleViewModel>> GetAllVehicles(string authHeader = null)
        {
            List<VehicleViewModel> result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await vehicleService.GetList(authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        [Route("AssignOrdersToTechnicanAutomatically/{orderId}")]
        [HttpGet]
        public async Task<ProcessResult<DistanceResultViewModel>> GetDistanceMatrixResult([FromRoute]int orderId)
        {
            ProcessResult<DistanceResultViewModel> result = new ProcessResult<DistanceResultViewModel>();
            result.IsSucceeded = true;
            List<DistancePoint> origins = new List<DistancePoint>();
            List<DistancePoint> destinations = new List<DistancePoint>();
            var vehicles = await GetAllVehicles();
            if (vehicles != null || vehicles.Count > 0)
            {
                foreach (var vehicle in vehicles)
                {
                    origins.Add(new DistancePoint(vehicle.Lat, vehicle.Long));
                }
            }
            else
            {
                result.IsSucceeded = false;
                result.Message = "no vehicles registered";
            }
            var order = manger.Get(orderId);
            var loc = await GetLocationById(order.FK_Location_Id);
            if (loc != null)
            {
                destinations.Add(new DistancePoint(loc.Latitude, loc.Longitude));
                result.returnData = DestinationMatrixService.GetDistances(appSettings.MapDestinationMatrixService, appSettings.MapAPIKey, origins, destinations);
                if (result.returnData.Status != "ZERO_RESULTS")
                {

                }
                else
                {
                    result.IsSucceeded = false;
                    result.Message = "result calculation between origins and destinations is invalid";
                }
            }
            else
            {
                result.IsSucceeded = false;
                result.Message = "order location is not correct";
            }
            return result;
        }
        public async Task<ApplicationUserViewModel> GetDispatcherSupervisor(string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            result = (await userService.GetDispatcherSupervisor(authHeader)).FirstOrDefault();
            return result;
        }
        private async Task<bool> AddUsedItems(List<int> usedItemIds, string FK_TechnicanId, int FK_Order_Id, string authHeader = null)
        {
            bool flag;
            try
            {
                if (usedItemIds != null && usedItemIds.Count > 0)
                {
                    List<TechnicanUsedItemsViewModel> usedItems = new List<TechnicanUsedItemsViewModel>();
                    var usdItmsgroped = usedItemIds.GroupBy((itm => itm)).Select(g => new
                    {
                        itemId = g.Key,
                        Count = g.Count()
                    });
                    if (Request != null && authHeader == null)
                        authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                    foreach (var data in usdItmsgroped)
                        usedItems.Add(new TechnicanUsedItemsViewModel()
                        {
                            ItemId = data.itemId,
                            Amount = data.Count,
                            FK_Technican_Id = FK_TechnicanId,
                            FK_Order_Id = FK_Order_Id
                        });
                    if (usedItems.Count > 0)
                    {
                        List<TechnicanUsedItemsViewModel> usedItemsViewModelList = await usedItmService.AddUsedItems(usedItems, authHeader);
                        flag = usedItemsViewModelList != null && usedItemsViewModelList.Count > 0;
                    }
                    else
                        flag = true;
                }
                else
                    flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return flag;
        }
        private List<OrderViewModel> BindFilesURL(List<OrderViewModel> model)
        {
            foreach (OrderViewModel model1 in model)
                BindFilesURL(model1);
            return model;
        }
        private OrderViewModel BindFilesURL(OrderViewModel model)
        {
            string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (model.SignaturePath != null)
            {
                model.SignaturePath = model.SignaturePath.Replace('\\', '/');
                model.SignatureURL = $"{str}/{model.SignaturePath}";
            }
            if (model.SignatureContractPath != null)
            {
                model.SignatureContractPath = model.SignatureContractPath.Replace('\\', '/');
                model.SignatureContractURL = $"{str}/{model.SignatureContractPath}";
            }
            return model;
        }

        private async Task NotifyAssignTechnician(OrderViewModel orderViewModel)
        {
            var uri = $"{configuration["DropoutServiceSetting:Uri"]}/{configuration["DropoutServiceSetting:SendVerb"]}";
            SendNotificationViewModel model = new SendNotificationViewModel
            {
                Body = "New order assigned to you",
                Title = "Atlas",
                UserId = orderViewModel.FK_Technican_Id,
                Data = new { orderViewModel.Id, orderViewModel.Code }
            };
            await HttpRequestFactory.Post(uri, model);
        }

        private async Task NotifyUnAssignTechnician(string userId)
        {
            var uri = $"{configuration["DropoutServiceSetting:Uri"]}/{configuration["DropoutServiceSetting:SendVerb"]}";
            SendNotificationViewModel model = new SendNotificationViewModel
            {
                Body = "Order unassigned from you",
                Title = "Atlas",
                UserId = userId
            };
            await HttpRequestFactory.Post(uri, model);
        }

        [Route("SetCustomerNotRespond/{orderId}"), HttpGet]
        public IActionResult SetCustomerNotRespond(int orderId)
        {
            var order = manger.Get(orderId);
            order.FK_OrderStatus_Id = orderStatusManager.GetCustomerNotRespondStatus().Id;
            var result = manger.Update(order);
            return Ok(result);
        }

        [Route("GetFilteredOrderByDispatcherId")]
        [HttpPost]
        public async Task<IActionResult> GetFilteredOrderByDispatcherId([FromBody] FilterOrderViewModelByDispatcher filter)
        {
            FilterOrderByDispatcher filter1 = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(filter);
            List<Order> list = manger.GetFilteredOrderByDispatcherId(filter1).ToList();
            List<OrderViewModel> orders = mapper.Map<List<Order>, List<OrderViewModel>>(list);
            List<FilteredOrderViewModel> result = new List<FilteredOrderViewModel>();
            orders = await FillCustomerOrders(orders);
            orders = await FillLocationOrders(orders);
            var model = await Fill_Tech_Dispatcher_Orders(orders);
            BindFilesURL(model);
            foreach (OrderViewModel orderViewModel in model)
            {
                OrderViewModel item = orderViewModel;
                if (item.LstOrderProgress != null && item.LstOrderProgress.Count > 0)
                {
                    List<OrderProgressViewModel> progressViewModelList = await Fill_Tech_Dispatcher_OrderProgress(item.LstOrderProgress);
                    foreach (OrderProgressViewModel source in item.LstOrderProgress)
                    {
                        FilteredOrderViewModel destination = mapper.Map<OrderViewModel, FilteredOrderViewModel>(item);
                        result.Add(mapper.Map(source, destination));
                    }
                }
                else
                    result.Add(mapper.Map<OrderViewModel, FilteredOrderViewModel>(item));
            }
            return Ok(result);
        }

        [Route("FilterToPrint"), HttpPost]
        public async Task<IActionResult> FilterToPrint([FromBody] FilterOrderViewModelByDispatcher filter)
        {
            FilterOrderByDispatcher filter1 = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(filter);
            List<Order> list = manger.GetFilteredOrderByDispatcherId(filter1).ToList();
            List<OrderViewModel> orders = mapper.Map<List<Order>, List<OrderViewModel>>(list);
            orders = await FillCustomerOrders(orders);
            orders = await FillLocationOrders(orders);
            var models = await Fill_Tech_Dispatcher_Orders(orders);
            var result = mapper.Map<List<OrderViewModel>, List<OrderResultViewModel>>(orders);
            return Ok(result);
        }

        [Route("GetPrevintive"), HttpGet]
        public async Task<IActionResult> GetPrevintive()
        {
            var authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");

            var orders = manger.GetPMOrders();
            List<OrderViewModel> resultOrders = mapper.Map<List<OrderViewModel>>(orders);
            //foreach (var item in resultOrders)
            //{
            //    if (item.FK_Location_Id != 0)
            //    {
            //        item.Location = await locService.GetItem(item.FK_Location_Id.ToString(), authHeader);
            //    }
            //    if (item.FK_Customer_Id != 0)
            //    {
            //        item.Customer = await GetCustomerById(item.FK_Customer_Id, authHeader);
            //    }
            //}
            return Ok(resultOrders);
        }
    }
}