﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;
using Utilites;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API;
using IdentityServer4.AccessTokenValidation;

namespace DispatchProduct.CustomerModule.API.Controllers
{

    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class OrderDistributionCriteriaController : Controller
    {
        public IOrderDistributionCriteriaManager manger;
        public readonly IMapper mapper;
        IUserService userService;
        public OrderDistributionCriteriaController(IOrderDistributionCriteriaManager _manger, IMapper _mapper, IUserService _userService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            userService = _userService;


        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            OrderDistributionCriteria entityResult = manger.Get(id);
            var result = mapper.Map<OrderDistributionCriteria, OrderDistributionCriteriaViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetByDispatcherId/{dispatcherId}")]
        [HttpGet]
        public async Task<IActionResult> GetByDispatcherId(string dispatcherId)
        {
            List<OrderDistributionCriteria> entityResult = manger.GetByDispatcherId(dispatcherId);
            List<OrderDistributionCriteriaViewModel> result = mapper.Map<List<OrderDistributionCriteria>, List<OrderDistributionCriteriaViewModel>>(entityResult);
            var itm = result.FirstOrDefault();
            if (itm.FK_Dispatcher_Id != null)
            {
                var dispatcher = await GetUser(itm.FK_Dispatcher_Id);
                foreach (var item in result)
                {
                    item.Dispatcher = dispatcher;
                }
            }
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<OrderDistributionCriteria> entityResult = manger.GetAll().ToList();
            List<OrderDistributionCriteriaViewModel> result = mapper.Map<List<OrderDistributionCriteria>, List<OrderDistributionCriteriaViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderDistributionCriteriaViewModel model)
        {
            OrderDistributionCriteria entityResult = mapper.Map<OrderDistributionCriteriaViewModel, OrderDistributionCriteria>(model);
            entityResult = manger.Add(entityResult);
            OrderDistributionCriteriaViewModel result = mapper.Map<OrderDistributionCriteria, OrderDistributionCriteriaViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderDistributionCriteriaViewModel model)
        {
            OrderDistributionCriteria entityResult = mapper.Map<OrderDistributionCriteriaViewModel, OrderDistributionCriteria>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            OrderDistributionCriteria entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        [Route("AssignCriteriaToDispatchers")]
        [HttpPost]
        public async Task<IActionResult> AssignCriteriaToDispatchers([FromBody]List<OrderMultiAreaDistributionCriteriaViewModel> model)
        {
            List<OrderMultiDistributionCriteriaViewModel> oldModel = new List<OrderMultiDistributionCriteriaViewModel>();
            foreach (var item in model)
            {
                OrderMultiDistributionCriteriaViewModel orderMultiDistributionCriteriaViewModel = new OrderMultiDistributionCriteriaViewModel
                {
                    FK_Dispatcher_Id = item.FK_Dispatcher_Id
                };
                orderMultiDistributionCriteriaViewModel.AreaProblems = new List<AreaProblems>();
                foreach (var orderProblem in item.AreasProblems)
                {
                    foreach (var area in orderProblem.Areas)
                    {
                        orderMultiDistributionCriteriaViewModel.AreaProblems.Add(new AreaProblems
                        {
                            Area = area,
                            Governorate = orderProblem.Governorate,
                            FK_OrderProblem_Ids = orderProblem.FK_OrderProblem_Ids
                        });
                    }
                }
                oldModel.Add(orderMultiDistributionCriteriaViewModel);
            }
            List<OrderDistributionCriteriaViewModel> result = new List<OrderDistributionCriteriaViewModel>();

            foreach (var item in oldModel)
            {
                manger.DeleteByDispatcherId(item.FK_Dispatcher_Id);
                item.Dispatcher = (await GetUser(item.FK_Dispatcher_Id));
                var dispatcherCriterias = mapper.Map<OrderMultiDistributionCriteriaViewModel, List<OrderDistributionCriteria>>(item);
                dispatcherCriterias = manger.AssignCriteriaToDispatcher(dispatcherCriterias);
                var dispatcherCriteriasVM = mapper.Map<List<OrderDistributionCriteria>, List<OrderDistributionCriteriaViewModel>>(dispatcherCriterias);
                result.AddRange(dispatcherCriteriasVM);
            }

            return Ok(result);
        }

        [Route("GetAllDistribution")]
        [HttpGet]
        public async Task<IActionResult> GetAllDistribution()
        {

            var entityResult = manger.GetAllDistribution();
            var result = mapper.Map<List<OrderDistributionCriteria>, List<OrderDistributionCriteriaViewModel>>(entityResult);

            foreach (var item in result)
            {
                try
                {
                    item.Dispatcher = await GetUser(item.FK_Dispatcher_Id);
                }
                catch (Exception)
                {
                }
            }
            return Ok(result);

        }
        public async Task<ApplicationUserViewModel> GetUser([FromRoute]string userId, string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            result = (await userService.GetByUserIds(new List<string> { userId }, authHeader)).FirstOrDefault();
            return result;
        }

    }
}