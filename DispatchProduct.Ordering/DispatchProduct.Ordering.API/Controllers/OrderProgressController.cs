﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;
using Microsoft.AspNetCore.SignalR;
using DispatchProduct.Ordering.Hubs;
using DispatchProduct.Ordering.API.ServicesCommunication;
using Utilites;
using IdentityServer4.AccessTokenValidation;

namespace DispatchProduct.CustomerModule.API.Controllers
{
  
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class OrderProgressController : Controller
    {
        public IOrderProgressManager manger;
        public readonly IMapper mapper;
        IHubContext<OrderingHub> orderingHub;
        IOrderingHub orderHub;
        IOrderManager orderManger;
        IUserService userService;
        public OrderProgressController(IOrderProgressManager _manger, IOrderManager _orderManger, IMapper _mapper, IHubContext<OrderingHub> _orderingHub, IOrderingHub _orderHub, IUserService _userService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            orderHub = _orderHub;
            orderingHub = _orderingHub;
            orderManger = _orderManger;
            userService = _userService;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public IActionResult Get([FromRoute]int id)
        {
            OrderProgress entityResult = manger.Get(id);
            var result = mapper.Map<OrderProgress, OrderProgressViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<OrderProgress> entityResult = manger.GetAll().ToList();
            List<OrderProgressViewModel>  result = mapper.Map<List<OrderProgress>, List<OrderProgressViewModel>>(entityResult);
            return Ok(result);
        }
        [Route("GetByOrderId/{orderId}")]
        [HttpGet]
        public async Task<IActionResult> GetByOrderId([FromRoute]int orderId,string authHeader = null)
        {
            List<OrderProgress> entityResult = manger.GetOrderProgressByOrderId(orderId).ToList();
            List<OrderProgressViewModel> result = mapper.Map<List<OrderProgress>, List<OrderProgressViewModel>>(entityResult);
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            foreach (var item in result)
            {
                item.CreatedBy = (await userService.GetByUserIds(new List<string>() { item.FK_CreatedBy_Id }, authHeader)).FirstOrDefault();
            }
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderProgressViewModel model)
        {
            OrderProgress entityResult = mapper.Map<OrderProgressViewModel, OrderProgress>(model);
            entityResult = manger.Add(entityResult);
            var result = mapper.Map<OrderProgress, OrderProgressViewModel>(entityResult);
            var order=orderManger.Get(model.FK_Order_Id);
            var orderViewModel = mapper.Map<Order,OrderViewModel> (order);
            result.Order = orderViewModel;
            if (model.FK_CreatedBy_Id == order.FK_Technican_Id)
            {
                orderHub.ChangeOrderProgress(result, orderingHub, order.FK_Dispatcher_Id);
            }
            else
            {
                orderHub.ChangeOrderProgress(result, orderingHub, order.FK_Technican_Id);
            }
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderProgressViewModel model)
        {
            OrderProgress entityResult = mapper.Map<OrderProgressViewModel, OrderProgress>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            OrderProgress entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}