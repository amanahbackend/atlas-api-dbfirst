﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;
using IdentityServer4.AccessTokenValidation;

namespace DispatchProduct.CustomerModule.API.Controllers
{
  
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class ProgressStatusController : Controller
    {
        public IProgressStatusManager manger;
        public readonly IMapper mapper;
        public ProgressStatusController(IProgressStatusManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            ProgressStatus entityResult = manger.Get(id);
            var result = mapper.Map<ProgressStatus, ProgressStatusViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<ProgressStatus> entityResult = manger.GetAll().ToList();
            List<ProgressStatusViewModel>  result = mapper.Map<List<ProgressStatus>, List<ProgressStatusViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ProgressStatusViewModel model)
        {
            ProgressStatus entityResult = mapper.Map<ProgressStatusViewModel, ProgressStatus>(model);
            entityResult = manger.Add(entityResult);
            ProgressStatusViewModel result = mapper.Map<ProgressStatus, ProgressStatusViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]ProgressStatusViewModel model)
        {
            ProgressStatus entityResult = mapper.Map<ProgressStatusViewModel, ProgressStatus>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            ProgressStatus entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}