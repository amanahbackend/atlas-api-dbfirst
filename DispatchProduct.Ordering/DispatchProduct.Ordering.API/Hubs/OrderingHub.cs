﻿using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Settings;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Utilites;

namespace DispatchProduct.Ordering.Hubs
{
    //[Authorize]
    public class OrderingHub : Hub, IOrderingHub
    {
        private static List<SignalRConnection> connections = new List<SignalRConnection>();
        private OrderingHubSettings setting;
        public OrderingHub(IOptions<OrderingHubSettings> _setting)
        {
            setting = _setting.Value;
        }
        public void AssignOrderToDispatcher(OrderViewModel order, IHubContext<OrderingHub> orderingHub,string dispatcherId)
        {
            if (dispatcherId != null)
            {
                var connection = connections.Where(con => con.UserId == dispatcherId);
                if (connection != null)
                {
                    foreach (var conn in connection)
                    {
                       orderingHub.Clients.Client(conn.ConnectionId).InvokeAsync(setting.AssignOrderToDispatcher, order);
                    }
                }
                   
            }
        }
        public void AssignOrderToTechnican(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string technicanId)
        {
            if (technicanId != null)
            {
                var connection = connections.Where(con => con.UserId == technicanId);
                if (connection != null)
                {
                    foreach (var conn in connection)
                    {
                        orderingHub.Clients.Client(conn.ConnectionId).InvokeAsync(setting.AssignOrderToTechnican, order);
                    }
                }

            }
        }
        public void UpdateOrder(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string token)
        {
            string connectionId1 = null;
            var signalRconnection = connections.Where(con => con.Token == token).FirstOrDefault();
            string connectionId2;
            if (signalRconnection != null)
            {
                if (order.FK_Dispatcher_Id != null && signalRconnection.UserId == order.FK_Dispatcher_Id)
                    connectionId2 = connections.Where(con => con.UserId == order.FK_Technican_Id).FirstOrDefault().ConnectionId;
                else if (order.FK_Technican_Id != null && signalRconnection.UserId == order.FK_Technican_Id)
                    connectionId2 = connections.Where(con => con.UserId == order.FK_Dispatcher_Id).FirstOrDefault().ConnectionId;
                else
                    this.SendUpdateOrderForBoth(order, orderingHub, connectionId1);
            }
            else
                this.SendUpdateOrderForBoth(order, orderingHub, connectionId1);
        }
        public void SendUpdateOrderForBoth(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string connectionId)
        {
            if (order.FK_Technican_Id != null)
            {
                foreach (SignalRConnection signalRconnection in connections.Where(con => con.UserId == order.FK_Technican_Id).ToList())
                {
                    if (signalRconnection != null)
                    {
                        connectionId = signalRconnection.ConnectionId;
                        SendUpdatedOrder(order, orderingHub, connectionId);
                    }
                }
            }
            if (order.FK_Dispatcher_Id == null)
                return;
            foreach (var signalRconnection in connections.Where(con => con.UserId == order.FK_Dispatcher_Id).ToList())
            {
                if (signalRconnection != null)
                {
                    connectionId = signalRconnection.ConnectionId;
                    SendUpdatedOrder(order, orderingHub, connectionId);
                }
            }
        }

        public void SendUpdatedOrder(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string connectionId)
        {
            if (connectionId != null)
            {
                var client = orderingHub.Clients.Client(connectionId);
                if (client == null)
                client.InvokeAsync(setting.UpdateOrder, order);
            }
        }


        public void ChangeOrderProgress(OrderProgressViewModel orderProgress, IHubContext<OrderingHub> orderingHub,string userId)
        {
            var connection = connections.Where(con => con.UserId == userId);
            if (connection != null)
            {
                foreach (var conn in connection)
                {
                    orderingHub.Clients.Client(conn.ConnectionId).InvokeAsync(setting.ChangeOrderProgress, orderProgress);
                }
            }
        }
        public async Task JoinGroup(string groupName)
        {
            await Groups.AddAsync(Context.ConnectionId, groupName);
        }
        public async Task LeaveGroup(string groupName)
        {
            if (connections.Where(r => r.ConnectionId == Context.ConnectionId).FirstOrDefault() != null)
            {
                await Groups.RemoveAsync(Context.ConnectionId, groupName);
            }
        }
     

        public override Task OnConnectedAsync()
        {
            //SignalRConnection signalRconnection = new SignalRConnection();
            //var httpContext = Context.Connection.GetHttpContext();
            //var token = httpContext.Request.Query[setting.token].ToString();
            //var userId = httpContext.Request.Query[setting.userId].ToString();
            //var roles = httpContext.Request.Query[setting.roles].ToString();
            //var lstroles = roles.Split(',').ToList();

            //if (token != null && userId != null && lstroles != null)
            //{
            //    token = token.Remove(0, 7);
            //    if (connections.Where
            //        (cn =>
            //    {
            //        if (cn.UserId == userId && cn.Token == token)
            //            return cn.ConnectionId == Context.ConnectionId;
            //        return false;
            //    }).FirstOrDefault() == null)
            //    {
            //        signalRconnection.Token = token;
            //        signalRconnection.Roles = lstroles;
            //        signalRconnection.UserId = userId;
            //        signalRconnection.ConnectionId = Context.ConnectionId;
            //        connections.Add(signalRconnection);
            //    }
            //}
            return base.OnConnectedAsync();
        }
    }
}
