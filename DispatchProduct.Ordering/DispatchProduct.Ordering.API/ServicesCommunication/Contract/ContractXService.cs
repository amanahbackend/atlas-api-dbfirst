﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels.Contract;
using DispatchProduct.Ordering.API.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Contract
{
    public class ContractXService : DefaultHttpClientCrud<ContractServiceSetting, CONT_VM, CONT_VM>, IContractXService
    {
        ContractServiceSetting settings;
        public ContractXService(IOptions<ContractServiceSetting> _settings) : base(_settings.Value)
        {
            settings = _settings.Value;
        }
    }
}
