﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels.Contract;
using DispatchProduct.Ordering.API.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Contract
{
    public interface IContractXService : IDefaultHttpClientCrud<ContractServiceSetting, CONT_VM, CONT_VM>
    {

    }
}
