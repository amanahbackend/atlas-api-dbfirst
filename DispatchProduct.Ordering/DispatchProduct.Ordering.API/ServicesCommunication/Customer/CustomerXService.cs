﻿using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class CustomerXService : DefaultHttpClientCrud<CustomerServiceSetting, CUST_VM, CUST_VM>, ICustomerXService
    {
        CustomerServiceSetting settings;
        public CustomerXService(IOptions<CustomerServiceSetting> _settings) : base(_settings.Value)
        {
            settings = _settings.Value;
        }
   

        public async Task<CUST_VM> GetCustomer(int key, string authHeader = "")
        {
            CustomerXService customerService = this;
            string requesturi = $"{customerService.settings.Uri}/{customerService.settings.GetCustomerVerb}/{key}";
            return await customerService.GetByUri(requesturi, authHeader);
        }
    }
}