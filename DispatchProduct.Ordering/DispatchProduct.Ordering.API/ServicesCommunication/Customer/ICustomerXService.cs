﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public interface ICustomerXService : IDefaultHttpClientCrud<CustomerServiceSetting, CUST_VM, CUST_VM>
    {
        Task<CUST_VM> GetCustomer(int key, string authHeader = "");
    }
}
