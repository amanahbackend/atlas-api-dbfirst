﻿
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence
{
    public class PreventiveMaintainenceService : DefaultHttpClientCrud<PreventiveMaintainenceServiceSetting, PreventiveMaintainenceScheduleViewModel, PreventiveMaintainenceScheduleViewModel>, IPreventiveMaintainenceService
    {
        PreventiveMaintainenceServiceSetting settings;
        public PreventiveMaintainenceService(IOptions<PreventiveMaintainenceServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<List<PreventiveMaintainenceScheduleViewModel>> GetDailyPreventiveOrders(string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetDailyPreventiveOrdersVerb}";
            return await GetListByUri(requesturi, authHeader);
        }
        public async Task<bool> UpdatePreventiveMaintainenceByOrder(List<PreventiveOrderViewModel> model, string authHeader = "")
        {
            string requesturi = $"{settings.Uri}/{settings.UpdatePreventiveMaintainenceByOrderVerb}";
            return await PostCustomize<List<PreventiveOrderViewModel>, bool>(requesturi, model, authHeader);
        }
    }
}
