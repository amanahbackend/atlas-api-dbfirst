﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public interface ITechnicanUsedItemService : IDefaultHttpClientCrud<TechnicanUsedItemServiceSetting, List<TechnicanUsedItemsViewModel>, List<TechnicanUsedItemsViewModel>>
  {
    Task<List<TechnicanUsedItemsViewModel>> AddUsedItems(List<TechnicanUsedItemsViewModel> usedItems, string authHeader = "");
  }
}
