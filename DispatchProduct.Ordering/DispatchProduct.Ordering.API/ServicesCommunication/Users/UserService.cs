﻿using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class UserService : DefaultHttpClientCrud<UserServiceSetting, List<string>, List<ApplicationUserViewModel>>, IUserService
    {
        UserServiceSetting settings;
        public UserService(IOptions<UserServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<List<ApplicationUserViewModel>> GetByUserIds(List<string> userIds, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetByUserIdsVerb}";
            return await Post(requesturi, userIds, authHeader);
        }
        public async Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicans(List<string> userIds, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetUnAssignedTechnicansVerb}";
            return await Post(requesturi, userIds, authHeader);
        }
        public async Task<List<ApplicationUserViewModel>> GetDispatcherSupervisor(string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetDispatcherSupervisorVerb}";
            return await GetByUri(requesturi, authHeader);
        }
    }
}
