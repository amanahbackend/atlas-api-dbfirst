﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public interface IVehicleService : IDefaultHttpClientCrud<VehicleServiceSetting, VehicleViewModel, VehicleViewModel>
    {
        
    }
}
