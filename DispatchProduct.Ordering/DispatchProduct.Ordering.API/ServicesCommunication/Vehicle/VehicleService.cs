﻿using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class VehicleService : DefaultHttpClientCrud<VehicleServiceSetting, VehicleViewModel, VehicleViewModel>, IVehicleService
    {
        VehicleServiceSetting settings;
        public VehicleService(IOptions<VehicleServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
    }
}
