﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class VehicleServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }
  }
}
