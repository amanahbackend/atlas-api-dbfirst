﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesViewModels.Contract
{
    public class CONT_VM
    {
        public string real_name { get; set; }
        public string owner_name { get; set; }
        public string cont_no { get; set; }
        public DateTime? cont_date { get; set; }
        public DateTime? from_dt { get; set; }
        public DateTime? to_date { get; set; }
        public decimal contract_id { get; set; }
        public decimal customer_id { get; set; }
        public string contract_type { get; set; }
        public long? rn { get; set; }
    }
}
