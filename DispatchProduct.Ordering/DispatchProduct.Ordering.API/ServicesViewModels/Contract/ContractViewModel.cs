﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ServicesViewModels.ContractViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.API.ViewModel;
using System;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
  public class ContractViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string ContractNumber { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public double Price { get; set; }

    public string Remarks { get; set; }

    public bool HasPreventiveMaintainence { get; set; }

    public int PreventivePeriod { get; set; }

    public int FK_Customer_Id { get; set; }

    public int FK_ContractType_Id { get; set; }
  }
}
