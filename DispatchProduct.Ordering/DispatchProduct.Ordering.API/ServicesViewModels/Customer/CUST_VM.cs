﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
    public class CUST_VM
    {
        public string id { get; set; }
        public string name_ar { get; set; }
        public string name_en { get; set; }
        public string civil_id { get; set; }
        public string passport_no { get; set; }
        public string gendre { get; set; }
        public string nationality_id { get; set; }
        public string marital_status { get; set; }
        public string black_list { get; set; }
        public string active { get; set; }
        public string type { get; set; }
        public string address { get; set; }
        public string comm_sign_person_tel { get; set; }
        public string area_code { get; set; }
        public string sector { get; set; }
        public string street { get; set; }
        public string gada { get; set; }
        public string home_no { get; set; }
        public string notes { get; set; }
        public string comm_sign_person_civil_id { get; set; }
        public string kinder_person_tel_no { get; set; }
        public string comm_sign_person_nationality { get; set; }
        public List<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }

    }
}
