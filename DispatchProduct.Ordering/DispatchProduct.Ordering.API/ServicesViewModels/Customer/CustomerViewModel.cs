﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ServicesViewModels.CustomerViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
  public class CustomerViewModel : BaseEntityViewModel
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public string CivilId { get; set; }

    public string Remarks { get; set; }

    public int FK_CustomerType_Id { get; set; }

    public string PhoneNumber { get; set; }

    public int FK_CallType_Id { get; set; }

    public int FK_PhoneType_Id { get; set; }

    public ICollection<ComplainViewModel> Complains { get; set; }

    public List<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }

    public ICollection<LocationViewModel> Locations { get; set; }
  }
}
