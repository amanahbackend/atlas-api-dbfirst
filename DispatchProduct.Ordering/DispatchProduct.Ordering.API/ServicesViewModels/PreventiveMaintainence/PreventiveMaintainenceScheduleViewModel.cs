﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ViewModel.PreventiveMaintainenceScheduleViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using System;

namespace DispatchProduct.Ordering.API.ViewModel
{
    public class PreventiveMaintainenceScheduleViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string ContractNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public int FK_Customer_Id { get; set; }

        public int FK_Contract_Id { get; set; }

        public int FK_Location_Id { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public int FK_OrderType_Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }

        //public string QuotationRefNo { get; set; }

        public int FK_Order_Id { get; set; }
        public bool IsPrevintive { get; set; }

    }
}
