﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ApplicationUserViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using System;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.API
{
  public class ApplicationUserViewModel
  {
    public string PhoneNumber { get; set; }

    public string Id { get; set; }

    public bool PhoneNumberConfirmed { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public bool EmailConfirmed { get; set; }

    public bool IsAvailable { get; set; }

    public string Password { get; set; }

    public string FirstName { get; set; }

    public string MiddleName { get; set; }

    public string LastName { get; set; }

    public string FK_CreatedBy_Id { get; set; }

    public string FK_UpdatedBy_Id { get; set; }

    public string FK_DeletedBy_Id { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime UpdatedDate { get; set; }

    public DateTime DeletedDate { get; set; }

    public List<string> RoleNames { get; set; }
  }
}
