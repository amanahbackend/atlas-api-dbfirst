﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
    public class TrackingLocationViewModel:BaseEntityViewModel
    {
        public int Id { get; set; }
        public double Lat {get; set;}
        public double Long {get; set;}
    }
}
