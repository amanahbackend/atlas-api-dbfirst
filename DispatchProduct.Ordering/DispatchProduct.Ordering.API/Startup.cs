﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using DispatchProduct.Inventory.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Ordering.Settings;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Repoistry;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.IEntities;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.CustomerModule.API.Controllers;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using Hangfire;
using System.Diagnostics;
using DispatchProduct.Ordering.API.Tasks;
using HangfireCore.HangfireActivitaor;
using DispatchProduct.Ordering.API.Controllers;
using DispatchProduct.Ordering.Hubs;
using Microsoft.Extensions.FileProviders;
using System.IO;
using System.Net;
using System.Net.Sockets;
using BuildingBlocks.ServiceDiscovery;
using DnsClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Contract;

namespace DispatchProduct.Ordering.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                 .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                 .AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddDbContext<OrderDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Ordering.EFCore.MSSQL")));

            services.AddSingleton(provider => Configuration);

            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            //services.AddApiVersioning(o => o.ReportApiVersions = true);

            services.AddOptions();
            services.AddOptions();
            services.Configure<OrderAppSettings>(Configuration);
            services.Configure<OrderingHubSettings>(Configuration.GetSection("OrderingHubSettings"));
            services.Configure<CustomerServiceSetting>(Configuration.GetSection("CustomerServiceSetting"));
            services.Configure<VehicleServiceSetting>(Configuration.GetSection("VehicleServiceSetting"));
            services.Configure<ContractServiceSetting>(Configuration.GetSection("ContractServiceSetting"));
            services.Configure<TechnicanUsedItemServiceSetting>(Configuration.GetSection("TechnicanUsedItemServiceSetting"));
            services.Configure<LocationServiceSetting>(Configuration.GetSection("LocationServiceSetting"));
            services.Configure<UserServiceSetting>(Configuration.GetSection("UserServiceSetting"));
            services.Configure<PreventiveMaintainenceServiceSetting>(Configuration.GetSection("PreventiveMaintainenceServiceSetting"));
            services.Configure<TokenServiceSetting>(Configuration.GetSection("TokenServiceSetting"));


            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1",
            //        new Info()
            //        {
            //            Title = "Calling API",
            //            Description = "Calling  API"
            //        });
            //    c.AddSecurityDefinition("oauth2", new OAuth2Scheme
            //    {
            //        Type = "oauth2",
            //        Flow = "implicit",
            //        AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
            //        TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
            //        Scopes = new Dictionary<string, string>()
            //        {
            //            { "calling", "calling API" }
            //        }
            //    });
            //});

            ConfigureAuthService(services);

            services.AddMvc();
            services.AddSignalR();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, OrderDbContext>();
            services.AddScoped(typeof(IOrderProblem), typeof(OrderProblem));
            services.AddScoped(typeof(IOrderDistributionCriteria), typeof(OrderDistributionCriteria));
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IOrder), typeof(Order));
            services.AddScoped(typeof(IOrderPriority), typeof(OrderPriority));
            services.AddScoped(typeof(IOrderStatus), typeof(OrderStatus));
            services.AddScoped(typeof(IOrderType), typeof(OrderType));
            services.AddTransient(typeof(IOrderProgress), typeof(OrderProgress));
            services.AddScoped(typeof(IAssignedTechnicans), typeof(AssignedTechnicans));
            services.AddScoped(typeof(IProgressStatus), typeof(ProgressStatus));
            services.AddScoped(typeof(IOrderManager), typeof(OrderManager));
            services.AddScoped(typeof(IOrderPriorityManager), typeof(OrderPriorityManager));
            services.AddScoped(typeof(IOrderStatusManager), typeof(OrderStatusManager));
            services.AddScoped(typeof(IOrderTypeManager), typeof(OrderTypeManager));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            services.AddScoped(typeof(ICustomerXService), typeof(CustomerXService));
            services.AddScoped(typeof(IVehicleService), typeof(VehicleService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(ILocationService), typeof(LocationService));
            services.AddScoped(typeof(IPreventiveMaintainenceService), typeof(PreventiveMaintainenceService));
            services.AddScoped(typeof(ITokenService), typeof(TokenService));
            services.AddScoped(typeof(IOrderProgressManager), typeof(OrderProgressManager));
            services.AddScoped(typeof(IAssignedTechnicansManager), typeof(AssignedTechnicansManager));
            services.AddScoped(typeof(IProgressStatusManager), typeof(ProgressStatusManager));
            services.AddScoped(typeof(IOrderProblemManager), typeof(OrderProblemManager));
            services.AddScoped(typeof(IOrderDistributionCriteriaManager), typeof(OrderDistributionCriteriaManager));
            services.AddScoped(typeof(OrderController), typeof(OrderController));
            services.AddTransient(typeof(IPreventiveOrderTask), typeof(PreventiveOrderTask));
            services.AddTransient(typeof(IContractService), typeof(ContractService));
            services.AddTransient(typeof(IContractXService), typeof(ContractXService));
            services.AddTransient(typeof(ITechnicanUsedItemService), typeof(TechnicanUsedItemService));
            services.AddTransient(typeof(ILocationService), typeof(LocationService));
            services.AddTransient(typeof(IOrderingHub), typeof(OrderingHub));
            //services.AddHangfire(x => x.UseSqlServerStorage(Configuration["ConnectionString"]));
            var container = new ContainerBuilder();
            container.Populate(services);
            var serviceProvider = new AutofacServiceProvider(container.Build());
           // GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(serviceProvider));
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");
            //app.UseHangfireDashboard();
            //app.UseHangfireServer();
            //RecurringJob.AddOrUpdate<IPreventiveOrderTask>(pre => pre.AddPreventiveOrder(), Cron.Daily);
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //var pathBase = Configuration["PATH_BASE"];
            //if (!string.IsNullOrEmpty(pathBase))
            //{
            //    loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
            //    app.UsePathBase(pathBase);
            //}

            app.UseSignalR(routes =>  // <-- SignalR
            {
                routes.MapHub<OrderingHub>("orderingHub/negotiate");
            });

            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                 Path.Combine(Directory.GetCurrentDirectory(), "OrderSignatures")),
                RequestPath = "/OrderSignatures",
                EnableDirectoryBrowsing = true
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
               Path.Combine(Directory.GetCurrentDirectory(), "OrderContractSignatures")),
                RequestPath = "/OrderContractSignatures",
                EnableDirectoryBrowsing = true
            });
            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            //});


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //app.UseConsulRegisterService();
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
