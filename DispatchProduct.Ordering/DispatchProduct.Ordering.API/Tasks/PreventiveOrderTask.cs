﻿using AutoMapper;
using DispatchProduct.Ordering.API.Controllers;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Ordering.API.Tasks
{
    public class PreventiveOrderTask: IPreventiveOrderTask
    {
        private OrderController ordController;
        public readonly IMapper mapper;
        private OrderAppSettings appSettings;
        private IPreventiveMaintainenceService preventiveService;
        private ITokenService tokenService;

        public PreventiveOrderTask(OrderController _ordController, IMapper _mapper, IOptions<OrderAppSettings> _appSettings, IPreventiveMaintainenceService _preventiveService, ITokenService _tokenService)
        {
            this.tokenService = _tokenService;
            this.ordController = _ordController;
            this.mapper = _mapper;
            this.appSettings = _appSettings.Value;
            this.preventiveService = _preventiveService;
        }
        //public void StartPreventiveOrderTask()
        //{
        //    var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute+ appSettings.PreventiveOrderHour, 0);
        //    StartPreventiveOrderTask(date);
        //}
        //private void StartPreventiveOrderTask(DateTime date)
        //{

        //    var m_ctSource = new CancellationTokenSource();

        //    var dateNow = DateTime.Now;
        //    TimeSpan ts;
        //    if (date > dateNow)
        //        ts = date - dateNow;
        //    else
        //    {
        //        //date = date.AddDays(1);
        //        date = date.AddMinutes(1);
        //        ts = date - dateNow;
        //    }
        //    Task.Delay(ts).ContinueWith((x) =>
        //    {
        //        PreventiveOrderProcess();
        //        // StartPreventiveOrderTask(date.AddDays(1));
        //         StartPreventiveOrderTask(date.AddMinutes(1));

        //    }, m_ctSource.Token);
        //}
        //private void PreventiveOrderProcess(string authHeader = null)
        //{
        //    PreventiveDelegate preventive = AddPreventiveOrder;
        //    Task.Run(() => preventive.Invoke());
        //    //IAsyncResult result = preventive.BeginInvoke(null, null);
        //    //preventive.EndInvoke(result);
        //}
        public async Task  AddPreventiveOrder()
        {
            List<PreventiveOrderViewModel> preventiveOrders = new List<PreventiveOrderViewModel>();
            var preventiveSchedulesRes = await  preventiveService.GetDailyPreventiveOrders(null);
            string token = await tokenService.GetSysToken();
            foreach (var item in preventiveSchedulesRes)
          {
                var model = mapper.Map<PreventiveMaintainenceScheduleViewModel, OrderViewModel>(item);
                var order = mapper.Map<PreventiveMaintainenceScheduleViewModel, Order>(item);
                try
                {
                    ProcessResult<OrderViewModel> processResult = await ordController.AddOrder(model, token);
                    if (processResult.IsSucceeded)
                    {
                        if (processResult.returnData != null)
                            preventiveOrders.Add(new PreventiveOrderViewModel()
                            {
                                FK_Order_Id = processResult.returnData.Id,
                                FK_PreventiveMaintainence_Id = item.Id
                            });
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
          }
        }
    }
}
