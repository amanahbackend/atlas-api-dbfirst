﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.Entities.AreaProblems
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class AreaProblems : BaseEntityViewModel
    {
        public List<int> FK_OrderProblem_Ids { get; set; }

        public List<OrderProblemViewModel> OrderProblems { get; set; }

        public string Area { get; set; }

        public string Governorate { get; set; }
    }
}
