﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ViewModel
{
    public class AreasProblems : BaseEntityViewModel
    {
        public List<int> FK_OrderProblem_Ids { get; set; }

        public List<OrderProblemViewModel> OrderProblems { get; set; }

        public List<string> Areas { get; set; }

        public string Governorate { get; set; }
    }
}
