﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ViewModel.BaseEntityViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using System;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class BaseEntityViewModel
  {
    public string FK_CreatedBy_Id { get; set; }

    public string FK_UpdatedBy_Id { get; set; }

    public string FK_DeletedBy_Id { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime UpdatedDate { get; set; }

    public DateTime DeletedDate { get; set; }
  }
}
