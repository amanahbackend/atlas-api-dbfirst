﻿using System.Collections.Generic;
using Utilites.UploadFile;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class CustomerSignatureViewModel : BaseEntityViewModel
  {
    public UploadFile File { get; set; }

    public UploadFile ContractSignatureFile { get; set; }

    public List<int> UsedItems { get; set; }

    public OrderProgressViewModel OrderProgress { get; set; }
  }
}
