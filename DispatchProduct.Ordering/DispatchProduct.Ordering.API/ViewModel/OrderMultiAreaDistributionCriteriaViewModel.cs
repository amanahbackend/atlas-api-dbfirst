﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ViewModel
{
    public class OrderMultiAreaDistributionCriteriaViewModel
    {
        public List<AreasProblems> AreasProblems { get; set; }

        public string FK_Dispatcher_Id { get; set; }

        public ApplicationUserViewModel Dispatcher { get; set; }
    }
}
