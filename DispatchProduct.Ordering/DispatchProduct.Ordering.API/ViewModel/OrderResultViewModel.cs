﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ViewModel
{
    public class OrderResultViewModel
    {
        public string Code { get; set; }
        public string TechnicanName { get; set; }
        public string DispatcherName { get; set; }
        public double Price { get; set; }
        public string Area { get; set; }
        public string Problem { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PreferedVisitTime { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public string Note { get; set; }
    }
}
