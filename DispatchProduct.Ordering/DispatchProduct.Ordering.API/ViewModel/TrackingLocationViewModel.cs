﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.Entities.TrackingLocationViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.API.ViewModel;

namespace DispatchProduct.Ordering.Entities
{
  public class TrackingLocationViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public double Lat { get; set; }

    public double Long { get; set; }
  }
}
