﻿

using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderManager : IRepositry<Order>
    {
        List<Order> Search(string orderCode);

        List<Order> SearchByCustomerId(int customerId);

        List<Order> GetAllOrderFilledProps();

        OrderProgress AddOrderProgress(OrderProgress orderProgress);

        List<Order> GetOrdersAssignedToTechnicans(List<string> technicanIds);

        List<Order> GetOrdersAssignedToTechnican(string technicanId);

        List<Order> GetDispatcherOrders(string dispatcherId);

        List<Order> GetOrdersAssignedToDispatchers(List<string> dispatcherIds);

        List<Order> GetFilteredOrderByDispatcherId(FilterOrderByDispatcher filter);

        List<Order> GetOrdersAssignedToDispatcher(string dispatcherId);

        Order Get(int id);

        bool AssignOrderToTechnican(int orderId, string technicanId);

        bool AssignOrderToDispatcher(int orderId, string dispatcherId);

        bool TransferOrderToDispatcher(int orderId, string dispatcherId);

        bool SetPreferedVisitTime(int orderId, DateTime PreferedVisitTime);

        string UnAssignOrderToTechnican(int orderId);

        List<Order> GetAllDispatcherOrders(string dispatcherId);

        bool AssignOrderToDispatcher_Technican(int orderId, string dispatcherId, string technicanId);

        bool AssignOrdersToDispatcher(List<int> orderIds, string dispatcherId);

        bool AssignOrdersToTechnican(List<int> orderIds, string technicanId);

        bool AssignOrdersToDispatcher_Technican(List<int> orderIds, string dispatcherId, string technicanId);
        List<Order> FillOrdersNavProps(List<Order> orders);

        List<Order> GetPMOrders();
    }
}
