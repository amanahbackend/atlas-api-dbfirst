﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderProblemManager : IRepositry<OrderProblem>
    {
    }
}
