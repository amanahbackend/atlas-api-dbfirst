﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderStatusManager : IRepositry<OrderStatus>
    {
        OrderStatus GetIntialOrderStatus();
        OrderStatus GetCustomerNotRespondStatus();
    }
}
