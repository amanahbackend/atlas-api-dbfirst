﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class AssignedTechnicansManager : Repositry<AssignedTechnicans>, IAssignedTechnicansManager
    {
        IOrderManager ordermanager;
        public AssignedTechnicansManager(OrderDbContext context, IOrderManager _Ordermanager)
            : base(context)
        {
            ordermanager = _Ordermanager;
        }
        public List<Technican> GetByDispatcherId(string fk_Dispatcher_Id)
        {
            List<Technican> result = new List<Technican>();
            //var technicans = GetAll().Where(t => t.FK_Dispatcher_Id == fk_Dispatcher_Id).ToList();
            var technicans = GetAll().ToList();
            var technicanIds = technicans.Select(tk => tk.FK_Technican_Id).ToList();
            foreach (var technicanId in technicanIds)
            {
                List<Order> orders = ordermanager.GetOrdersAssignedToTechnican(technicanId);
                result.Add(new Technican()
                {
                    Orders = orders,
                    Id = technicanId
                });
            }
            return result;
        }

        public override AssignedTechnicans Add(AssignedTechnicans entity)
        {
           var technican= GetByTechnicanId(entity.FK_Technican_Id);
            if (technican != null)
            {
                technican.FK_Dispatcher_Id = entity.FK_Dispatcher_Id;
                Update(technican);
            }
            else
            {
                technican = base.Add(entity);
            }
            return technican;
        }
        public bool UpdateAssignedTechnican(AssignedTechnicans assignedTechnican)
        {
          var technican=  GetByTechnicanId(assignedTechnican.FK_Technican_Id);
            technican.FK_Dispatcher_Id = assignedTechnican.FK_Dispatcher_Id;
           return Update(technican);
        }
        public bool DeleteAssignedTechnican(string technicanId)
        {
           return Delete(GetAll().Where(t => t.FK_Technican_Id == technicanId).ToList());
        }
        public AssignedTechnicans GetByTechnicanId(string technicanId)
        {
            return GetAll().Where(t => t.FK_Technican_Id == technicanId).FirstOrDefault();
        }
    }

}
