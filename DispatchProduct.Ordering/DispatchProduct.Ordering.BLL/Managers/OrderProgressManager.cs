﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.BLL.Managers.OrderProgressManager
// Assembly: DispatchProduct.Ordering.BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F0ECB4BD-FA66-4F49-A2C8-C68D6C6E9E15
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.BLL.dll

using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.IEntities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderProgressManager : Repositry<OrderProgress>, IOrderProgressManager, IRepositry<OrderProgress>
    {
        private IServiceProvider serviceProvider;
        private IProgressStatusManager orderProgressStatusManager;

        public OrderProgressManager(OrderDbContext context, IProgressStatusManager _orderProgressStatusManager, IServiceProvider _serviceProvider)
          : base(context)
        {
            orderProgressStatusManager = _orderProgressStatusManager;
            serviceProvider = _serviceProvider;
        }

        public List<OrderProgress> GetOrderProgressByOrderId(int orderId)
        {
            List<OrderProgress> list = GetAll().Where(ord => ord.FK_Order_Id == orderId).ToList();
            foreach (OrderProgress orderProgress in list)
                orderProgress.ProgressStatus = orderProgressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
            return list;
        }

        public OrderProgress AddAssignedProgress(IOrder order)
        {
            OrderProgress entity = (OrderProgress)serviceProvider.GetService<IOrderProgress>();
            if (entity != null)
            {
                entity.FK_Technican_Id = order.FK_Technican_Id;
                entity.FK_Order_Id = order.Id;
                ProgressStatus assignedValue = orderProgressStatusManager.GetAssignedValue();
                if (assignedValue != null)
                    entity.FK_ProgressStatus_Id = assignedValue.Id;
                if (order.FK_Dispatcher_Id != null)
                {
                    entity.FK_CreatedBy_Id = order.FK_Dispatcher_Id;
                }
                else
                {
                    entity.FK_CreatedBy_Id = "";
                }
                entity = Add(entity);
            }
            return entity;
        }

        public OrderProgress AddTransferProgress(IOrder order)
        {
            OrderProgress entity = (OrderProgress)serviceProvider.GetService<IOrderProgress>();
            if (entity != null)
            {
                entity.FK_Technican_Id = order.FK_Technican_Id;
                entity.FK_Order_Id = order.Id;
                ProgressStatus transferValue = orderProgressStatusManager.GetTransferValue();
                if (transferValue != null)
                    entity.FK_ProgressStatus_Id = transferValue.Id;
                entity.FK_CreatedBy_Id = order.FK_Dispatcher_Id;
                entity = Add(entity);
            }
            return entity;
        }

        public override OrderProgress Add(OrderProgress entity)
        {
            return base.Add(entity);
        }
    }
}
