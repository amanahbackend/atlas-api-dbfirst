﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.BLL.Managers.OrderStatusManager
// Assembly: DispatchProduct.Ordering.BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F0ECB4BD-FA66-4F49-A2C8-C68D6C6E9E15
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.BLL.dll

using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderStatusManager : Repositry<OrderStatus>, IOrderStatusManager, IRepositry<OrderStatus>
    {
        private OrderAppSettings settings;

        public OrderStatusManager(OrderDbContext context, IOptions<OrderAppSettings> _settings)
          : base(context)
        {
            settings = _settings.Value;
        }

        public OrderStatus GetIntialOrderStatus()
        {
            OrderStatus orderStatus = null;
            if (!string.IsNullOrEmpty(settings.IntialProgressStatusKey))
                orderStatus = GetAll().Where(st => st.Name == settings.IntialProgressStatusKey)
                    .FirstOrDefault();
            return orderStatus;
        }

        public OrderStatus GetCustomerNotRespondStatus()
        {
            var status = GetAll().FirstOrDefault(x => x.Name.Equals("Not Responding"));
            return status;
        }
    }
}
