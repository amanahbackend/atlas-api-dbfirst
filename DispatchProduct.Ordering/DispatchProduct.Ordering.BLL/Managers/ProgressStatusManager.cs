﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.BLL.Managers.ProgressStatusManager
// Assembly: DispatchProduct.Ordering.BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F0ECB4BD-FA66-4F49-A2C8-C68D6C6E9E15
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.BLL.dll

using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class ProgressStatusManager : Repositry<ProgressStatus>, IProgressStatusManager, IRepositry<ProgressStatus>
    {
        private OrderAppSettings ordeAppSetings;
        private IOrderStatusManager statusManager;

        public ProgressStatusManager(OrderDbContext context, IOptions<OrderAppSettings> _ordeAppSetings, IOrderStatusManager _statusManager)
          : base(context)
        {
            ordeAppSetings = _ordeAppSetings.Value;
            statusManager = _statusManager;
        }

        public ProgressStatus GetAssignedValue()
        {
            return GetAll().Where(p => p.Name == ordeAppSetings.AssignedProgressStatusKey)
                .FirstOrDefault();
        }

        public ProgressStatus GetTransferValue()
        {
            return GetAll().Where(p => p.Name == ordeAppSetings.TransferProgressStatusKey)
                .FirstOrDefault();
        }

        public override ProgressStatus Add(ProgressStatus model)
        {
            ProgressStatus progressStatus = base.Add(model);
            progressStatus.OrderStatus = statusManager.Get(model.FK_OrderStatus_Id);
            return progressStatus;
        }

        public List<ProgressStatus> GetAll()
        {
            List<ProgressStatus> list = base.GetAll().ToList();
            for (int index = 0; index < list.Count; ++index)
                list[index].OrderStatus = statusManager.Get(list[index].FK_OrderStatus_Id);
            return list;
        }
    }
}
