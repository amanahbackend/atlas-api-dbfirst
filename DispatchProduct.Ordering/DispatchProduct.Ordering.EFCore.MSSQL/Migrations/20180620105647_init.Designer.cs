﻿// <auto-generated />
using DispatchProduct.Inventory.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace DispatchProduct.Ordering.EFCore.MSSQL.Migrations
{
    [DbContext(typeof(OrderDbContext))]
    [Migration("20180620105647_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("Relational:Sequence:.OrderDistributionCriteriaseq", "'OrderDistributionCriteriaseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:.OrderPriorityseq", "'OrderPriorityseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:.OrderProblemseq", "'OrderProblemseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:.OrderProgressseq", "'OrderProgressseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:.OrderStatusseq", "'OrderStatusseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:.OrderTypeseq", "'OrderTypeseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:.ProgressStatusseq", "'ProgressStatusseq', '', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.AssignedTechnicans", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderStatusseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_Dispatcher_Id")
                        .IsRequired();

                    b.Property<string>("FK_Technican_Id")
                        .IsRequired();

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("AssignedTechnicans");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Area");

                    b.Property<string>("Code")
                        .IsRequired();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<DateTime>("EndDate");

                    b.Property<int>("FK_Contract_Id");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<int>("FK_Customer_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_Dispatcher_Id");

                    b.Property<int>("FK_Location_Id");

                    b.Property<int>("FK_OrderPriority_Id");

                    b.Property<int>("FK_OrderProblem_Id");

                    b.Property<int>("FK_OrderStatus_Id");

                    b.Property<int>("FK_OrderType_Id");

                    b.Property<string>("FK_Technican_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Note");

                    b.Property<int?>("OrderProblemId");

                    b.Property<DateTime>("PreferedVisitTime");

                    b.Property<double>("Price");

                    b.Property<string>("SignatureContractPath");

                    b.Property<string>("SignaturePath");

                    b.Property<DateTime>("StartDate");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.HasIndex("OrderProblemId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.OrderDistributionCriteria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderDistributionCriteriaseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<string>("Area")
                        .IsRequired();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_Dispatcher_Id")
                        .IsRequired();

                    b.Property<int>("FK_OrderProblem_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<string>("Governorate")
                        .IsRequired();

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("OrderDistributionCriteria");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.OrderPriority", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderPriorityseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("OrderPriority");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.OrderProblem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderProblemseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("OrderProblem");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.OrderProgress", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderProgressseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id")
                        .IsRequired();

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<int>("FK_Order_Id");

                    b.Property<int>("FK_ProgressStatus_Id");

                    b.Property<string>("FK_Technican_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<string>("Note");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("OrderProgress");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.OrderStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderStatusseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("OrderStatus");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.OrderType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "OrderTypeseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("OrderType");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.ProgressStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:HiLoSequenceName", "ProgressStatusseq")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("DeletedDate");

                    b.Property<string>("FK_CreatedBy_Id");

                    b.Property<string>("FK_DeletedBy_Id");

                    b.Property<int>("FK_OrderStatus_Id");

                    b.Property<string>("FK_UpdatedBy_Id");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.ToTable("ProgressStatus");
                });

            modelBuilder.Entity("DispatchProduct.Ordering.Entities.Order", b =>
                {
                    b.HasOne("DispatchProduct.Ordering.Entities.OrderProblem", "OrderProblem")
                        .WithMany()
                        .HasForeignKey("OrderProblemId");
                });
#pragma warning restore 612, 618
        }
    }
}
