﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class AssignedTechnicans : BaseEntity, IAssignedTechnicans
    {
        public int Id { get; set; }

        public string FK_Technican_Id { get; set; }

        public string FK_Dispatcher_Id { get; set; }
    }
}
