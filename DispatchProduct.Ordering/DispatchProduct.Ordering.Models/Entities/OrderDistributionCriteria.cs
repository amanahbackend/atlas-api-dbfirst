﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderDistributionCriteria : BaseEntity, IOrderDistributionCriteria, IBaseEntity
    {
        public int Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }

        public OrderProblem OrderProblem { get; set; }

        public string Area { get; set; }

        public string Governorate { get; set; }

        public string FK_Dispatcher_Id { get; set; }

        public ApplicationUser Dispatcher { get; set; }
    }
}
