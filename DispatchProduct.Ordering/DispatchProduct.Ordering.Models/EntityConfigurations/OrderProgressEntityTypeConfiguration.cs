﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderProgressEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderProgress>
    {
        public void Configure(EntityTypeBuilder<OrderProgress> OrderProgressConfiguration)
        {
            OrderProgressConfiguration.ToTable("OrderProgress");
            OrderProgressConfiguration.HasKey(o => o.Id);
            OrderProgressConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("OrderProgressseq");
            OrderProgressConfiguration.Property(o => o.FK_Technican_Id).IsRequired(false);
            OrderProgressConfiguration.Property(o => o.FK_Order_Id).IsRequired(true);
            OrderProgressConfiguration.Property(o => o.FK_CreatedBy_Id).IsRequired(true);
            OrderProgressConfiguration.Property(o => o.FK_ProgressStatus_Id).IsRequired(true);
            OrderProgressConfiguration.Property(o => o.Note).IsRequired(false);
            OrderProgressConfiguration.Property(o => o.Latitude).IsRequired(false);
            OrderProgressConfiguration.Property(o => o.Longitude).IsRequired(false);
            OrderProgressConfiguration.Ignore(o => o.ProgressStatus);
            OrderProgressConfiguration.Ignore(o => o.CreatedBy);
            OrderProgressConfiguration.Ignore(o => o.Order);
        }
    }
}
