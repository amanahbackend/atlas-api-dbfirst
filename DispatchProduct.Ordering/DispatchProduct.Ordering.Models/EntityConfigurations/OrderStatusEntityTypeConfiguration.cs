﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderStatusEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderStatus>
    {
        public void Configure(EntityTypeBuilder<OrderStatus> OrderStatusConfiguration)
        {
            OrderStatusConfiguration.ToTable("OrderStatus");

            OrderStatusConfiguration.HasKey(o => o.Id);

            OrderStatusConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderStatusseq");


            OrderStatusConfiguration.Property(o => o.Name)
                .IsRequired();
        }
    }
}
