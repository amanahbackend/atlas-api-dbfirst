﻿namespace DispatchProduct.Ordering.Entities
{
    public interface IOrderType
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}