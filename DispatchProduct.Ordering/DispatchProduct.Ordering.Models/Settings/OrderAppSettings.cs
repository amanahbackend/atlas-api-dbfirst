﻿namespace DispatchProduct.Ordering.Settings
{
    public class OrderAppSettings
    {
        public bool UseCustomizationData { get; set; }

        public string IdentityUrl { get; set; }

        public string CustomerUrl { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }

        public string OrderPrefix { get; set; }

        public string OrderDayStartNo { get; set; }

        public string OrderCancelStatus { get; set; }

        public string OrderCompleteStatus { get; set; }

        public string IntialProgressStatusKey { get; set; }

        public string AssignedProgressStatusKey { get; set; }

        public string TransferProgressStatusKey { get; set; }

        public string SignaturePath { get; set; }
        public string SignatureContractPath { get; set; }
        public string MapDestinationMatrixService { get; set; }
        public string MapAPIKey { get; set; }


        
    }
}
