﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Entities;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.UserManagment.API;
using IdentityServer4.AccessTokenValidation;

namespace DispatchProduct.UserManagment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    //[Authorize(Roles = "Admin")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class RoleController : Controller
    {
        private readonly IApplicationRoleManager manger;
        public readonly IMapper mapper;
        public RoleController(IApplicationRoleManager _manger, IMapper _mapper)
        {
            mapper = _mapper;
            manger = _manger;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationRoleViewModel model)
        {
            ApplicationRoleViewModel result = new ApplicationRoleViewModel();
            var entityResult = new ApplicationRole();
            entityResult = mapper.Map<ApplicationRoleViewModel, ApplicationRole>(model);
            entityResult = await manger.GetRoleAsync(entityResult);
            result = mapper.Map<ApplicationRole, ApplicationRoleViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        //[Authorize]
        public IActionResult Get()
        {
            var entityResult = manger.GetAllRoles();
            var result = mapper.Map<List<ApplicationRole>, List<ApplicationRoleViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ApplicationRoleViewModel model)
        {
            ApplicationRoleViewModel result = new ApplicationRoleViewModel();
            ApplicationRole entityResult = new ApplicationRole();
            entityResult = mapper.Map<ApplicationRoleViewModel, ApplicationRole>(model);
            entityResult = await manger.AddRoleAsyncronous(entityResult);
            result = mapper.Map<ApplicationRole, ApplicationRoleViewModel>(entityResult);
            return Ok(result);

        }
        #endregion
        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ApplicationRoleViewModel model)
        {
            bool result = false;
            ApplicationRole entityResult = new ApplicationRole();
            entityResult = mapper.Map<ApplicationRoleViewModel, ApplicationRole>(model);
            result = await manger.UpdateRoleAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{roleName}")]
        [HttpDelete]
        //[Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromRoute]string roleName)
        {
            bool result = false;
            ApplicationRole entity = await manger.GetRoleAsyncByName(roleName);
            result = await manger.DeleteRoleAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}