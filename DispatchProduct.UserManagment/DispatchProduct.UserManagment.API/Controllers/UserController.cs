﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.BLL.IManagers;
using Utilites;
using DispatchProduct.UserManagment.API;
using Microsoft.AspNetCore.Identity;
using IdentityServer4.AccessTokenValidation;
using Microsoft.Extensions.Options;
using DispatchProduct.Identity.Settings;
using DispatchProduct.Identity.BLL.BLL.IManagers;

namespace DispatchProduct.UserManagment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {

        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        IApplicationUserManager appUserManager;
        private readonly IUserDeviceManager _userDeviceManager;

        AppSettings appSettings;
        public UserController(IMapper _mapper, UserManager<ApplicationUser> _userManager,
            IApplicationUserManager _appuserManager, IOptions<AppSettings> _appSettings,
            IUserDeviceManager userDeviceManager)
        {
            this.mapper = _mapper;
            this.userManager = _userManager;
            appUserManager = _appuserManager;
            appSettings = _appSettings.Value;
            _userDeviceManager = userDeviceManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region DefaultCrudOperation
        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationUserViewModel model)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            ApplicationUser entityResult = new ApplicationUser();
            entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(result);
            entityResult = await appUserManager.GetAsync(entityResult);
            result = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Get()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetAll();
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetByUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public List<ApplicationUserViewModel> GetByUserIds([FromBody]List<string> userIds)
        {
            List<ApplicationUser> entityResult = appUserManager.GetByUserIds(userIds);
            List<ApplicationUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

        [Route("GetTechnicansExceptUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<List<ApplicationUserViewModel>> GetTechnicansExceptUserIds([FromBody]List<string> userIds)
        {
            List<ApplicationUser> entityResult = await appUserManager.GetByRoleExcept(appSettings.TechnicanRole, userIds);
            List<ApplicationUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }
        [Route("GetAllExceptUserIds")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public List<ApplicationUserViewModel> GetByRoleExcept([FromBody]List<string> userIds)
        {
            List<ApplicationUser> entityResult = appUserManager.GetAllExcept(userIds);
            List<ApplicationUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }
        [Route("GetTechnicans")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetTechnicans()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetByRoleExcept(appSettings.TechnicanRole);
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatchers")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetDispatchers()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetByRoleExcept(appSettings.DispatcherRole);
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatcherSupervisor")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetDispatcherSupervisor()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetByRoleExcept(appSettings.SupervisorDispatcherRole);
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        [HttpGet]
        [Route("UserRoles/{username}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ApplicationUserViewModel> UserRoles([FromRoute]string username, string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrWhiteSpace(username))
            {
                var user = await appUserManager.GetByUserNameAsync(username);
                if (user != null)
                {
                    user.RoleNames = (await appUserManager.GetRolesAsync(username)).ToList();
                    result = mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    return result;
                }

            }
            return result;
        }


        #region PostApi
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody] ApplicationUserViewModel model)
        {
            IActionResult result = BadRequest();
            if (ModelState.IsValid)
            {
                var entityResult = new ApplicationUser();
                entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(model);
                entityResult = await appUserManager.AddUserAsync(entityResult, model.Password);
                model = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
                if (model != null)
                {
                    result = Ok(model);
                }
            }
            return result;
        }
        #endregion


        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

        public async Task<IActionResult> Update([FromBody] EditApplicationUserViewModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                var entityResult = mapper.Map<EditApplicationUserViewModel, ApplicationUser>(model);
                result = await appUserManager.UpdateUserAsync(entityResult);
            }
            return Ok(result);
        }
        #region DeleteApi
        [Route("Delete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]string username)
        {
            bool result = false;
            var entity = await appUserManager.GetByUserNameAsync(username);
            result = await appUserManager.DeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [HttpGet]
        [Route("UsernameExists/{username}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

        public async Task<IActionResult> UsernameExists([FromRoute]string username)
        {
            bool result = await appUserManager.IsUserNameExistAsync(username);
            return Ok(result);
        }

        [HttpGet]
        [Route("EmailExists/{email}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> EmailExists([FromRoute]string email)
        {
            bool result = await appUserManager.IsEmailExistAsync(email);
            return Ok(result);
        }

        [HttpPost]
        [Route("AssignUserToRole")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AssignUserToRole([FromBody]AssignRoleViewModel assignRoleViewModel)
        {

            bool result = await appUserManager.AddUserToRoleAsync(assignRoleViewModel.UserName, assignRoleViewModel.RoleName);
            return Ok(result);
        }
        [HttpPost]
        [Route("ChangeUserAvailability")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<bool> ChangeUserAvailability([FromBody]UserAvailableViewModel userAvailableViewModel)
        {
            bool result = false;
            result = await appUserManager.ChangeUserAvailabiltyAsync(userAvailableViewModel.Id, userAvailableViewModel.IsAvailable);
            return result;
        }

        [HttpGet, Route("GetDevices/{userId}"), MapToApiVersion("1.0")]
        public List<UserDevice> GetDevices([FromRoute] string userId)
        {
            var result = _userDeviceManager.GetByUserId(userId);
            return result;
        }

        [HttpDelete, Route("DeleteDevice/{token}"), MapToApiVersion("1.0")]
        public IActionResult DeleteDevice([FromRoute] string token)
        {
            var result = _userDeviceManager.DeleteDevice(token);
            return Ok(result);
        }

        [HttpGet, Route("ResetPassword")]
        public IActionResult ResetPassword([FromQuery] string username, string newPass)
        {
            var result = appUserManager.ResetPassword(username, newPass);
            return Ok(result);
        }
    }
}