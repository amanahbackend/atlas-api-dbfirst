﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DispatchProduct.UserManagment.API
{
    public class EditApplicationUserViewModel
    {
        public string PhoneNumber { get; set; }

        public string Id { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool IsAvailable { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string FK_CreatedBy_Id { get; set; }

        public string FK_UpdatedBy_Id { get; set; }

        public string FK_DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }

        public List<string> RoleNames { get; set; }
    }
}
