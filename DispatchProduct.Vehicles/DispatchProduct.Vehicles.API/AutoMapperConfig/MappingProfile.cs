﻿using AutoMapper;
using DispatchProduct.Vehicles.API.ViewModel;
using DispatchProduct.Vehicles.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<VehicleViewModel, Vehicle>();
            CreateMap<Vehicle, VehicleViewModel>();

            CreateMap<VehicleTechnicanViewModel, VehicleTechnican>();
            CreateMap<VehicleTechnican, VehicleTechnicanViewModel>();
        }
    }
}
