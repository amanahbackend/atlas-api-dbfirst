﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.BLL.Managers;
using Utilites.ProcessingResult;
using DispatchProduct.Vehicles.API.ViewModel;
using DispatchProduct.Vehicles.Hubs;
using Microsoft.AspNetCore.SignalR;
using IdentityServer4.AccessTokenValidation;

namespace DispatchProduct.Vehicles.API.Controllers
{
    //[Authorize]
    [Route("api/VehicleTechnican")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class VehicleTechnicanController : Controller
    {
        public IVehicleTechnicanManager manger;
        public readonly IMapper mapper;
        IDispatchingHub hub;
        IHubContext<DispatchingHub> dispatchingHub;
        public VehicleTechnicanController(IVehicleTechnicanManager _manger, IMapper _mapper, IDispatchingHub _hub, IHubContext<DispatchingHub> _dispatchingHub)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            hub = _hub;
            dispatchingHub = _dispatchingHub;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public IActionResult Get([FromRoute]int id)
        {
            VehicleTechnican entityResult = manger.Get(id);
            var result = mapper.Map<VehicleTechnican, VehicleTechnicanViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<VehicleTechnican> entityResult = manger.GetAll().ToList();
            List<VehicleTechnicanViewModel>  result = mapper.Map<List<VehicleTechnican>, List<VehicleTechnicanViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]VehicleTechnicanViewModel model)
        {
            VehicleTechnican entityResult = mapper.Map<VehicleTechnicanViewModel, VehicleTechnican>(model);
            entityResult = manger.Add(entityResult);
            VehicleTechnicanViewModel result = mapper.Map<VehicleTechnican, VehicleTechnicanViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]VehicleTechnicanViewModel model)
        {
            VehicleTechnican entityResult = mapper.Map<VehicleTechnicanViewModel, VehicleTechnican>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            VehicleTechnican entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        [Route("ReleaseByVehicleId/{id}")]
        [HttpGet]
        public bool ReleaseByVehicleId([FromRoute]int id)
        {
           return manger.ReleaseByVehicleId(id);
           
        }
        [Route("ReleaseByVehicleId/{technicanId}")]
        [HttpGet]
        public bool ReleaseByTechnicanId([FromRoute]string technicanId)
        {
            return manger.ReleaseByTechnicanId(technicanId);
        }

    }
}