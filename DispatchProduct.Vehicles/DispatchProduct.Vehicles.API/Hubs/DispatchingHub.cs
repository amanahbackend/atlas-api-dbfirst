﻿using DispatchProduct.Vehicles.API.ViewModel;
using DispatchProduct.Vehicles.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Utilites;

namespace DispatchProduct.Vehicles.Hubs
{
    //[Authorize]
    public class DispatchingHub : Hub, IDispatchingHub
    {
        private List<SignalRConnection> connections = new List<SignalRConnection>();
        DispatchingHubSettings setting;
        public DispatchingHub(IOptions<DispatchingHubSettings> _setting)
        {
            setting = _setting.Value;
        }
        public Task SendLiveVehicleLocation(List<VehicleViewModel> vehicles, IHubContext<DispatchingHub> dispatchingHub)
        {
            if (dispatchingHub.Clients != null)
            {
                //return dispatchingHub.Clients.All.InvokeAsync(setting.SendLiveVehicleLocation, vehicles);
                 dispatchingHub.Clients.Group(setting.SendLiveVehicleLocationGroupName).InvokeAsync(setting.SendLiveVehicleLocation, vehicles);
            }
            return null;
        }
        public async Task JoinGroup(string groupName)
        {
            await Groups.AddAsync(Context.ConnectionId, groupName);
        }
        public async Task LeaveGroup(string groupName)
        {
            await Groups.RemoveAsync(Context.ConnectionId, groupName);
        }
        public override Task OnConnectedAsync()
        {
            SignalRConnection con = new SignalRConnection();

            var httpContext = Context.Connection.GetHttpContext();
            var token = httpContext.Request.Query[setting.token].ToString();
            var userId = httpContext.Request.Query[setting.userId].ToString();
            var roles = httpContext.Request.Query[setting.roles].ToString();
            var lstroles=roles.Split(',').ToList();
            if (token != null && userId != null && lstroles != null)
            {
                con.Token = token;
                con.Roles = lstroles;
                con.UserId = userId;
                con.ConnectionId = Context.ConnectionId;
                connections.Add(con);
                if (setting.SendLiveVehicleLocationRoles.Contains("All"))
                {
                    JoinGroup(setting.SendLiveVehicleLocationGroupName).Wait();
                }
                else
                {
                    foreach (var item in lstroles)
                    {
                        if (setting.SendLiveVehicleLocationRoles.Contains(item))
                        {
                            JoinGroup(setting.SendLiveVehicleLocationGroupName).Wait();
                            break;
                        }
                    }
                }
            }

            return base.OnConnectedAsync();
        }
    }
}
