﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Vehicles.API.ViewModel;
using System;

namespace DispatchProduct.Vehicles.Entities
{
    public class VehicleTechnicanViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public string VID { get; set; }
        public string FK_Technican_Id { get; set; }
        public bool IsReleased { get; set; }
    }
}
