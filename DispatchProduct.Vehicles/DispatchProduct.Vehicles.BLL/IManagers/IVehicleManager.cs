﻿using DispatchProduct.Repoistry;
using DispatchProduct.Vehicles.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;

namespace DispatchProduct.Vehicles.BLL.Managers
{
    public interface IVehicleManager : IRepositry<Vehicle>
    {
        ProcessResult<List<Vehicle>> AddOrUpdateVehicles(List<Vehicle> Vehicles);
        ProcessResult<List<Vehicle>> GetVehicles();
        ProcessResult<List<Vehicle>> GetVehiclesReg();
        ProcessResult<List<Vehicle>> GetVehiclesByVIDS(List<string> VIDS);
        bool ReleaseVehicle(int vehicleId);
        bool ReleaseVehicles(List<int> vehicleIds);
        bool AssignVehicle(int vehicleId);
        List<Vehicle> GetAssignedVehicles();
        List<Vehicle> GetUnAssignedVehicles();
    }
}
