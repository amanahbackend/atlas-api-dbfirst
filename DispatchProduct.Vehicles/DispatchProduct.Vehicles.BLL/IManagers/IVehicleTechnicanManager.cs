﻿using DispatchProduct.Repoistry;
using DispatchProduct.Vehicles.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;

namespace DispatchProduct.Vehicles.BLL.Managers
{
    public interface IVehicleTechnicanManager : IRepositry<VehicleTechnican>
    {
        bool ReleaseByVehicleId(int vehicleId);
        bool ReleaseByTechnicanId(string technicanId);
        VehicleTechnican GetAssignedVehicleTechnican(int vehicleId);
    }
}
