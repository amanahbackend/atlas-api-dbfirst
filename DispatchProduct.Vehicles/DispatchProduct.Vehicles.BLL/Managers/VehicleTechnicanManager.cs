﻿using DispatchProduct.Repoistry;
using DispatchProduct.Vehicles.BLL.Managers;
using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.Models.Context;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Utilites;
using Utilites.ProcessingResult;

namespace DispatchProduct.VehicleTechnicans.BLL.Managers
{
    public class VehicleTechnicanManager : Repositry<VehicleTechnican>, IVehicleTechnicanManager
    {
        IVehicleManager vehicleManager;
        public VehicleTechnicanManager(VehicleDbContext context, IVehicleManager _vehicleManager)
            : base(context)
        {
            vehicleManager = _vehicleManager;

        }
        public VehicleTechnican GetAssignedVehicleTechnican(int vehicleId)
        {
            return GetAll().Where(r => r.IsReleased == true && r.VehicleId == vehicleId).FirstOrDefault();
        }
        public bool ReleaseByVehicleId(int vehicleId)
        {
            bool result = false;
            var vehicle = GetAll().Where(r => r.VehicleId == vehicleId).FirstOrDefault();
            if (vehicle != null)
            {
                vehicleManager.ReleaseVehicle(vehicleId);
                vehicle.IsReleased = true;
                result = Update(vehicle);
            }
            return result;
        }
        public bool ReleaseByTechnicanId(string technicanId)
        {
            bool result = false;
            var vehicle = GetAll().Where(r => r.FK_Technican_Id == technicanId).FirstOrDefault();
            if (vehicle != null)
            {
                vehicleManager.ReleaseVehicle(vehicle.VehicleId);
                vehicle.IsReleased = true;
                result = Update(vehicle);
            }
            return result;
        }
        public override VehicleTechnican Add(VehicleTechnican entity)
        {
            var vehicles = GetAll().Where(r => (r.VehicleId == entity.VehicleId && r.IsReleased == false) || (r.FK_Technican_Id == entity.FK_Technican_Id && r.IsReleased == false)).ToList();
            if (vehicles.Count == 0)
            {
                var vehicleIds = vehicles.Select(v => v.VehicleId).ToList();
                vehicleManager.ReleaseVehicles(vehicleIds);
            }
            vehicleManager.AssignVehicle(entity.VehicleId);
            return base.Add(entity);
        }

    }
}
