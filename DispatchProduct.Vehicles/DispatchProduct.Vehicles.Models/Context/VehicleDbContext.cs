﻿using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Vehicles.Models.Context
{
    public class VehicleDbContext : DbContext
    {
        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<VehicleTechnican> VehicleTechnican { get; set; }
        public VehicleDbContext(DbContextOptions<VehicleDbContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
        }
    }
}
