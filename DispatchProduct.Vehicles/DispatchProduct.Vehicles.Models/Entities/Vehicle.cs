﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;

namespace DispatchProduct.Vehicles.Entities
{
    public class Vehicle : TrackingLocation, IVehicle, ITrackingLocation, IBaseEntity
    {
        public string VID { get; set; }

        public string VReg { get; set; }

        public string Vtype { get; set; }

        public string PanicButton { get; set; }

        public string DID { get; set; }

        public string DriverName { get; set; }

        public string Area { get; set; }

        public string Address { get; set; }

        public DateTime? Date { get; set; }

        public DateTime? ModifyDate { get; set; }

        public bool IsAssigned { get; set; }

    }
}
