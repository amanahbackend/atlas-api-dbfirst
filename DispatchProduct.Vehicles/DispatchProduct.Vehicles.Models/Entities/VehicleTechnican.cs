﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;

namespace DispatchProduct.Vehicles.Entities
{
    public class VehicleTechnican : BaseEntity
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public string VID { get; set; }
        public string FK_Technican_Id { get; set; }
        public bool IsReleased { get; set; }
    }
}
