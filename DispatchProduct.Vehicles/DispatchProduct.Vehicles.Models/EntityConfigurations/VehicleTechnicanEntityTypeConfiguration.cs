﻿using DispatchProduct.Vehicles.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Vehicles.EntityConfigurations
{
    public class VehicleTechnicanEntityTypeConfiguration
        : IEntityTypeConfiguration<VehicleTechnican>
    {
        public void Configure(EntityTypeBuilder<VehicleTechnican> VehicleConfiguration)
        {
            VehicleConfiguration.ToTable("VehicleTechnican");

            VehicleConfiguration.HasKey(o => o.Id);

            VehicleConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("VehicleTechnicanseq");
        }
    }
}
