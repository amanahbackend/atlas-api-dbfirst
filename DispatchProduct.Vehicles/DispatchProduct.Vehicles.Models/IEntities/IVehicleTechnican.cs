﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;

namespace DispatchProduct.Vehicles.Entities
{
    public interface IVehicleTechnican : IBaseEntity
    {
        int Id { get; set; }
        int VehicleId { get; set; }
        string VID { get; set; }
        string FK_Technican_Id { get; set; }
        bool IsReleased { get; set; }
    }
}
