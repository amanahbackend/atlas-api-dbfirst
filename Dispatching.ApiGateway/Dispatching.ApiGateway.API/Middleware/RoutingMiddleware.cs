﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Dispatching.ApiGateway.API.Proxy;
using DnsClient;
using System.Net;
using BuildingBlocks.ServiceDiscovery;

namespace Dispatching.ApiGateway.API.Middleware
{
    public class RoutingMiddleware
    {
        public readonly RequestDelegate _next;

        public RoutingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IDnsQuery dnsQuery)
        {
            string requestedAction = httpContext.Request.Path.Value;
            if (requestedAction.ToLower().Contains("apigateway"))
            {
                await _next.Invoke(httpContext);
            }
            else
            {
                var urlSegments = requestedAction.Split('/');
                string service = urlSegments[1];
                string segment2 = urlSegments[2].ToLower();

                string basePath = "";

                if (segment2.Equals("uae") || segment2.Equals("kw"))
                {
                    if (!service.ToLower().Equals("contentapi"))
                    {
                        string country = segment2.ToLower();
                        basePath = await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, $"{service}_{country}");
                    }
                    else
                    {
                        basePath = await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, service);
                    }
                }
                else
                {
                    basePath = await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, service);
                }


                if (String.IsNullOrEmpty(basePath))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                }
                else
                {
                    var queryString = httpContext.Request.QueryString.Value;
                    requestedAction = requestedAction.Remove(0, service.Length + 1);
                    if (segment2.Equals("uae") || segment2.Equals("kw"))
                    {
                        if (!service.ToLower().Equals("contentapi"))
                        {
                            string country = segment2;
                            requestedAction = requestedAction.Remove(0, country.Length + 1);
                        }
                    }
                    string url = $"{basePath}{requestedAction}{queryString}";

                    await httpContext.ProxyRequest(new Uri(url));
                }
            }
        }
    }

}
